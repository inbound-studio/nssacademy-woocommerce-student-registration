<?php
/**
 * WC_Student_Registration_Form_Handler class file
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration;

use function WC_Student_Registration\Functions\get_district_rep_district;
use function WC_Student_Registration\Functions\get_students_in_school_district;
use function WC_Student_Registration\Functions\get_all_students;
use function WC_Student_Registration\Functions\get_financial_aid_orders_page_id;
use function WC_Student_Registration\Functions\get_financial_aid_discount_coupon;
use function WC_Student_Registration\Functions\get_order_pending_financial_aid_amount;
use function WC_Student_Registration\Functions\update_order_financial_aid_discount;
use function WC_Student_Registration\Functions\get_school_district_name;
use function WC_Student_Registration\Functions\get_student_registration_line_item;
use function WC_Student_Registration\Functions\get_parent_guardian;
use function WC_Student_Registration\Functions\get_student_order;
use function WC_Student_Registration\Functions\get_student_registered_class;

use function WC_Student_Registration\Functions\get_director_all_students;
use function WC_Student_Registration\Functions\get_director_students_in_school_district;
use function WC_Student_Registration\Functions\get_student_registered_class_teacher;
use function WC_Student_Registration\Functions\get_student_parent_guardian_1;
use function WC_Student_Registration\Functions\get_student_order_phone;
use function WC_Student_Registration\Functions\get_student_order_email;
use function WC_Student_Registration\Functions\get_student_order_address;
use function WC_Student_Registration\Functions\get_student_order_tuition_balance;
use function WC_Student_Registration\Functions\get_student_order_financial_aid;
use function WC_Student_Registration\Functions\get_student_order_year;
use function WC_Student_Registration\Functions\get_all_products;
use function WC_Student_Registration\Functions\get_all_students_by_year;
use function WC_Student_Registration\Functions\get_student_emergency_phone;
use function WC_Student_Registration\Functions\get_student_emergency_email;
use function WC_Student_Registration\Functions\get_student_order_without_html_address;
use function WC_Student_Registration\Functions\get_student_emergency_name;

use function WC_Student_Registration\Functions\get_district_all_students;
use function WC_Student_Registration\Functions\get_district_students_in_school_district;
use function WC_Student_Registration\Functions\get_district_financial_amount;
use function WC_Student_Registration\Functions\get_district_remaining_financial_amount;
use function WC_Student_Registration\Functions\get_order_financial_aid_amount;
use function WC_Student_Registration\Functions\get_student_order_user_address;
use function WC_Student_Registration\Functions\get_student_order_user_city;
use function WC_Student_Registration\Functions\get_student_order_user_state;
use function WC_Student_Registration\Functions\get_student_order_user_country;
use function WC_Student_Registration\Functions\get_student_order_user_postal_code;

class WC_Student_Registration_Form_Handler {

    public function __construct() {
        add_action( 'wp', [ $this, 'submit_financial_aid' ] );
        add_action( 'wp', [ $this, 'submit_financial_aid_review' ] );
        add_action( 'wp', [ $this, 'export_student_registration_list' ] );
        add_action( 'wp', [ $this, 'export_director_student_registration_list' ] );
        add_action( 'wp', [ $this, 'export_district_student_registration_list' ] );
        add_action( 'wp', [ $this, 'export_director_master_list' ] );

        add_action( 'admin_init', [ $this, 'export_student_registrations' ] );
    }

    /**
     * District rep submit financial aid form
     *
     * @return void
     */
    function submit_financial_aid() {
        global $wpdb;

        if ( is_admin() || empty( $_POST['wcsr_action'] ) || $_POST['wcsr_action'] !== 'submit_financial_aid' ) {
            return;
        }

        if ( ! wp_verify_nonce( $_POST['_wpnonce'] ) ) {
            wp_die( __( 'Invalid nonce, please try again.', 'wc-student-registration' ) );
        }

        if ( ! current_user_can( 'edit_workflow' ) ) {
            wp_die( __( 'You do not have permission to view this content.', 'wc-student-registration' ) );
        }
        
        if ( ! isset( $_POST['financial_aid_amount'] ) || ! is_numeric( $_POST['financial_aid_amount'] ) ) {
            wp_die( __( 'Invalid financial aid amount.', 'wc-student-registration' ) );
        }

        if ( ! isset( $_POST['order_id'] ) ) {
            wp_die( __( 'Invalid order ID.', 'wc-student-registration' ) );
        }

        
        $amount = $_POST['financial_aid_amount'] ?? 0;
        $order  = wc_get_order( $_POST['order_id'] );

        if ( ! $order ) {
            wp_die( __( 'Order not found', 'wc-student-registration' ) );
        }

        $order_total = $order->get_total();
        $order_total_price = wc_price( $order_total );
        
        if( $amount > $order_total  ) {
            $_POST['error'] = __( 'Please Provide less then order total amount. Order Total is '.$order_total_price.'', 'wc-student-registration' ) ;
            return ( $_POST['error'] );
        } 
        $order_district = $order->get_meta( 'school_district' );
        $fd_remaining_amount = get_district_remaining_financial_amount($order_district);
        $fd_remaining_amount = (int)$fd_remaining_amount;
        $fd_remaining_amount_price = wc_price($fd_remaining_amount);
        if( $amount > $fd_remaining_amount ) {
            $_POST['error'] = __( 'Apply a value less then '.$fd_remaining_amount_price.'', 'wc-student-registration' );
            return ( $_POST['error'] );
        }
        
        $order->update_meta_data( 'financial_aid_district_rep', get_current_user_id() );

        $emails = new \WC_Emails;
        $order_district_term = get_term( $order_district, 'school_district' );
        if ( $order_district_term ) {
            $fd_approve_amount = get_term_meta( $order_district_term->term_id, 'financial_aid_approve_amount', true );
            $fd_approve_amount = (int)$fd_approve_amount;
            if( empty($fd_approve_amount) ) {
                $fd_approve_amount = 0;
            }
            $fd_approve_amount += $amount;
            update_term_meta($order_district_term->term_id, 'financial_aid_approve_amount', $fd_approve_amount );
        }
        if ( $amount > 0 ) {
            $order->update_meta_data( 'pending_financial_aid_amount', $amount );
            $order->update_meta_data( 'pending_financial_aid_needs_review', 1 );
            $emails->emails['WC_Email_Director_Financial_Aid']->trigger( $order->get_id(), $order );
        } else {
            $order->update_meta_data( 'financial_aid_amount', $amount );
            $order->update_status( 'pending', sprintf( __( 'Financial aid amount: %s', 'wc-student-registration' ), wc_price( $amount ) ) );
            $emails->emails['WC_Email_Customer_Financial_Aid_Denial']->trigger( $order->get_id(), $order );
        }

        $order->save();

        wp_redirect( get_permalink( get_financial_aid_orders_page_id() ) );
        exit;
    }

    /**
     * Director submit financial aid review
     *
     * @return void
     */
    function submit_financial_aid_review() {
        if ( is_admin() || empty( $_POST['wcsr_action'] ) || $_POST['wcsr_action'] !== 'submit_financial_aid_review' ) {
            return;
        }

        if ( ! wp_verify_nonce( $_POST['_wpnonce'] ) ) {
            wp_die( __( 'Invalid nonce, please try again.', 'wc-student-registration' ) );
        }

        if ( ! current_user_can( 'review_financial_aid' ) ) {
            wp_die( __( 'You do not have permission to view this content.', 'wc-student-registration' ) );
        }
        
        if ( ! isset( $_POST['financial_aid_review'] ) || ! in_array( $_POST['financial_aid_review'], [ 'approved', 'denied' ] ) ) {
            wp_die( __( 'Invalid financial aid response.', 'wc-student-registration' ) );
        }

        if ( ! isset( $_POST['order_id'] ) ) {
            wp_die( __( 'Invalid order ID.', 'wc-student-registration' ) );
        }

        $status = $_POST['financial_aid_review'];
        $order  = wc_get_order( $_POST['order_id'] );
        $notes  = isset( $_POST['financial_aid_notes'] ) ? $_POST['financial_aid_notes'] : '';

        if ( ! $order ) {
            wp_die( __( 'Order not found', 'wc-student-registration' ) );
        }

        $amount = get_order_pending_financial_aid_amount( $order );
        
        $order->update_meta_data( 'financial_aid_notes', $notes );
        $order->update_meta_data( 'pending_financial_aid_needs_review', 0 );
        $order->save();
        $order_district = $order->get_meta( 'school_district' );
        $emails = new \WC_Emails;
        if ( $status === 'approved' ) {
            $order_total = $order->get_total();
            $order_total_price = wc_price( $order_total );
            
            if( $amount > $order_total  ) {
                $_POST['error'] = ( __( 'Please Provide less then order total amount. Order Total is '.$order_total_price.'', 'wc-student-registration' ) );
                return $_POST['error'];
            } else {
                $order->update_meta_data( 'financial_aid_amount', $amount );
                if( $amount == $order_total ) {
                    $order->update_status('completed', sprintf( __( 'Financial aid amount: %s', 'wc-student-registration' ), wc_price( $amount ) ) ); 
                } else {
                    $order->update_status( 'pending', sprintf( __( 'Financial aid amount: %s', 'wc-student-registration' ), wc_price( $amount ) ) );
                }
                
                update_order_financial_aid_discount( $order, $amount );
                $order->save();
                $emails->emails['WC_Email_Customer_Financial_Aid_Approval']->trigger( $order->get_id(), $order );
            }
        } else {
            $order_district_term = get_term( $order_district, 'school_district' );
            if ( $order_district_term ) {
                $fd_approve_amount = get_term_meta( $order_district_term->term_id, 'financial_aid_approve_amount', true );
                $fd_approve_amount = (int)$fd_approve_amount;
                if( empty($fd_approve_amount) ) {
                    $fd_approve_amount = 0;
                }
                
                $fd_approve_amount -= $amount;
                update_term_meta($order_district_term->term_id, 'financial_aid_approve_amount', $fd_approve_amount );
            }
            $emails->emails['WC_Email_District_Rep_Financial_Aid_Reviewed']->trigger( $order->get_id(), $order );
        }
     
        wp_redirect( get_permalink( get_financial_aid_orders_page_id() ) );
        exit;
    }

    /**
     * Export class registration list
     *
     * @return void
     */
    function export_student_registration_list() {
        global $wpdb;

        if ( is_admin() || empty( $_GET['export-wcsr-reg-list'] ) ) {
            return;
        }

        if ( ! wp_verify_nonce( $_GET['_wpnonce'] ) ) {
            wp_die( __( 'Invalid nonce, please try again.', 'wc-student-registration' ) );
        }

        if ( ! current_user_can( 'edit_workflow' ) ) {
            wp_die( __( 'You do not have permission to view this content.', 'wc-student-registration' ) );
        }

        header( 'Content-Description: File Transfer' );
        header( 'Content-Type: application/csv' );
        header( 'Content-Disposition: attachment; filename=student-registration-list.csv' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );

        $handle = fopen( 'php://output', 'w' );

        // Return single or array of districts
        $district = ! current_user_can( 'manage_options' ) ? get_district_rep_district( get_current_user_id() ) : get_terms( [ 'taxonomy' => 'school_district', 'hide_empty' => false, 'fields' => 'ids' ] );

        $students = get_students_in_school_district( $district );

        $keys = [
            'student_first_name',
            'student_last_name',
            'registered_class',
            'student_gender',
            'student_dob',
            'student_ethnicity',
            'student_shirt_size',
            'student_school_name',
            'student_school_district',
            'student_grade',
            'student_health_concerns_conditional',
            'student_health_concerns',
            'student_medications_conditional',
            'student_reason_medication',
            'student_medications',
            'student_diabetes_conditional',
            'student_method_transport',
            'student_strengths',
            'student_skills_to_improve',
            'student_additional_comments',
            'student_emergency_contacts',
            'authorize_medicate_student',
            'consent_media_release_student'
        ];

        ob_clean();

        fputcsv( $handle, $keys );

        foreach ( $students as $student ) {
            $meta  = [];
            $_meta = get_post_meta( $student->ID );

            foreach ( $keys as $key ) {
                $set = false;

                foreach ( $_meta as $_key => $_value ) {
                    if ( $key === $_key ) {

                        switch ( $key ) {
                            case 'registered_class' :
                                if( !empty( current( $_value ) ) ) {
                                    $value_array = explode(',', current( $_value )) ;
                                    $value_title = array();
                                    foreach ($value_array as $val) {
                                        if( !empty( $val ) ) {
                                            $title = get_the_title( $val );
                                            $value_title[] = $title;
                                        }
                                    }
                                    $meta[] = implode(',', $value_title);
                                } else {
                                    $meta[] = '';
                                }
                                break;


                            default :
                                $meta[] = current( $_value );
                        }
                             
                        $set = true;
                    }
                }

                if ( ! $set ) {
                    $meta[] = '-';
                }
            }

            fputcsv( $handle, $meta );
        }

        ob_flush();
        fclose( $handle );
        exit;
    }

    /**
     * Export director registration list
     *
     * @return void
     */
    function export_director_student_registration_list() {
        global $wpdb;

        if ( is_admin() || empty( $_GET['export-wcsr-director-reg-list'] ) ) {
            return;
        }

        if ( ! wp_verify_nonce( $_GET['_wpnonce'] ) ) {
            wp_die( __( 'Invalid nonce, please try again.', 'wc-student-registration' ) );
        }
        
        if ( ! current_user_can( 'administrator' ) && ( ! current_user_can( 'edit_workflow' ) && ! current_user_can( 'review_financial_aid' ) ) ) {
            wp_die( __( 'You do not have permission to view this content.', 'wc-student-registration' ) );
        }

        header( 'Content-Description: File Transfer' );
        header( 'Content-Type: application/csv' );
        header( 'Content-Disposition: attachment; filename=student-director-registration-list.csv' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );

        $handle = fopen( 'php://output', 'w' );
        
        $filter_course      = $_GET['filter_course'];
        $filter_year        = $_GET['filter_year'];
        $filter_month       = $_GET['filter_month'];
        $search_student     = $_GET['search_student'];
        $posts_per_page = ( !empty($filter_year) || !empty( $search_student )  || !empty( $filter_course ) || !empty( $filter_month ) ) ? 100 : 100 ;
        
        /*
        * Add order month dropdown
        */
        global $wpdb, $wp_locale;
        $extra_checks = "AND post_status != 'auto-draft'";
        if (! isset($_GET['post_status']) || 'trash' !== $_GET['post_status']) {
            $extra_checks .= " AND post_status != 'trash'";
        } elseif (isset($_GET['post_status'])) {
            $extra_checks = $wpdb->prepare(' AND post_status = %s', $_GET['post_status']);
        }
        $months = $wpdb->get_results($wpdb->prepare("
                SELECT DISTINCT YEAR( post_date ) AS year, MONTH( post_date ) AS month
                FROM $wpdb->posts
                WHERE post_type = %s
                $extra_checks
                ORDER BY post_date DESC
        ", 'shop_order'));
        $month_count = count($months);
        if ( !empty( $months ) ) {
            $m = isset($_GET['filter_month']) ? $_GET['filter_month'] : $months[0]->year.'-'.zeroise($months[0]->month,2);
        }
        
        $current_year_change = date("Y"); 
        $filter_course      = $_GET['filter_course'];
        $filter_year        = $_GET['filter_year'] ? $_GET['filter_year'] : $current_year_change;
        $filter_month       = $_GET['filter_month'] ? $_GET['filter_month'] : $m;
        $search_student     = $_GET['search_student'];
        
        $paged = get_query_var( 'paged' ) ? get_query_var('paged') : 1 ;
        $posts_per_page = ( !empty($filter_year) || !empty( $search_student )  || !empty( $filter_course ) || !empty( $filter_month ) ) ? 100 : 100 ;
    
        if ( current_user_can( 'manage_options' ) ) {
            $students       = get_director_all_students( $posts_per_page , $search_student , $filter_course , $filter_year , $filter_month );
        } else {
            $students       = get_director_students_in_school_district( get_district_rep_district( get_current_user_id() ), $posts_per_page , $search_student , $filter_course , $filter_year, $filter_month  );
        }

        $keys = [
            'Student Name',
            'Course',
            'Teacher',
            'Grade',
            'District and School',
            'Parent First/Last Name',
            'Phone Number',
            'Email',
            'Address',
            'Transportation',
            'Tuition Balance',
            'Applying for aid'
        ];

        ob_clean();

        fputcsv( $handle, $keys );
        
        if ( $students ) :
            foreach ( $students as $student_key => $student ) : 
                $stu_orders     = get_student_order($student,true,true);
                if( $stu_orders ) {
                    foreach ($stu_orders as $stu_orders_key => $stu_orders_value) {
                        $order = wc_get_order( $stu_orders_value ); 
                        $class_id = '';
                        foreach ($order->get_items() as $item_id => $item ) {
                            if( $item->get_meta( 'Student' ) == $student->ID ) { 
                                $class_id = $item->get_product_id();
                                $class_id       = !empty( $class_id ) ? $class_id : '';
                                $flag = true;
                                if( !empty( $filter_course ) ) {
                                    if( $filter_course != $class_id ) {
                                        $flag = false;
                                    }
                                }
                                if( $flag ) {
                                    $meta  = [];
                                    $teacher        = get_student_registered_class_teacher( $class_id );
                                    $parent_guardian_1 = $item->get_meta( 'Parent/Guardian 1' );
                                    $parent_1       = trim( sprintf( '%s %s', get_post_meta( $parent_guardian_1, 'parent_guardian_first_name', true ), get_post_meta( $parent_guardian_1, 'parent_guardian_last_name', true ) ) );
                                    $parent_1       = empty( $parent_1 ) ? 'No name set' : $parent_1;
                                    $parent_1_id    = $parent_guardian_1;
                                    $phone          = get_student_emergency_phone( $student );
                                    $email          = get_student_emergency_email( $student );
                                    $address        = get_student_order_without_html_address($student);
                                    $product        = $item->get_product();
                                    $tuition_bal    = (  $product->get_price() );
                                    $financial_aid  = ucfirst( get_student_order_financial_aid( $student,$stu_orders_value ) ? get_student_order_financial_aid( $student,$stu_orders_value ) : 'no' );
                                    $meta[] =  esc_html($student->get_full_name());
                                    $meta[] =  esc_html($class_id ? get_the_title( $class_id ) : '');
                                    $meta[] =  esc_html($teacher ? $teacher : ''); 
                                    $meta[] =  esc_html($student->get_grade() ? $student->get_grade() : ''); 
                                    $meta[] =  esc_html( $student->get_district() ? $student->get_district() : '' ) . '-' . esc_html( $student->get_school() ? $student->get_school() : '' ); 
                                    $meta[] =  esc_html($parent_1 ? $parent_1 : ''); 
                                    $meta[] =  esc_html($phone ? $phone : ''); 
                                    $meta[] =  esc_html($email ? $email : '');
                                    $meta[] =  ( $address ? $address : ''); 
                                    $meta[] =  esc_html($student->get_transportation() ? $student->get_transportation() : ''); 
                                    $meta[] =  esc_html($tuition_bal);       
                                    $meta[] =  esc_html($financial_aid ? $financial_aid : '');  
                                    fputcsv( $handle, $meta );
                                }
                            }
                        }
                    }
                }
                
            endforeach; 
        endif;
        
        ob_flush();
        fclose( $handle );
        exit;
    }

    /**
     * Export district registration list
     *
     * @return void
     */
    function export_district_student_registration_list() {
        global $wpdb;

        if ( is_admin() || empty( $_GET['export-wcsr-district-reg-list'] ) ) {
            return;
        }

        if ( ! wp_verify_nonce( $_GET['_wpnonce'] ) ) {
            wp_die( __( 'Invalid nonce, please try again.', 'wc-student-registration' ) );
        }
        
        if ( ! current_user_can( 'administrator' ) && ( ! current_user_can( 'edit_workflow' ) && ! current_user_can( 'review_financial_aid' ) ) ) {
            wp_die( __( 'You do not have permission to view this content.', 'wc-student-registration' ) );
        }
		
        header( 'Content-Description: File Transfer' );
        header( 'Content-Type: application/csv' );
        header( 'Content-Disposition: attachment; filename=student-district-registration-list.csv' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );

        $handle = fopen( 'php://output', 'w' );
        $filter_course      = $_GET['filter_course'];
        $filter_year        = $_GET['filter_year'];
        $search_student     = $_GET['search_student'];
        $posts_per_page = ( !empty($filter_year) || !empty( $search_student )  || !empty( $filter_course ) ) ? -1 : -1 ;
        
        if ( current_user_can( 'manage_options' ) ) {
            $students       = get_district_all_students( $posts_per_page , $search_student  , $filter_year );
        } else {
            $students       = get_district_students_in_school_district( get_district_rep_district( get_current_user_id()), $posts_per_page , $search_student , $filter_year );
        }

        $keys = [
            'Student Name',
            'Course',
            'Teacher',
            'Grade',
            'District and School',
            'Parent First/Last Name',
            'Phone Number',
            'Email',
            'Transportation',
            'Applying for aid'
        ];

        ob_clean();

        fputcsv( $handle, $keys );
        
        if ( $students ) :
            foreach ( $students as $student_key => $student ) : 
               
                $stu_orders     = get_student_order($student,true);
                if( $stu_orders ) {
                    foreach ($stu_orders as $stu_orders_key => $stu_orders_value) {
                        $order = wc_get_order( $stu_orders_value ); 
                        $class_id = '';
                        foreach ($order->get_items() as $item_id => $item ) {
                            if( $item->get_meta( 'Student' ) == $student->ID ) { 
                                $class_id = $item->get_product_id();
                                $class_id       = !empty( $class_id ) ? $class_id : '';
                                $flag = true;
                                if( !empty( $filter_course ) ) {
                                    if( $filter_course != $class_id ) {
                                        $flag = false;
                                    }
                                }
                                if( $flag ) {
                                    $meta  = [];
                                    $teacher        = get_student_registered_class_teacher( $class_id );
                                    $parent_guardian_1 = $item->get_meta( 'Parent/Guardian 1' );
                                    $parent_1       = trim( sprintf( '%s %s', get_post_meta( $parent_guardian_1, 'parent_guardian_first_name', true ), get_post_meta( $parent_guardian_1, 'parent_guardian_last_name', true ) ) );
                                    $parent_1       = empty( $parent_1 ) ? 'No name set' : $parent_1;
                                    $parent_1_id    = $parent_guardian_1;
                                    $phone          = get_student_emergency_phone( $student );
                                    $email          = get_student_emergency_email( $student );
                                    $address        = get_student_order_without_html_address($student);
                                    $product        = $item->get_product();
                                    $tuition_bal    = (  $product->get_price() );
                                    $financial_aid  = ucfirst( get_student_order_financial_aid( $student,$stu_orders_value ) ? get_student_order_financial_aid( $student,$stu_orders_value ) : 'no' );
                                    $meta[] =  esc_html($student->get_full_name());
                                    $meta[] =  esc_html($class_id ? get_the_title( $class_id ) : '');
                                    $meta[] =  esc_html($teacher ? $teacher : ''); 
                                    $meta[] =  esc_html($student->get_grade() ? $student->get_grade() : ''); 
                                    $meta[] =  esc_html( $student->get_district() ? $student->get_district() : '' ) . '-' . esc_html( $student->get_school() ? $student->get_school() : '' ); 
                                    $meta[] =  esc_html($parent_1 ? $parent_1 : ''); 
                                    $meta[] =  esc_html($phone ? $phone : ''); 
                                    $meta[] =  esc_html($email ? $email : '');
                                    $meta[] =  esc_html($student->get_transportation() ? $student->get_transportation() : ''); 
                                    $meta[] =  esc_html($financial_aid ? $financial_aid : '');  
                                    fputcsv( $handle, $meta );
                                }
                            }
                        }
                    }
                }
                
            endforeach; 
        endif;
        
        ob_flush();
        fclose( $handle );
        exit;
    }


    /**
     * Export student class registrations
     *
     * @return void
     */
    function export_student_registrations() {
        global $wpdb;

        if ( ! is_admin() || empty( $_GET['export-wcsr-students-registratios'] ) ) {
            return;
        }

        if ( ! wp_verify_nonce( $_GET['_wpnonce'] ) ) {
            wp_die( __( 'Invalid nonce, please try again.', 'wc-student-registration' ) );
        }

        if ( ! current_user_can( 'manage_options' ) ) {
            wp_die( __( 'You do not have permission to view this content.', 'wc-student-registration' ) );
        }

        header( 'Content-Description: File Transfer' );
        header( 'Content-Type: application/csv' );
        header( 'Content-Disposition: attachment; filename=students-class-registrations.csv' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );

        $handle = fopen( 'php://output', 'w' );

        // Return single or array of districts
        $district = get_terms( [ 'taxonomy' => 'school_district', 'hide_empty' => false, 'fields' => 'ids' ] );

        $students = get_all_students();

        $keys = [
            'student_first_name',
            'student_last_name',
            'registered_class',
            'student_school_district',
            'student_parents_1_name',
            'student_parents_1_phone',
            'student_parents_1_email',
            'student_parents_2_name',
            'student_parents_2_phone',
            'student_parents_2_email',
            'related_order',
            'order_status',
        ];

        ob_clean();

        fputcsv( $handle, $keys );

        foreach ( $students as $student ) {
            $meta  = [];
            $_meta = wp_parse_args( get_post_meta( $student->ID ),
                array_map( function( $value ) {
                    return [ '' ];
                }, array_flip( $keys ) )
            );

            $registration_line = get_student_registration_line_item( $student->ID );

            if ( ! $registration_line ) {
                continue;
            }

            foreach ( $_meta as $_key => $_value ) {
                if ( in_array( $_key, $keys ) ) {
                    $value = '';

                    switch ( $_key ) {
                        case 'registered_class' :
                            $order = get_student_order( $student->ID,true );
                            if( $order ) {
                                $class_id = '';
                                $class_array = array();
                                foreach ($order as $orders_key => $orders_value) {
                                    $order = wc_get_order( $orders_value ); 
                                    foreach ($order->get_items() as $item_id => $item ) {
                                        if( $item->get_meta( 'Student' ) == $student->ID ) { 
                                            $class_id       = $item->get_product_id();
                                            $class_id       = !empty( $class_id ) ? $class_id : '';
                                            $class_array[]  = get_the_title($class_id);
                                        }
                                    }
                                }
                                $class_name = $class_array ? implode(',', $class_array) : '';
                            } else {
                                $class_name = '';
                            }
                            $value = $class_name ? $class_name : '-';
                            break;

                        case 'student_school_district' :
                            $value = get_school_district_name( current( $_value ) );
                            break;

                        case 'student_parents_1_name' :
                            $parent_guardian_1 = get_parent_guardian( $registration_line->get_meta( 'Parent/Guardian 1' ) );
                            $value = $parent_guardian_1->get_full_name();
                            break;

                        case 'student_parents_1_phone' :
                            $parent_guardian_1 = get_parent_guardian( $registration_line->get_meta( 'Parent/Guardian 1' ) );
                            $value = $parent_guardian_1->get_phone();
                            break;

                        case 'student_parents_1_email' :
                            $parent_guardian_1 = get_parent_guardian( $registration_line->get_meta( 'Parent/Guardian 1' ) );
                            $value = $parent_guardian_1->get_email();
                            break;

                        case 'student_parents_2_name' :
                            $parent_guardian_2 = get_parent_guardian( $registration_line->get_meta( 'Parent/Guardian 2' ) );
                            $value = $parent_guardian_2->get_full_name();
                            break;

                        case 'student_parents_2_phone' :
                            $parent_guardian_2 = get_parent_guardian( $registration_line->get_meta( 'Parent/Guardian 2' ) );
                            $value = $parent_guardian_2->get_phone();
                            break;

                        case 'student_parents_2_email' :
                            $parent_guardian_2 = get_parent_guardian( $registration_line->get_meta( 'Parent/Guardian 2' ) );
                            $value = $parent_guardian_2->get_email();
                            break;

                        case 'related_order' :
                            $order = get_student_order( $student->ID,true );
                            $value = $order ? implode(',', $order) : '';
                            break;

                        case 'order_status' :
                            $order = get_student_order( $student->ID,true );
                            if( $order ) {
                                $order_status_array = array();
                                foreach ( $order as $order_id ) {
                                    $order_data = wc_get_order( $order_id );
                                    $order_status_array[] = $order_data->get_status();
                                }
                                $value = $order ? implode(',', $order_status_array) : '';
                            } else {
                                $value = '';
                            }
                            break;

                        default :
                            $value = current( $_value );
                    }

                    if ( ! $value ) {
                        $value = '-';
                    }

                    $meta[] = $value;
                }
            }

            fputcsv( $handle, $meta );
        }

        ob_flush();
        fclose( $handle );
        exit;
    }
    
    
    /**
     * Export Director Master list
     *
     * @return void
     */
    function export_director_master_list() {
        global $wpdb;

        if ( is_admin() || empty( $_GET['export-wcsr-director-master-list'] ) ) {
            return;
        }

        if ( ! wp_verify_nonce( $_GET['_wpnonce'] ) ) {
            wp_die( __( 'Invalid nonce, please try again.', 'wc-student-registration' ) );
        }
        
        if ( ! current_user_can( 'administrator' ) ) {
            wp_die( __( 'You do not have permission to view this content.', 'wc-student-registration' ) );
        }
        
        $is_emergeny_contact    = $_GET['export-wcsr-emergency-contact'];
        $is_teacher_info        = $_GET['export-wcsr-teacher-info'];
        $is_financial_aid       = $_GET['export-wcsr-financial-aid'];
        $is_t_shirt             = $_GET['export-wcsr-t-shirt'];
        
        if( $is_emergeny_contact ) {
            $filename = 'Emergency-contact-list.csv';
            $is_master_list         = false;
        } else if( $is_teacher_info ) {
            $filename = 'Teacher-information-list.csv';
            $is_master_list         = false;
        } else  if( $is_financial_aid ) {
            $filename = 'Financial-aid-list.csv';
            $is_master_list         = false;
        } else if( $is_t_shirt ) {
            $filename = 'Student-t-shirt-list.csv';
            $is_master_list         = false;
        } else {
            $filename = 'Master-list.csv';
            $is_master_list         = true;
        }
        
        header( 'Content-Description: File Transfer' );
        header( 'Content-Type: application/csv' );
        header( 'Content-Disposition: attachment; filename='.$filename );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );

        $handle = fopen( 'php://output', 'w' );
        
        /*
        * Add order month dropdown
        */
        global $wpdb, $wp_locale;
        $extra_checks = "AND post_status != 'auto-draft'";
        if (! isset($_GET['post_status']) || 'trash' !== $_GET['post_status']) {
            $extra_checks .= " AND post_status != 'trash'";
        } elseif (isset($_GET['post_status'])) {
            $extra_checks = $wpdb->prepare(' AND post_status = %s', $_GET['post_status']);
        }
        $months = $wpdb->get_results($wpdb->prepare("
                SELECT DISTINCT YEAR( post_date ) AS year, MONTH( post_date ) AS month
                FROM $wpdb->posts
                WHERE post_type = %s
                $extra_checks
                ORDER BY post_date DESC
        ", 'shop_order'));
        $month_count = count($months);
        if ( !empty( $months ) ) {
            $m = isset($_GET['filter_month']) ? $_GET['filter_month'] : $months[0]->year.'-'.zeroise($months[0]->month,2);
        }
        
        $current_year_change = date("Y"); 
        $filter_course      = $_GET['filter_course'];
        $filter_year        = $_GET['filter_year'] ? $_GET['filter_year'] : $current_year_change;
        $filter_month       = $_GET['filter_month'] ? $_GET['filter_month'] : $m;
        $search_student     = $_GET['search_student'];
        
        $paged = get_query_var( 'paged' ) ? get_query_var('paged') : 1 ;
        $posts_per_page = ( !empty($filter_year) || !empty( $search_student )  || !empty( $filter_course ) || !empty( $filter_month ) ) ? 100 : 100 ;
		 
        if ( current_user_can( 'manage_options' ) ) {
            $students       = get_director_all_students( $posts_per_page , $search_student , $filter_course , $filter_year , $filter_month  );
        } else {
            $students       = '';
        }
        $args_key[] = 'Course #';
        $args_key[] = 'Course';
        $args_key[] = 'Last Name';
        $args_key[] = 'First Name';
        
        if( $is_master_list || $is_teacher_info || $is_financial_aid || $is_t_shirt ) {
            $args_key[] = 'Gender';
        }
        if( $is_master_list || $is_teacher_info || $is_financial_aid ) {
            $args_key[] = 'Date of Birth';
            $args_key[] = 'Ethnicity';
        }
        $args_key[] = 'School Name';
        $args_key[] = 'School District';
        $args_key[] = 'Grade';
        if( $is_master_list || $is_teacher_info || $is_emergeny_contact  ) {
            $args_key[] = 'Transportation';
        }
        if( $is_master_list || $is_teacher_info || $is_t_shirt  ) {
            $args_key[] = 'T-Shirt Size';
        }
        if( $is_master_list || $is_teacher_info || $is_financial_aid || $is_emergeny_contact ) {
            $args_key[] = 'Parent/Guardian Last Name';
            $args_key[] = 'Parent/Guardian First Name';
            $args_key[] = 'Parent/Guardian Email';
            $args_key[] = 'Cell Phone';
        }
        if( $is_master_list || $is_teacher_info || $is_emergeny_contact  ) {
            $args_key[] = 'Address';
            $args_key[] = 'City';
            $args_key[] = 'State/Province';
            $args_key[] = 'Country';
            $args_key[] = 'Postal/Zip Code';
        }
        if( $is_master_list || $is_teacher_info || $is_financial_aid || $is_emergeny_contact ) {
            $args_key[] = 'Parent/Guardian #2 Last Name';
            $args_key[] = 'Parent/Guardian #2 First Name';
            $args_key[] = 'Parent/Guardian #2 Email';
            $args_key[] = 'Cell Phone';
        }
        if( $is_master_list || $is_teacher_info || $is_emergeny_contact  ) {
            $args_key[] = 'Relationship';
            $args_key[] = 'Emergency Name';
            $args_key[] = 'Primary Phone Number';
            $args_key[] = 'Health Concerns?';
            $args_key[] = 'List Health Concerns';
            $args_key[] = 'Taking Medications?';
            $args_key[] = 'Type 1 Diabetes?';
        }
        if( $is_master_list || $is_teacher_info ) {
            $args_key[] = 'Strengths';
            $args_key[] = 'Skills to Improve';
            $args_key[] = 'Anything Else to Know';
        }
        if( $is_master_list || $is_financial_aid ) {
            $args_key[] = 'Tuition Paid';
            $args_key[] = 'Tuition Owed';
            $args_key[] = 'Financial Aid Request';
            $args_key[] = 'Financial Aid Received';
        }
        $keys = $args_key;

        ob_clean();

        fputcsv( $handle, $keys );
        
        if ( $students ) :
            foreach ( $students as $student_key => $student ) : 
                $stu_orders     = get_student_order($student,true,true);
                if( $stu_orders ) {
                    foreach ($stu_orders as $stu_orders_key => $stu_orders_value) {
                        $order  = wc_get_order( $stu_orders_value ); 
                        $class_id = '';
                        foreach ($order->get_items() as $item_id => $item ) {
                            if( $item->get_meta( 'Student' ) == $student->ID ) { 
                                $meta                   = [];
                                $product                = $item->get_product();
                                $pro_id                 = $item->get_product_id();
                                $pro_id                 = !empty( $pro_id ) ? $pro_id : '';
                                $pro_name               = $product->get_name();
                                $stu_fname              = $student->get_first_name();
                                $stu_lname              = $student->get_last_name();
                                $stu_gender             = $student->get_gender();
                                $stu_dob                = $student->get_dob();
                                $stu_ethnicity          = $student->get_ethnicity();
                                $stu_school             = $student->get_school();
                                $stu_district           = $student->get_district();
                                $stu_grade              = $student->get_grade();
                                $stu_transportation     = $student->get_transportation();
                                $stu_t_shirt            = $student->get_shirt_size();
                                $stu_health_con_condi   = $student->get_health_concerns_conditional();
                                $stu_health_con         = $student->get_health_concerns();
                                $stu_medications_condi  = $student->get_medications_conditional();
                                $stu_diabetes_condi     = $student->get_diabetes_conditional();
                                $stu_stregths           = $student->get_strengths();
                                $stu_skills_to_improve  = $student->get_skills_to_improve();
                                $stu_anything_else      = $student->get_additional_comments();
                                $tuition_paid           = $product->get_price();
                                $parent_guardian_1      = $item->get_meta( 'Parent/Guardian 1' );
                                $parent_1_fname         = get_post_meta( $parent_guardian_1, 'parent_guardian_first_name', true );
                                $parent_1_lname         = get_post_meta( $parent_guardian_1, 'parent_guardian_last_name', true );
                                $parent_1_phone         = get_post_meta( $parent_guardian_1, 'parent_guardian_phone', true );
                                $parent_1_email         = get_post_meta( $parent_guardian_1, 'parent_guardian_email', true );
                                $parent_1_relation      = get_post_meta( $parent_guardian_1, 'parent_guardian_relationship', true );
                                $parent_guardian_2      = $item->get_meta( 'Parent/Guardian 2' );
                                $parent_2_fname         = get_post_meta( $parent_guardian_2, 'parent_guardian_first_name', true );
                                $parent_2_lname         = get_post_meta( $parent_guardian_2, 'parent_guardian_last_name', true );
                                $parent_2_phone         = get_post_meta( $parent_guardian_2, 'parent_guardian_phone', true );
                                $parent_2_email         = get_post_meta( $parent_guardian_2, 'parent_guardian_email', true );
                                $emergency_name         = get_student_emergency_name( $student );
                                $emergency_phone        = get_student_emergency_phone( $student );
                                $emergency_email        = get_student_emergency_email( $student );
                                $address                = get_student_order_without_html_address($student);
                                $financial_aid_request  = ( get_order_pending_financial_aid_amount( $stu_orders_value ) ? get_order_pending_financial_aid_amount( $stu_orders_value ) : 0 );
                                $financial_aid_approve  = ( get_order_financial_aid_amount( $stu_orders_value ) ? get_order_financial_aid_amount( $stu_orders_value ) : 0 );
                                $address                = get_student_order_user_address( $student );
                                $city                   = get_student_order_user_city( $student );
                                $state                  = get_student_order_user_state( $student );
                                $country                = get_student_order_user_country( $student );
                                $postal_code            = get_student_order_user_postal_code( $student );
                                $tuition_owed           = $item->get_total();
                                $tuition_owed           = $order->get_total();
                                
                                $meta[] =  esc_html($pro_id);
                                $meta[] =  ($pro_name ? $pro_name : '-' );
                                $meta[] =  ($stu_lname ? $stu_lname : '-' );
                                $meta[] =  ($stu_fname ? $stu_fname : '-' );
                                if( $is_master_list || $is_teacher_info || $is_financial_aid || $is_t_shirt ) {
                                    $meta[] =  ($stu_gender ? $stu_gender : '-' );
                                }
                                if( $is_master_list || $is_teacher_info || $is_financial_aid ) {
                                    $meta[] =  ($stu_dob ? $stu_dob : '-' );
                                    $meta[] =  ($stu_ethnicity ? $stu_ethnicity : '-' );
                                }
                                $meta[] =  ($stu_school ? $stu_school : '-' );
                                $meta[] =  ($stu_district ? $stu_district : '-' );
                                $meta[] =  ($stu_grade ? $stu_grade : '-' );
                                if( $is_master_list || $is_teacher_info || $is_emergeny_contact  ) {
                                    $meta[] =  ($stu_transportation ? $stu_transportation : '-' );
                                }
                                if( $is_master_list || $is_teacher_info || $is_t_shirt  ) {
                                    $meta[] =  ($stu_t_shirt ? $stu_t_shirt : '-' );
                                }
                                if( $is_master_list || $is_teacher_info || $is_financial_aid || $is_emergeny_contact ) {
                                    $meta[] =  ($parent_1_lname ? $parent_1_lname : '-' );
                                    $meta[] =  ($parent_1_fname ? $parent_1_fname : '-' );
                                    $meta[] =  ($parent_1_email ? $parent_1_email : '-' );
                                    $meta[] =  ($parent_1_phone ? $parent_1_phone : '-' );
                                }
                                if( $is_master_list || $is_teacher_info || $is_emergeny_contact  ) {
                                    $meta[] =  ($address ? $address : '-' );
                                    $meta[] =  ($city ? $city : '-' );
                                    $meta[] =  ($state ? $state : '-' );
                                    $meta[] =  ($country ? $country : '-' );
                                    $meta[] =  ($postal_code ? $postal_code : '-' );
                                }
                                if( $is_master_list || $is_teacher_info || $is_financial_aid || $is_emergeny_contact ) {
                                    $meta[] =  ($parent_2_lname ? $parent_2_lname : '-' );
                                    $meta[] =  ($parent_2_fname ? $parent_2_fname : '-' );
                                    $meta[] =  ($parent_2_email ? $parent_2_email : '-' );
                                    $meta[] =  ($parent_2_phone ? $parent_2_phone : '-' );
                                }
                                if( $is_master_list || $is_teacher_info || $is_emergeny_contact  ) {
                                    $meta[] =  ($parent_1_relation ? $parent_1_relation : '-' );
                                    $meta[] =  ($emergency_name ? $emergency_name : '-' );
                                    $meta[] =  ($emergency_phone ? $emergency_phone : '-' );
                                    $meta[] =  ($stu_health_con_condi ? $stu_health_con_condi : '-' );
                                    $meta[] =  ($stu_health_con ? $stu_health_con : '-' );
                                    $meta[] =  ($stu_medications_condi ? $stu_medications_condi : '-' );
                                    $meta[] =  ($stu_diabetes_condi ? $stu_diabetes_condi : '-' );
                                }
                                if( $is_master_list || $is_teacher_info ) {
                                    $meta[] =  ($stu_stregths ? $stu_stregths : '-' );
                                    $meta[] =  ($stu_skills_to_improve ? $stu_skills_to_improve : '-' );
                                    $meta[] =  ($stu_anything_else ? $stu_anything_else : '-' );
                                }
                                if( $is_master_list || $is_financial_aid ) {
                                    $meta[] =  ($tuition_paid ? $tuition_paid : 0 );
                                    $meta[] =  ($tuition_owed ? $tuition_owed : 0 );
                                    $meta[] =  ($financial_aid_request ? $financial_aid_request : 0 );
                                    $meta[] =  ($financial_aid_approve ? $financial_aid_approve : 0 );
                                }
                                fputcsv( $handle, $meta );
                            }
                        }
                    }
                }

            endforeach; 
        endif;
        
        ob_flush();
        fclose( $handle );
        exit;
    }
}

return new WC_Student_Registration_Form_Handler;