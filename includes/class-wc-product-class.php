<?php
/**
 * WC_Product_Class class file
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration;

class WC_Product_Class extends \WC_Product {

    /**
     * Get internal type.
     *
     * @return string
     */
    public function get_type() {
        return 'class';
    }

    /**
	 * Checks if a product is virtual (has no shipping).
	 *
	 * @return bool
	 */
    public function is_virtual() {
        return true;
    }

    /**
	 * Check if a product is sold individually (no quantities).
	 *
	 * @return bool
	 */
    public function is_sold_individually() {
        return false;
    }

}
