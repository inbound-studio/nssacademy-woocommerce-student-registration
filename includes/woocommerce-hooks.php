<?php
/**
 * WooCommerce action and filter hooks
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration;

use function WC_Student_Registration\Functions\get_user_students;
use function WC_Student_Registration\Functions\get_student;
use function WC_Student_Registration\Functions\get_school_districts_options_array;
use function WC_Student_Registration\Functions\get_order_school_district;
use function WC_Student_Registration\Functions\get_out_of_district_id;
use function WC_Student_Registration\Functions\get_user_parents_guardians;
use function WC_Student_Registration\Functions\get_school_district_name;
use function WC_Student_Registration\Functions\get_order_students;
use function WC_Student_Registration\Functions\get_student_order;
use function WC_Student_Registration\Functions\get_student_has_order;
use function WC_Student_Registration\Functions\get_all_order_has_district;
use function WC_Student_Registration\Functions\get_order_pending_financial_aid_amount;

// Remove add to cart notices and errors
add_filter( 'wc_add_to_cart_message_html', '\__return_false' );

/**
 * Redirect to cart on add to cart
 *
 * @param string $redirect
 * @return string
 */
function wc_student_registration_add_to_cart_redirect( $redirect ) {
    return wc_get_cart_url();
}
add_filter( 'woocommerce_add_to_cart_redirect', __NAMESPACE__ . '\wc_student_registration_add_to_cart_redirect' );

/**
 * Add new product type Class
 *
 * @param array $types
 * @return array
 */
function wc_student_registration_product_type_class( $types ) {
    $types['class'] = __( 'Class Registration', 'wc-student-registration' );

    return $types;
}
add_filter( 'product_type_selector', __NAMESPACE__ . '\wc_student_registration_product_type_class' );

/**
 * Register new product class
 *
 * @param string $classname
 * @param string $product_type
 * @return string
 */
function wc_student_registration_product_class( $classname, $product_type ) {
    if ( $product_type == 'class' ) {
        $classname = __NAMESPACE__ . '\WC_Product_Class';
    }

    return $classname;
}
add_filter( 'woocommerce_product_class', __NAMESPACE__ . '\wc_student_registration_product_class', 10, 2 );

/**
 * Modify product data tabs
 *
 * @param array $tabs
 * @return array
 */
function wc_student_registration_product_data_tabs( $tabs ) {
    //$tabs['general']['class'] = array_merge( $tabs['general']['class'], [ 'show_if_class' ] );
    $tabs['inventory']['class'] = array_merge( $tabs['inventory']['class'], [ 'show_if_class' ] );

    return $tabs;
}
add_filter( 'woocommerce_product_data_tabs', __NAMESPACE__ . '\wc_student_registration_product_data_tabs' );

/**
 * Gravity flow order status pending
 *
 * @param string $status
 * @param integer $order_id
 * @param WC_Order $order
 * @return string
 */
function wc_gravity_flow_order_status( $status, $order_id, $order ) {
	if ( $order->get_payment_method() === 'financial_aid_pay_later' ) {
		return 'pending';
	}

	return $status;
}
add_filter( 'woocommerce_payment_complete_order_status', __NAMESPACE__ . '\wc_gravity_flow_order_status', 10, 3 );

/**
 * Add our conditional field tags as attributes
 *
 * @param array $args
 * @return array
 */
function wc_form_field_args( $args ) {
	if ( isset( $args['condition'] ) ) {
		$args['custom_attributes'] = $args['custom_attributes'] ?? [];

		$args['custom_attributes']['data-condition'] = json_encode( $args['condition'] );
	}

	return $args;
}
add_filter( 'woocommerce_form_field_args', __NAMESPACE__ . '\wc_form_field_args' );

/**
 * Replace checkout field values with ones from the session
 * 
 * @param string $value
 * @param string $key
 * @return string
 */
function wc_checkout_get_value( $value, $key ) {
    if ( substr( $key, 0, 8 ) === 'billing' || substr( $key, 0, 8 ) === 'account' ) {
        return null;
    }

    $_value = WC()->session->get( 'wc_student_registration_checkout_field_' . $key );

    if ( $_value ) {
        $value = $_value;
    }

    return $value;
}
add_filter( 'woocommerce_checkout_get_value', __NAMESPACE__ . '\wc_checkout_get_value', 10, 2 );

/**
 * During checkout if selecting a previously registered student, use those values
 *
 * @param string $value
 * @param string $key
 * @return string
 */
function wc_checkout_get_student_value( $value, $key ) {
    $student_id = WC()->session->get( 'wc_student_registration_checkout_field_student_selected' );
	
	if ( $student_id ) {
		return get_post_meta( $student_id, $key, true );
	}
	
	return $value;
}
add_filter( 'woocommerce_checkout_get_value', __NAMESPACE__ . '\wc_checkout_get_student_value', 15, 2 );

/**
 * Custom checkout fields
 *
 * @param array $fields
 * @return array
 */
function wc_student_registration_checkout_fields( $fields ) {
	$_fields = $fields;
	$fields  = [];

	$phone = isset( $fields['billing'] ) ? $fields['billing']['billing_phone'] : [];

	/**
	 * Step 1
	 */
	$fields['billing'] = [
		'title'						=> [
			'label'					=> __( 'Registration', 'wc-student-registration' ),
			'type'					=> 'title'
		],
		'billing_first_name'		=> $_fields['billing']['billing_first_name'],
		'billing_last_name'			=> $_fields['billing']['billing_last_name'],
		'billing_email'				=> $_fields['billing']['billing_email']
	];

	// User fields
	if ( ! is_user_logged_in() && WC()->checkout->is_registration_required() ) {
		$fields['billing'] = array_merge( $fields['billing'], $_fields['account'] );
	}

	$fields['billing']['billing_phone'] = [
		'label'				=> __( 'Phone', 'wc-student-registration' ),
		'required'			=> true,
		'type'				=> 'text',
		'priority' 			=> 120,
	];

	$fields['billing']['billing_relationship'] = [
		'label'				=> __( 'Relationship to student', 'wc-student-registration' ),
		'required'			=> true,
		'type'				=> 'text',
		'priority' 			=> 130,
	];

	$fields['billing']['carpool'] = [
		'label'				=> __( 'Would you like to be included in the carpool lists?', 'wc-student-registration' ),
		'required'			=> true,
		'type'				=> 'radio',
		'description'		=> __( 'By the end of April, the names of those students who wish to become part of car pools will be mailed. This list will include names, addresses, and phone numbers of those students who live within the same school district. If you wish to participate, you will need to make your own contacts to form car pools.', 'wc-student-registration' ),
		'options'			=> [ 'yes' => __( 'Yes', 'wc-student-registration' ), 'no' => __( 'No', 'wc-student-registration' ) ],
		'priority' 			=> 140,
	];

	$fields['billing']['financial_aid_title'] = [
		'label'				=> __( 'Financial Aid', 'wc-student-registration' ),
		'type'				=> 'title',
		'priority'			=> 150,
	];

	$fields['billing']['financial_aid'] = [
		'label'				=> __( 'Would you like to apply for financial aid?', 'wc-student-registration'),
		'required'			=> true,
		'description'		=> __( 'Financial aid, where available, covers only a portion of the total tuition. It is the parent\'s responsibility to pay the remaining balance by the April 12 deadline.', 'wc-student-registration' ),
		'type'				=> 'radio',
		'options'			=> [
			'yes'			=> __( 'Yes', 'wc-student-registration' ),
			'no'			=> __( 'No', 'wc-student-registration' )
		],
		'priority'			=> 160,
	];

	return $fields;
}
add_filter( 'woocommerce_checkout_fields', __NAMESPACE__ . '\wc_student_registration_checkout_fields' );

function wc_registration_form() {
	?>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="reg_fname"><?php esc_html_e( 'Parent/Gaurdian First name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="fname" id="reg_fname" value="<?php echo (!empty($_POST['fname'])) ? esc_attr(wp_unslash($_POST['fname'])) : ''; ?>" />
	</p>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="reg_lname"><?php esc_html_e( 'Parent/Gaurdian Last name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="lname" id="reg_lname"  value="<?php echo (!empty($_POST['lname'])) ? esc_attr(wp_unslash($_POST['lname'])) : ''; ?>"/>
	</p>
<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="address"><?php esc_html_e( 'Parent/Gaurdian Address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="address" id="address"  value="<?php echo (!empty($_POST['address'])) ? esc_attr(wp_unslash($_POST['address'])) : ''; ?>"/>
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="reg_phone"><?php esc_html_e( 'Phone', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<input type="tel" class="woocommerce-Input woocommerce-Input--text input-text" name="phone" id="reg_phone" pattern="[0-9]{3}[0-9]{3}[0-9]{4}" maxlength="10"  value="<?php echo (!empty($_POST['phone'])) ? esc_attr(wp_unslash($_POST['phone'])) : ''; ?>"/>
                <span class="woocommerce-input-hint-msg">This entry can only contain numbers (i.e. 9999999999 )</span>
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="reg_relationship"><?php esc_html_e( 'Relationship', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="relationship" id="reg_relationship"  value="<?php echo (!empty($_POST['relationship'])) ? esc_attr(wp_unslash($_POST['relationship'])) : ''; ?>"/>
	</p>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<div class="carpool_main_txt"><input type="checkbox" class="woocommerce-Input woocommerce-Input--checkbox input-checkbox" name="carpool" id="reg_carpool"  <?php echo ( !empty($_POST['carpool']) ) ? "checked" : '';?> />
		<label for="reg_carpool"><?php esc_html_e( 'I would like to be included in the carpool list', 'woocommerce' ); ?></label></div>
		<span class="description"><?php esc_html_e( 'By the end of April, the names of those students who wish to become part of car pools will be mailed. This list will include names, addresses, and phone numbers of those students who live within the same school district. If you wish to participate, you will need to make your own contacts to form car pools.', 'wc-student-registration' ); ?></span>
	</p>

	<?php
}
add_action( 'woocommerce_register_form', __NAMESPACE__ . '\wc_registration_form' );

/**
 * To validate registration form 
 */
add_action( 'woocommerce_register_post', __NAMESPACE__ . '\wc_custom_validate_fields', 10, 3 );
 
function wc_custom_validate_fields( $username, $email, $errors ) {
 
    if ( empty( $_POST['fname'] ) ) {
        $errors->add( 'fname_error', 'First name is required!' );
    }
    if ( empty( $_POST['lname'] ) ) {
        $errors->add( 'lname_error', 'Last name is required!' );
    }
	 if ( empty( $_POST['address'] ) ) {
      $errors->add( 'address_error', 'Address is required!' );
     }
     if ( empty( $_POST['phone'] ) ) {
      $errors->add( 'phone_error', 'Phone is required!' );
     }
	if ( empty( $_POST['relationship'] ) ) {
      $errors->add( 'relationship_error', 'Relationship is required!' );
     }
}

function wc_registration_form_process( $customer_id, $new_customer_data, $password_generated ) {
	$fname = isset( $_POST['fname'] ) ? $_POST['fname'] : '';
	$lname = isset( $_POST['lname'] ) ? $_POST['lname'] : '';
	$phone = isset( $_POST['phone'] ) ? $_POST['phone'] : '';
	$address = isset( $_POST['address'] ) ? $_POST['address'] : '';
	$relationship = isset( $_POST['relationship'] ) ? $_POST['relationship'] : '';
	$carpool = isset( $_POST['carpool'] ) ? $_POST['carpool'] : '';

	if ( empty( $fname ) ) {
		return;
	}

	$customer = get_userdata( $customer_id );

	wp_update_user( [
		'ID'			=> $customer_id,
		'first_name'	=> $fname,
		'last_name'		=> $lname
	] );

	update_user_meta( $customer_id, 'billing_first_name', $fname );
	update_user_meta( $customer_id, 'billing_last_name', $lname );
	update_user_meta( $customer_id, 'billing_phone', $phone );
	update_user_meta( $customer_id, 'address', $address );
	update_user_meta( $customer_id, 'billing_relationship', $relationship );
	
	if ( ! empty( $carpool ) ) {
		update_user_meta( $customer_id, 'carpool', 'yes' );
	}

	if ( empty( get_user_parents_guardians( $customer_id ) ) ) {
		wp_insert_post( [
			'post_type'			=> 'parent_guardian',
			'post_status'		=> 'publish',
			'post_author'		=> $customer_id,
			'meta_input'		=> [
				'parent_guardian_first_name'		=> $fname,
				'parent_guardian_last_name'			=> $lname,
				'parent_guardian_email'				=> $customer->user_email,
				'parent_guardian_phone'				=> $phone, 
				'parent_guardian_relationship'		=> $relationship, 
				'address'		=> $address, 
			]
		] );
	}
}
add_action( 'woocommerce_created_customer', __NAMESPACE__ . '\wc_registration_form_process', 10, 3 );

/**
 * Add the step title
 *
 * @param string $field
 * @param string $key
 * @param array $args
 * @param string $value
 * @return string
 */
function wc_checkout_step_title( $field, $key, $args, $value ) {
	return '<h3 class="step-title">' . esc_html__( $args['label'], 'wc-student-registration' ) . '</h3>';
}
add_filter( 'woocommerce_form_field_title', __NAMESPACE__ . '\wc_checkout_step_title', 10, 4 );

/**
 * Redirect shop to classes page
 *
 * @param string $redirect
 * @return string
 */
function wc_shop_redirect( $redirect ) {
	return home_url( '/classes/' );
}
add_filter( 'woocommerce_return_to_shop_redirect', __NAMESPACE__ . '\wc_shop_redirect' );

/**
 * Apply a coupon to give discount depending on school district
 *
 * @param WC_Cart $cart
 * @return void
 */
function wc_apply_school_district_coupon( $cart ) {
	$school_district = WC()->session->get( 'wc_student_registration_school_district' );

	if ( ! empty( $school_district ) ) {
		$school_district_term = get_term( $school_district, 'school_district' );

		if ( $school_district_term ) {
			$coupon = get_term_meta( $school_district_term->term_id, 'class_discount', true );
			$coupon = new \WC_Coupon( $coupon );

			if ( $coupon && $coupon->get_code() ) {
				if ( ! WC()->cart->has_discount( $coupon->get_code() ) ) {
					WC()->cart->apply_coupon( $coupon->get_code() );
				}
			} else {
				WC()->cart->remove_coupons();
			}
		}
	}
}
add_action( 'woocommerce_before_calculate_totals', __NAMESPACE__ . '\wc_apply_school_district_coupon' );

/**
 * Add checkout button to order review step
 *
 * @return void
 */
function wc_checkout_back_button() {
	?>

	<a href="<?php print esc_url( add_query_arg( [ 'wcsr_step' => 'back' ] ), home_url() ); ?>" class="button alt" value="<?php esc_attr_e( 'Back', 'wc-student-registration' ); ?>" data-value="<?php esc_attr_e( 'Back', 'wc-student-registration' ); ?>">
		<?php esc_html_e( 'Back', 'wc-student-registration' ); ?>
	</a>

	<?php
}
add_action( 'woocommerce_review_order_before_submit', __NAMESPACE__ . '\wc_checkout_back_button' );

/**
 * Translate some strings
 *
 * @param string $translated_text
 * @param string $text
 * @param string $text_domain
 * @return string
 */
function wc_string_translations( $translated_text, $text, $text_domain ) {
	if ( $text_domain === 'woocommerce' ) {
		switch ( $translated_text ) {
			case '(can be backordered)' :
				$translated_text = __( '(can be added to waiting list', 'wc-student-registration' );
				break;
			case 'Allow backorders?' :
				$translated_text = __( 'Allow waiting list?', 'wc-student-registration' );
				break;
			case 'Backordered' :
				$translated_text = __( 'On waiting list', 'wc-student-registration' );
				break;
			case 'Available on backorder' :
				$translated_text = __( 'Will be added to waiting list', 'wc-student-registration' );
				break;
		}
	}

	return $translated_text;
}
add_filter( 'gettext', __NAMESPACE__ . '\wc_string_translations', 10, 3 );

/**
 * If password is required to add this product to cart, redirect them to the password page
 * In addition, remove the current added product from cart, and store it in session to be added later
 *
 * @param string $cart_item_key
 * @param integer $product_id
 * @param integer $quantity
 * @param integer $variation_id
 * @param array $variation
 * @param array $cart_item_data
 * @return void
 */
function wc_password_check_add_to_cart( $cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data ) {
	$product = wc_get_product( $product_id );

	$password = false;

	$given_password = isset( $_REQUEST['course_registration_password'] ) ? $_REQUEST['course_registration_password'] : '';
        if( $product_id == 5397 || $product_id == 5423 || $product_id == '6236' ||  $product_id == '6235' ||  $product_id == '6234'   ) {
                $pre_reg_timestamp = defined( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_EXPLORING_PRE_REG_TIMESTAMP' ) ? constant( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_EXPLORING_PRE_REG_TIMESTAMP' ) : '';
                $normal_timestamp = defined( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_EXPLORING_REG_TIMESTAMP' ) ? constant( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_EXPLORING_REG_TIMESTAMP' ) : '';
        } else {
                $pre_reg_timestamp = defined( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PRE_REG_TIMESTAMP' ) ? constant( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PRE_REG_TIMESTAMP' ) : '';
                $normal_timestamp = defined( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_REG_TIMESTAMP' ) ? constant( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_REG_TIMESTAMP' ) : '';
        }

	$pre_reg_password = defined( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PRE_REG_PASSWORD' ) ? constant( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PRE_REG_PASSWORD' ) : '';
	$normal_password = defined( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PASSWORD' ) ? constant( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PASSWORD' ) : '';

	if ( $product instanceof WC_Product_Class && ( ! isset( $_COOKIE['wcsr_course_registration_password'] ) || $_COOKIE['wcsr_course_registration_password'] !== 'yes' ) && !empty( $given_password ) ) {
		
		/**
		 * Pre registration password check
		 */
		if ( $given_password === $pre_reg_password ) { // Does the given password match the pre-reg password
			if ( time() < $pre_reg_timestamp ) {
				throw new \Exception( sprintf( __( 'This password is not yet active until %s.', 'wc-student-registration' ), date( 'F j, Y \a\t g:i a', $pre_reg_timestamp ) ) );
			} else {
				$password = true;
			}
		}

		if ( ! $password ) {
			/**
			 * Normal registration password check
			 */
			if ( $given_password === $normal_password ) { // Does the given password match the normal registration password
				if ( time() < $normal_timestamp ) {
					throw new \Exception( sprintf( __( 'This password is not yet active until %s.', 'wc-student-registration' ), date( 'F j, Y \a\t g:i a', $normal_timestamp ) ) );
				} else {
					$password = true;
				}
			}
		}

		if ( ! $password ) {
			throw new \Exception( __( 'Incorrect password, please try again.', 'wc-student-registration' ) );
		}

		if ( $password ) {
			setcookie( 'wcsr_course_registration_password', 'yes', time() + 60 * 60 * 24 * 30, '/' );
		}

		if ( ! isset( $cart_item_data['wcsr_no_password'] ) && ! $password ) {
			$cart_item = WC()->cart->get_cart_item( $cart_item_key );

			setcookie( 'wcsr_course_cart_item', base64_encode( json_encode( $cart_item ) ), time() + 60 * 15, '/' );

			WC()->cart->remove_cart_item( $cart_item_key );

			wp_redirect( add_query_arg( [ 'wcsr_password_check' => 1 ], home_url() ) );
			exit;
		}
	}
}
add_action( 'woocommerce_add_to_cart', __NAMESPACE__ . '\wc_password_check_add_to_cart', 0, 6 );

/**
 * Modify WooCommerce account menu items
 *
 * @param array $menu_links
 * @return array
 */
function wc_account_menu_items( $menu_links ){
	unset( $menu_links['edit-address'] );
	unset( $menu_links['payment-methods'] );
	unset( $menu_links['downloads'] );
	unset( $menu_links['customer-logout'] );

	$menu_links['orders'] = __( 'My Orders', 'wc-student-registration' );
	$menu_links['students'] = __( 'My Students', 'wc-student-registration' );
	$menu_links['parents-guardians'] = __( 'Parents/Guardians', 'wc-student-registration' );
	$menu_links['class-registrations'] = __( 'Class Registrations', 'wc-student-registration' );
	$menu_links['support'] = __( 'Support', 'wc-student-registration' );

	asort( $menu_links );

	return $menu_links;
}
add_filter( 'woocommerce_account_menu_items', __NAMESPACE__ . '\wc_account_menu_items' );

/**
 * Register new query vars for endpoint
 *
 * @param array $vars
 * @return array
 */
function wc_query_vars( $vars ) {
	$vars['students'] = 'students';
	$vars['class-registrations'] = 'class-registrations';
	$vars['parents-guardians'] = 'parents-guardians';
	$vars['support'] = 'support';

	return $vars;
}
add_filter( 'woocommerce_get_query_vars', __NAMESPACE__ . '\wc_query_vars' );

/**
 * Class registrations endpoint content
 *
 * @return void
 */
function wc_class_registrations_endpoint() {
	wc_get_template( 'myaccount/class-registrations.php', [ 'current_user' => get_user_by( 'id', get_current_user_id() ) ] );
}
add_action( 'woocommerce_account_class-registrations_endpoint', __NAMESPACE__ . '\wc_class_registrations_endpoint' );

/**
 * Class registrations endpoint content
 *
 * @return void
 */
function wc_students_endpoint() {
	wc_get_template( 'myaccount/students.php', [ 'current_user' => get_user_by( 'id', get_current_user_id() ) ] );
}
add_action( 'woocommerce_account_students_endpoint', __NAMESPACE__ . '\wc_students_endpoint' );

/**
 * Parents/Guardians endpoint content
 *
 * @return void
 */
function wc_parents_guardians_endpoint() {
	wc_get_template( 'myaccount/parents-guardians.php', [ 'current_user' => get_user_by( 'id', get_current_user_id() ) ] );
}
add_action( 'woocommerce_account_parents-guardians_endpoint', __NAMESPACE__ . '\wc_parents_guardians_endpoint' );

/**
 * Support endpoint content
 *
 * @return void
 */
function wc_support_endpoint() {
	wc_get_template( 'myaccount/support.php', [ 'current_user' => get_user_by( 'id', get_current_user_id() ) ] );
}
add_action( 'woocommerce_account_support_endpoint', __NAMESPACE__ . '\wc_support_endpoint' );

/**
 * New title for students account page
 *
 * @param string $title
 * @return string
 */
function wc_students_endpoint_title( $title ) {
	return __( 'My Students', 'wc-student-registration' );
}
add_filter( 'woocommerce_endpoint_students_title', __NAMESPACE__ . '\wc_students_endpoint_title' );

/**
 * New title for class registrations account page
 *
 * @param string $title
 * @return string
 */
function wc_class_registrations_endpoint_title( $title ) {
	return __( 'Class Registrations', 'wc-student-registration' );
}
add_filter( 'woocommerce_endpoint_class-registrations_title', __NAMESPACE__ . '\wc_class_registrations_endpoint_title' );

/**
 * New title for parents/guardians account page
 *
 * @param string $title
 * @return string
 */
function wc_parents_guardians_endpoint_title( $title ) {
	return __( 'Parents/Guardians', 'wc-student-registration' );
}
add_filter( 'woocommerce_endpoint_parents-guardians_title', __NAMESPACE__ . '\wc_parents_guardians_endpoint_title' );

/**
 * New title for support account page
 *
 * @param string $title
 * @return string
 */
function wc_support_endpoint_title( $title ) {
	return __( 'Support', 'wc-student-registration' );
}
add_filter( 'woocommerce_endpoint_support_title', __NAMESPACE__ . '\wc_support_endpoint_title' );

/**
 * Remove checkout session variables after adding product to cart
 *
 * @param string $cart_item_key
 * @return void
 */
function wc_reduce_stock_add_to_cart( $cart_item_key ) {
	WC()->session->set( 'wc_student_registration_checkout_step', null );
	WC()->session->set( 'wc_student_registration_checkout_num_steps', null );
}
add_action( 'woocommerce_add_to_cart', __NAMESPACE__ . '\wc_reduce_stock_add_to_cart' );

/**
 * Split cart items by quantity
 *
 * @param string $cart_item_key
 * @param integer $product_id
 * @param integer $quantity
 * @param integer $variation_id
 * @param WC_Product_Variation $variation
 * @param array $cart_item_data
 * @return void
 */
function wc_split_cart_items_by_quantity( $cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data ) {
    if ( $quantity == 1 ) return;

    // Keep the product but set its quantity to 1
    WC()->cart->set_quantity( $cart_item_key, 1 );

    // Loop through each unit of item quantity
    for ( $i = 1; $i <= $quantity -1; $i++ ) {
        // Make each quantity item unique and separated
        $cart_item_data['unique_key'] = md5( microtime() . mt_rand( 10000, 99999 ) );

        // Add each item quantity as a separated cart item
        WC()->cart->add_to_cart( $product_id, 1, $variation_id, $variation, $cart_item_data );
    }
}
add_action( 'woocommerce_add_to_cart', __NAMESPACE__ . '\wc_split_cart_items_by_quantity', 10, 6 );

/**
 * Unique product cart key to separate line items
 *
 * @param array $cart_item_data
 * @param integer $product_id
 * @param integer $variation_id
 * @param integer $quantity
 * @return array
 */
function wc_unique_product_cart_key( $cart_item_data, $product_id, $variation_id, $quantity ) {
    if ( ! isset( $cart_item_data['unique_key'] ) ) {
        // Make this item unique
        $cart_item_data['unique_key'] = md5( microtime() . rand() . mt_rand( 10000, 99999 )  );
	}
	
    return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', __NAMESPACE__ . '\wc_unique_product_cart_key', 10, 4 );


add_filter( 'woocommerce_add_to_cart_sold_individually_found_in_cart', '__return_false' );

/**
 * Updates the class student and parent/guardian information
 *
 * @return void
 */
function update_class_student() {
	if ( isset( $_POST ) && isset( $_POST['wcsr_action'] ) && $_POST['wcsr_action'] == 'update_class_student' ) {
		if ( ! wp_verify_nonce( $_POST['update-class-student-nonce'], 'update_class_student' ) ) {
			wc_add_notice( __( 'Your session has expired, please try again.', 'wc-student-registration' ), 'error' );
			return;
		}
                $order_id = isset( $_POST['order'] ) ? ( $_POST['order'] ) : false;
		$order = isset( $_POST['order'] ) ? wc_get_order( $_POST['order'] ) : false;
		$item_id = isset( $_POST['item_id'] ) ? absint( $_POST['item_id'] ) : false;
		$pro_id = isset( $_POST['pro_id'] ) ? absint( $_POST['pro_id'] ) : false;
		$student = isset( $_POST['student'] ) ? absint( $_POST['student'] ) : false;
		$parent_guardian_1 = isset( $_POST['parent_guardian_1'] ) ? absint( $_POST['parent_guardian_1'] ) : false;
		$parent_guardian_2 = isset( $_POST['parent_guardian_2'] ) ? absint( $_POST['parent_guardian_2'] ) : false;

                $stu_order          = get_student_has_order( $student,$pro_id,$order_id );
                if( $stu_order ) {
                    wc_add_notice( __( 'Student already have this course.Please select other student.', 'wc-student-registration' ), 'error' );
                    wp_redirect( trailingslashit( wc_get_page_permalink( 'myaccount' ) ) . 'class-registrations/' );
                    exit;
                }
                
		if ( $student && get_post_type( $student ) !== false ) {
			if ( $order && $item_id ) {
				foreach ( $order->get_items() as $item ) {
					if ( $item->is_type( 'line_item' ) && $item->get_id() == $item_id ) {
						$product = is_callable( [ $item, 'get_product' ] ) ? $item->get_product() : false;

						if ( $product instanceof WC_Product_Class ) {
							$stu_class = get_post_meta( $student, 'registered_class', true );
                                                        if( !empty( $stu_class ) ) {
                                                            $stu_class_array = explode(',', $stu_class);
                                                            if( !in_array($product->get_id(), $stu_class_array) ) {
                                                                $stu_class = $stu_class .','.$product->get_id();
                                                            }
                                                        } else {
                                                            $stu_class = $product->get_id();
                                                        }
                                                        
							update_post_meta( $student, 'registered_class', $stu_class );
                                                        global $wpdb;
                                                        $time = time();
                                                        $mysql_time_format= "Y-m-d H:i:s";
                                                        $post_modified = gmdate( $mysql_time_format, $time );
                                                        $post_modified_gmt = gmdate( $mysql_time_format, ( $time + get_option( 'gmt_offset' ) * HOUR_IN_SECONDS )  );
                                                        $wpdb->query("UPDATE $wpdb->posts SET post_modified = '{$post_modified}', post_modified_gmt = '{$post_modified_gmt}'  WHERE ID = {$student}" );
							$item->update_meta_data( 'Student', $student, true );
							$item->update_meta_data( 'Parent/Guardian 1', $parent_guardian_1, true );
							$item->update_meta_data( 'Parent/Guardian 2', $parent_guardian_2, true );
							$item->save_meta_data();

							wc_add_notice( __( 'Class registration updated successfully.', 'wc-student-registration' ), 'success' );
						}
					}
				}
			} else {
				wc_add_notice( __( 'Invalid order, please contact an administrator.', 'wc-student-registration' ), 'error' );
			}
		} else {
			wc_add_notice( __( 'Invalid student, please double check the student exists or try creating and adding the student again.', 'wc-student-registration' ), 'error' );
		}

		wp_redirect( trailingslashit( wc_get_page_permalink( 'myaccount' ) ) . 'class-registrations/' );
		exit;
	}
}
add_action( 'wp_loaded', __NAMESPACE__ . '\update_class_student' );

/**
 * Hide coupon notice
 */
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );

/**
 * Change description field class as to not toggle on label click
 *
 * @param string $field
 * @return string
 */
function checkout_form_field( $field ) {
	$field = str_replace( 'class="description', 'class="field-description', $field );

	return $field;
}
add_filter( 'woocommerce_form_field', __NAMESPACE__ . '\checkout_form_field' );

/**
 * Don't automatically cancel unpaid orders
 */
remove_action( 'woocommerce_cancel_unpaid_orders', 'wc_cancel_unpaid_orders' );

/**
 * Add to cart template for classes
 *
 * @return void
 */
function class_add_to_cart_template() {
	wc_get_template( 'single-product/add-to-cart/class.php' );
}
add_action( 'woocommerce_class_add_to_cart', __NAMESPACE__ . '\class_add_to_cart_template', 30 );

/**
 * Add custom product options
 *
 * @return void
 */
function product_options() {
	global $product;
        if( $product->id == 5397 || $product->id == 5423 ) {
            $pre_reg_timestamp = defined( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_EXPLORING_PRE_REG_TIMESTAMP' ) ? constant( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_EXPLORING_PRE_REG_TIMESTAMP' ) : '';
        } else {
            $pre_reg_timestamp = defined( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PRE_REG_TIMESTAMP' ) ? constant( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PRE_REG_TIMESTAMP' ) : '';
        }
	?>

	<div class="product_school_district_field">
            
            <input type="hidden" value="<?php echo $product->id;?>" id="pro_id">
            	<label for="school_district"><?php _e( 'School District', 'wc-student-registration' ); ?> <span class="required">*</span></label>
		
		<?php
                        global $woocommerce;
                        if( WC()->cart->cart_contents_count == 0 ){
                            $prev_school_district = false;
                        } else {
                            $prev_school_district = isset( $_COOKIE['wcsr_school_district'] ) ? absint( $_COOKIE['wcsr_school_district'] ) : '';
                        }

			$out_of_district = get_term_by( 'slug', 'out-of-district', 'school_district' );

			$school_districts = get_terms( [ 
				'taxonomy' 		=> 'school_district', 
				'hide_empty' 	=> false, 
				'orderby' 		=> 'term_order',
				'exclude'		=> $out_of_district ? [ $out_of_district->term_id ] : null
			] );
			?>

			<select name="school_district" id="school_district" required <?php if ( $prev_school_district ) : ?>disabled<?php endif; ?>>
				<option value=""><?php esc_html_e( 'Select an option...', 'wc-student-registration' ); ?></option>

				<?php if ( $out_of_district ) : ?>

				<optgroup label="<?php esc_attr_e( 'Out of district', 'wc-student-registration' ); ?>">
					<option value="<?php print esc_attr( $out_of_district->term_id ); ?>"><?php esc_html_e( $out_of_district->name, 'wc-student-registration' ); ?></option>
				</optgroup>

				<?php endif; ?>

				<optgroup label="<?php esc_attr_e( 'School District', 'wc-student-registration' ); ?>">
					<?php foreach ( $school_districts as $school_district ) : ?>

						<option value="<?php print esc_attr( $school_district->term_id ); ?>" <?php selected( $school_district->term_id, $prev_school_district ); ?>><?php esc_html_e( $school_district->name ); ?></option>

					<?php endforeach; ?>
				</optgroup>

			</select>

			<?php if ( $prev_school_district ) : ?>

				<input type="hidden" name="school_district" value="<?php print esc_attr( $prev_school_district ); ?>" />

			<?php endif; ?>
	</div>
        
	<?php if ( ! isset( $_COOKIE['wcsr_course_registration_password'] ) || $_COOKIE['wcsr_course_registration_password'] !== 'yes' || WC()->cart->cart_contents_count == 0 || time() <= $pre_reg_timestamp  ) : ?>

	<div class="course_registration_password_field">
		<label for="course_registration_password"><?php _e( 'Password', 'wc-student-registration' ); ?> <span class="required">*</span></label>
		<input type="text" id="course_registration_password" name="course_registration_password" value="" required />
	</div>

	<?php endif; ?>

	<?php
}
add_action( 'woocommerce_before_add_to_cart_button', __NAMESPACE__ . '\product_options' );

/**
 * Add custom cart item data
 *
 * @param array $cart_item_data
 * @param integer $product_id
 * @param integer $variation_id
 * @return array
 */
function add_cart_item_data( $cart_item_data, $product_id, $variation_id ) {
	if ( isset( $_POST['school_district'] ) ) {
		$cart_item_data['school_district'] = absint( $_POST['school_district'] );
		setcookie( 'wcsr_school_district', absint( $_POST['school_district'] ), time() + 60 * 60 * 24, '/' );

		WC()->session->set( 'wc_student_registration_school_district', absint( $_POST['school_district'] ) );
	}
	if ( isset( $_POST['wc_deposit_option'] ) ) {
		setcookie( 'wc_deposit_option', ( $_POST['wc_deposit_option'] ), time() + 60 * 60 * 24, '/' );

		WC()->session->set( 'wc_student_registration_deposit_option', ( $_POST['wc_deposit_option'] ) );
	}

	return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', __NAMESPACE__ . '\add_cart_item_data', 10, 3 );

/**
 * Display extra cart item data
 *
 * @param array $item_data
 * @param string $cart_item
 * @return array
 */
function cart_item_data( $item_data, $cart_item ) {
    if ( empty( $cart_item['school_district'] ) ) {
        return $item_data;
	}

	if ( $school_district ) {
		$item_data[] = [
			'key'     	=> __( 'School District', 'wc-student-registration' ),
			'value'   	=> get_school_district_name( $cart_item['school_district'] ),
			'display' 	=> '',
		];
	}
 
    return $item_data;
}
add_filter( 'woocommerce_get_item_data', __NAMESPACE__ . '\cart_item_data', 15, 2 );

/**
 * Return school district name instead of ID
 *
 * @param string $display_value
 * @param array $meta
 * @param WC_Order_item $order_item
 * @return string
 */
function order_item_display_meta_value( $display_value, $meta, $order_item ) {
	if ( empty( $display_value ) ) {
		return $display_value;
	}

	if ( $meta->key == 'school_district' ) {
		$display_value = get_school_district_name( $display_value );
	}

	if ( $meta->key == 'Student' ) {
		$student = new WC_Student( $display_value );

		if ( $student ) {
			$display_value = $student->get_full_name();
		}
	}

	if ( $meta->key == 'Parent/Guardian 1' || $meta->key == 'Parent/Guardian 2' ) {
		$parent_guardian = new WC_Parent_Guardian( $display_value );

		if ( $parent_guardian ) {
			$display_value = $parent_guardian->get_full_name();
		}
	}

	return $display_value;
}
add_filter( 'woocommerce_order_item_display_meta_value', __NAMESPACE__ . '\order_item_display_meta_value', 10, 3 );

/**
 * Format display keys nicely
 *
 * @param string $display_value
 * @param array $meta
 * @param WC_Order_item $order_item
 * @return string
 */
function order_item_display_meta_key( $display_key, $meta, $order_item ) {
	$display_key = ucfirst( preg_replace( '/[-_]/', ' ', $display_key ) );

	return $display_key;
}
add_filter( 'woocommerce_order_item_display_meta_key', __NAMESPACE__ . '\order_item_display_meta_key', 10, 3 );

/**
 * Create order line item data on checkout
 *
 * @param WC_Order_Item $item
 * @param string $cart_item_key
 * @param array $values
 * @param WC_Order $order
 * @return void
 */
function save_cart_item_data( $item, $cart_item_key, $values, $order ) {
    if ( isset( $values['school_district'] ) ) {
        $item->update_meta_data( 'school_district', $values['school_district'] );
    }
}
add_action( 'woocommerce_checkout_create_order_line_item', __NAMESPACE__ . '\save_cart_item_data', 10, 4 );

/**
 * Save order meta
 *
 * @param integer $order_id
 * @param array $data
 * @return void
 */
function save_order_meta( $order_id, $data ) {
	$order = wc_get_order( $order_id );
    
	$school_district = get_order_school_district( $order );

	if ( $school_district ) {
		$order->update_meta_data( 'school_district', $school_district );
		$order->save();
	}
}
add_action( 'woocommerce_checkout_update_order_meta', __NAMESPACE__ . '\save_order_meta', 10, 2 );


add_filter( 'woocommerce_get_cart_contents', function( $cart_contents ) {
	foreach( $cart_contents as $cart_item_key => $cart_item ) {
		$school_district = $cart_item['school_district'] ?? 0;

		$cart_contents[$cart_item_key]['line_subtotal'] = 1;
	}

	return $cart_contents;
} );

/**
 * Upon registration, redirect to My Students page
 *
 * @param string $redirect
 * @return string
 */
function wc_registration_redirect( $redirect ) {
	$redirect = trailingslashit( wc_get_page_permalink( 'myaccount' ) ) . 'students';

	return $redirect;
}
add_filter( 'woocommerce_registration_redirect', __NAMESPACE__ . '\wc_registration_redirect', 15 );

/**
 * Remove Deposit cart item data
 */
if ( class_exists( '\WC_Deposits_Cart_Manager' ) ) {
	remove_filter( 'woocommerce_get_item_data', [ \WC_Deposits_Cart_Manager::get_instance(), 'get_item_data' ], 10, 2 );
}

/**
 * Register new post status type with WordPress
 *
 * @return void
 */
function register_post_statuses() {
    register_post_status( 'wc-pending-fa', [
        'label'                     => __( 'Pending financial aid review', 'wc-student-registration' ),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Pending financial aid review (%s)', 'Pending financial aid review (%s)' )
	] );
}
add_action( 'init', __NAMESPACE__ . '\register_post_statuses' );

/**
 * Register new WooCommerce order statuses
 *
 * @param array $order_statuses
 * @return array
 */
function register_order_status( $order_statuses ) {
    $order_statuses['wc-pending-fa'] = _x( 'Pending financial aid review', 'Order status', 'wc-student-registration' );
 
    return $order_statuses;
}
add_filter( 'wc_order_statuses', __NAMESPACE__ . '\register_order_status' );

/**
 * Register new shop order columns
 *
 * @param array $columns
 * @return array
 */
function shop_order_columns( $columns ) {
	$columns['students'] = __( 'Students', 'wc-student-registration' );

	return $columns;
}
add_filter( 'manage_edit-shop_order_columns', __NAMESPACE__ . '\shop_order_columns' );

/**
 * Display custom shop order column data
 *
 * @param string $column
 * @param integer $post_id
 * @return void
 */
function shop_order_column_data( $column, $post_id ) {
	if ( $column === 'students' ) {
		$students = get_order_students( $post_id );
		
		if ( $students ) {
			$html = [];

			foreach ( $students as $student ) {
				$student = new WC_Student( $student );

				if ( $student ) {
					$html[] = $student->get_full_name();
				}
			}

			print implode( ', ', $html );
		}
	}
}
add_action( 'manage_shop_order_posts_custom_column', __NAMESPACE__ . '\shop_order_column_data', 10, 2 );

/**
 * Display form to modify order item meta
 *
 * @param integer $item_id
 * @param WC_Order_item $item
 * @param WC_Product $product
 * @return void
 */
function order_item_meta_editor( $item_id, $item, $product ) {
	$order = wc_get_order( $item->get_order_id() );

	$students = get_posts( [
		'post_type'         => 'student',
		'post_status'       => 'publish',
		'posts_per_page'    => -1,
		'author'			=> $order->get_user_id()
	] );
	

	$parents_new_var = get_posts( [
		'post_type'         => 'parent_guardian',
		'post_status'       => 'publish',
		'posts_per_page'    => -1,
		'author'			=> $order->get_user_id()
	] );
	
if($parents_new_var){
		$parents_new_var_length = count($parents_new_var);
	}else{
	$parents_new_var_length = 0;
	}
	?>

	<hr>

	<select class="student-search wc-enhanced-select" data-item-id="<?php print esc_attr( $item_id ); ?>" data-placeholder="<?php esc_attr_e( 'Change assigned student', 'wc-student-registration' ); ?>">
		<option value="" selected><?php esc_html_e( 'Change assigned student', 'wc-student-registration' ); ?></option>

		<?php foreach ( $students as $student ) : $student = new WC_Student( $student->ID ); ?>

		<option value="<?php print esc_attr( $student->ID ); ?>"><?php print esc_html( $student->get_full_name() ); ?></option>

		<?php endforeach; ?>
	</select>


	<?php /* <select class="wc-enhanced-select student-search" data-item-id="<?php print esc_attr( $item_id ); ?>" data-placeholder="<?php esc_attr_e( 'Change Assigned Student', 'wc-student-registration' ); ?>" data-allow_clear="true"></select> */ ?>
	<button class="button button-primary save-student"><?php esc_html_e( 'Save Student', 'wc-student-registration' ); ?></button>

	<span class="spinner" style="float: none;"></span>
<hr>


<select class="parent-guardian-1-new-search wc-enhanced-select" data-item-id="<?php print esc_attr( $item_id ); ?>" data-placeholder="<?php esc_attr_e( 'Change assigned Parent/Guardian 1', 'wc-student-registration' ); ?>">
		<option value="" selected><?php esc_html_e( 'Change assigned Parent/Guardian 1', 'wc-student-registration' ); ?></option>
		<?php foreach ( $parents_new_var as $parents_new1 ) : $parents_new1 = new WC_Parent_Guardian( $parents_new1->ID ); ?>

		<option value="<?php print esc_attr( $parents_new1->ID ); ?>"><?php print esc_html( $parents_new1->get_full_name() ); ?></option>

		<?php endforeach; ?>
	</select>


	<?php /* <select class="wc-enhanced-select student-search" data-item-id="<?php print esc_attr( $item_id ); ?>" data-placeholder="<?php esc_attr_e( 'Change Assigned Student', 'wc-student-registration' ); ?>" data-allow_clear="true"></select> */ ?>
	<button class="button button-primary parent-guardian-1-new-search-btn"><?php esc_html_e( 'Save Parent/Guardian 1', 'wc-student-registration' ); ?></button>

	<span class="spinner spinner_parent1" style="float: none;"></span>

	<hr>

	<select class="parent-guardian-2-new-search wc-enhanced-select" data-item-id="<?php print esc_attr( $item_id ); ?>" data-placeholder="<?php esc_attr_e( 'Change assigned Parent/Guardian 2', 'wc-student-registration' ); ?>">
		<option value="" selected><?php esc_html_e( 'Change assigned Parent/Guardian 2', 'wc-student-registration' ); ?></option>
		<?php foreach ( $parents_new_var as $parents_new2 ) : $parents_new2 = new WC_Parent_Guardian( $parents_new2->ID ); ?>

		<option value="<?php print esc_attr( $parents_new2->ID ); ?>"><?php print esc_html( $parents_new2->get_full_name() ); ?></option>

		<?php endforeach; ?>
	</select>


	<?php /* <select class="wc-enhanced-select student-search" data-item-id="<?php print esc_attr( $item_id ); ?>" data-placeholder="<?php esc_attr_e( 'Change Assigned Student', 'wc-student-registration' ); ?>" data-allow_clear="true"></select> */ ?>
	<button class="button button-primary parent-guardian-2-new-search-btn"><?php esc_html_e( 'Save Parent/Guardian 2', 'wc-student-registration' ); ?></button>

	<span class="spinner spinner_parent2" style="float: none;"></span>


	<?php
}
add_action( 'woocommerce_after_order_itemmeta', __NAMESPACE__ . '\order_item_meta_editor', 10, 3 );

/**
 * Add new order actions
 *
 * @param array $actions
 * @return array
 */
function order_actions( $actions ) {
	$actions['send_financial_aid_notifications'] = __( 'Resend financial aid notifications', 'wc-student-registration' );

	return $actions;
}
add_filter( 'woocommerce_order_actions', __NAMESPACE__ . '\order_actions' );

/**
 * Resend financial aid notifications hook
 *
 * @param WC_Order $order
 * @return void
 */
function send_financial_aid_notifications( $order ) {
	WC()->mailer()->emails['WC_Email_Customer_Financial_Aid_Review']->trigger( $order->get_id(), $order );
}
add_action( 'woocommerce_order_action_send_financial_aid_notifications', __NAMESPACE__ . '\send_financial_aid_notifications' );


add_filter( 'woocommerce_add_to_cart_validation', __NAMESPACE__. '\one_product_the_same_cat',10,3);
function one_product_the_same_cat($valid, $product_id, $quantity) {
    global $woocommerce;
    if($woocommerce->cart->cart_contents_count == 0){
         return true;
    }
   
    foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
        $_product = $values['data'];
        
        if( $_product->id == $product_id ) {
            wc_add_notice( 'Already is on cart! Please purchase another course.', 'error' );
            return false;
        } 
    }
    return $valid;
}
/**
 * Handle a custom 'financial_aid' query var to get orders with the 'financial_aid' meta.
 * @param array $query - Args for WP_Query.
 * @param array $query_vars - Query vars from WC_Order_Query.
 * @return array modified $query
 */
function handle_order_custom_query_var( $query, $query_vars ) {
	if ( ! empty( $query_vars['financial_aid'] ) ) {
		$query['meta_query'][] = array(
			'key' => 'financial_aid',
			'value' => esc_attr( $query_vars['financial_aid'] ),
                        'compare'       => '='
		);
	}
	if ( ! empty( $query_vars['school_district'] ) ) {
                $query['meta_query']['relation'] = 'AND';
		$query['meta_query'][] = array(
			'key' => 'school_district',
			'value' => absint( $query_vars['school_district'] ),
                        'compare'       => '='
		);
	}
	return $query;
}
add_filter( 'woocommerce_order_data_store_cpt_get_orders_query',  __NAMESPACE__ . '\handle_order_custom_query_var', 10, 2 );

add_action( 'woocommerce_payment_complete', __NAMESPACE__ . '\order_change_status_function' );
function order_change_status_function( $order_id ) {
    $order = wc_get_order( $order_id );
    if( $order->get_meta( 'financial_aid' ) == 'yes' ) {
        $order->update_status( 'completed' );
    }
    return $order_id;
}
add_action( 'woocommerce_order_status_processing',  __NAMESPACE__ . '\processing_to_completed');
function processing_to_completed($order_id){
    $order = wc_get_order($order_id);
    $order->update_status('completed'); 
    $parent_id = $order->get_parent_id(); 
    if( $parent_id ) {
        $order->set_date_created( time() );
        $order->save();
    }
}

add_action( 'woocommerce_admin_order_data_after_order_details', __NAMESPACE__ . '\add_custom_financial_aid_field_display_admin_order_meta');
function add_custom_financial_aid_field_display_admin_order_meta( $order ){  
    woocommerce_wp_radio( array(
            'id' => 'financial_aid',
            'label' => 'Financial Aid',
            'value' => $financial_aid,
            'options' => array(
                    'no' => 'No',
                    'yes' => 'Yes'
            ),
            'style' => 'width:16px', 
            'wrapper_class' => 'form-field-wide',
            'description' => 'Update only when admin create an order manually'
    ) );
    $school_districts = get_terms( [ 
            'taxonomy' 		=> 'school_district', 
            'hide_empty' 	=> false, 
            'orderby' 		=> 'term_order',
            'fields' 		=> 'id=>name' 
    ] );
    $school_district =  get_post_meta( $order->get_id(), 'school_district', true );
    $school_district1 =  get_order_school_district( $order );
    $school_districts = array('' => 'Select Option') + $school_districts;
    woocommerce_wp_select( array(
            'id' => 'school_district',
            'label' => 'Order School District',
            'value' => $school_district,
            'options' => $school_districts,
            'wrapper_class' => 'form-field-wide',
            'description' => 'Update only when admin create an order manually'
    ) );
 
}

add_action( 'woocommerce_process_shop_order_meta', __NAMESPACE__ . '\save_custom_financial_aid_field_display_admin_order_meta' );
function save_custom_financial_aid_field_display_admin_order_meta( $ord_id ){
    update_post_meta( $ord_id, 'financial_aid', wc_clean( $_POST[ 'financial_aid' ] ) );
    update_post_meta( $ord_id, 'school_district', wc_clean( $_POST[ 'school_district' ] ) );
}