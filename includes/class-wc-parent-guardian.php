<?php
/**
 * Parent/guardian object
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration;

class WC_Parent_Guardian {

	/**
	 * Parent/guardian post ID
	 *
	 * @var integer
	 */
	public $ID;

	/**
	 * Parent/guarian post ID
	 *
	 * @var integer
	 */
	public $id;

	/**
	 * Instance of parent/guardian WP_Post object
	 *
	 * @var WP_Post
	 */
	protected $post;

	public function __construct( $id ) {
		$this->ID    = $id;
		$this->id    = $id;
		$this->post  = get_post( $id );
	}

	/**
	 * Returns WP_Post object of this parent/guardian
	 *
	 * @return WP_Post
	 */
	public function get_post() {
		return $this->post;
	}

	/**
	 * Return associated WP user account ID
	 *
	 * @return integer
	 */
	public function get_account() {
		return absint( $this->get_post()->post_author );
	}

	/**
	 * Return parent/guardian first name
	 *
	 * @return string
	 */
	public function get_first_name() {
		return get_post_meta( $this->ID, 'parent_guardian_first_name', true );
	}

	/**
	 * Return parent/guardian last name
	 *
	 * @return string
	 */
	public function get_last_name() {
		return get_post_meta( $this->ID, 'parent_guardian_last_name', true );
	}

	/**
	 * Return parent/guardian full name
	 *
	 * @return string
	 */
	public function get_full_name() {
		$name = trim( sprintf( '%s %s', $this->get_first_name(), $this->get_last_name() ) );

		if ( empty( $name ) ) {
			$name = __( 'No name set', 'wc-student-registration' );
		}

		return $name;
	}

	/**
	 * Get parent/guardian phone
	 *
	 * @return string
	 */
	public function get_phone() {
		return get_post_meta( $this->ID, 'parent_guardian_phone', true );
	}

	/**
	 * Get parent/guardian email
	 *
	 * @return string
	 */
	public function get_email() {
		return get_post_meta( $this->ID, 'parent_guardian_email', true );
	}
}
