<?php
/**
 * WC_Student_Registration_Checkout class file
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration;

use function WC_Student_Registration\Functions\get_user_parents_guardians;
use function WC_Student_Registration\Functions\get_out_of_district_id;
use function WC_Student_Registration\Functions\get_out_of_district_fee;

class WC_Student_Registration_Checkout extends \WC_Checkout {

    protected $num_steps;

    public function __construct() {
		if ( ! is_admin() ) {
			add_action( 'woocommerce_init',								[ $this, 'init' ] );
			add_action( 'template_redirect', 							[ $this, 'reload' ], -10 );
			add_action( 'woocommerce_checkout_process', 				[ $this, 'process_checkout' ] );
			add_filter( 'woocommerce_checkout_posted_data',				[ $this, 'checkout_posted_data' ] );
			add_filter( 'woocommerce_checkout_fields',					[ $this, 'step_checkout_fields' ], 1000 );
			add_filter( 'woocommerce_available_payment_gateways', 		[ $this, 'available_payment_gateways' ], 1000 );
			add_action( 'woocommerce_after_checkout_validation', 		[ $this, 'checkout_validation' ], 10, 2 );
			add_action( 'woocommerce_checkout_after_fields',			[ $this, 'checkout_buttons' ], 50 );
			add_action( 'woocommerce_checkout_order_processed', 		[ $this, 'payment_complete' ], 10, 3 );
			add_filter( 'woocommerce_get_checkout_order_received_url', 	[ $this, 'order_received_url' ] );
			add_action( 'woocommerce_cart_loaded_from_session', 		[ $this, 'cart_loaded_from_session' ], 1000 );
			add_action( 'woocommerce_before_calculate_totals',			[ $this, 'calculate_totals' ] );
			add_filter( 'woocommerce_coupon_message',					'__return_false' );

			add_action( 'woocommerce_before_checkout_form', function() {
				add_filter( 'woocommerce_get_checkout_url', [ $this, 'checkout_endpoint' ] );
			} );
			
			add_action( 'woocommerce_after_checkout_form', function() {
				remove_filter( 'woocommerce_get_checkout_url', [ $this, 'checkout_endpoint' ] );
			} );
                        
              }
	}

	public function init() {
        if ( ! $this->get_step() ) {
            $this->set_step( 1 );
		}
	}

	/**
	 * Returns the current step
	 *
	 * @return integer
	 */
    public function get_step() {
        return isset( WC()->session ) ? WC()->session->get( 'wc_student_registration_checkout_step' ) : 1;
    }

	/**
	 * Sets the specified step in a session variable
	 *
	 * @param integer $step
	 * @return integer
	 */
    public function set_step( int $step ) {
        WC()->session->set( 'wc_student_registration_checkout_step', $step );

        return $step;
    }

	/**
	 * Returns total number of checkout flow steps
	 *
	 * @return integer
	 */
    public function get_num_steps() {
        return WC()->session->get( 'wc_student_registration_checkout_num_steps' ) + 1;
    }

	/**
	 * Set the total number of checkout flow steps in session variable
	 *
	 * @param integer $count
	 * @return integer
	 */
    public function set_num_steps( int $count ) {
        WC()->session->set( 'wc_student_registration_checkout_num_steps', $count );

        return $count;
    }

	/**
	 * Proceed to next step by setting session variable
	 *
	 * @return integer
	 */
    public function next_step() {
        $step = min( $this->get_step() + 1, $this->get_num_steps() );

        return $this->set_step( $step );
    }

	/**
	 * Is this the first step in the checkout flow
	 *
	 * @return boolean
	 */
    public function is_first_step() {
        return $this->get_step() <= 1 ? true : false;
    }

	/**
	 * Is this the last step in the checkout flow
	 *
	 * @return boolean
	 */
    public function is_last_step() {
        if ( $this->get_step() >= $this->get_num_steps() ) {
            return true;
        }

        return false;
	}

	/**
	 * Check registration password
	 *
	 * @param string $password
	 * @return boolean
	 */
	protected function check_password( $password = '' ) {
		if ( defined( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PASSWORD' ) ) {
			if ( $password == constant( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PASSWORD' ) ) {
				return true;
			}

			return false;
		}

		return true;
	}

	/**
	 * Save all POST variables to the session
	 *
	 * @return void
	 */
    public function save_session() {
		if ( ! is_user_logged_in() && ( $this->is_registration_required() || ! empty( $_POST['createaccount'] ) ) ) {
            $this->process_customer( $this->get_posted_data() );
		}

        foreach ( $_POST as $key => $value ) {
			WC()->session->set( 'wc_student_registration_checkout_field_' . $key, $value );
		}
    }

	/**
	 * Call methods during checkout processing
	 *
	 * @return void
	 */
	public function process_checkout() {
		$this->save_session();

		if ( ! $this->is_last_step() ) {
			add_filter( 'woocommerce_cart_needs_payment', '__return_false' );
		} else {
			add_filter( 'woocommerce_cart_needs_payment', '__return_true' );
		}
	}

	/**
	 * If Gravity Flow pay later payment gateway is enabled and user requests financial aid...
	 * Only show the pay later payment option during checkout
	 *
	 * @param array $payment_gateways
	 * @return array
	 */
	public function available_payment_gateways( $payment_gateways ) {
		global $wp;

		if ( ! isset( $_GET['key'] ) || ! isset( $wp->query_vars['order-pay'] ) ) {
			$financial_aid = WC()->session->get( 'wc_student_registration_checkout_field_financial_aid' );

			if ( $financial_aid == 'yes' && array_key_exists( 'financial_aid_pay_later', $payment_gateways ) ) {
				$payment_gateways = [ 'financial_aid_pay_later' => $payment_gateways['financial_aid_pay_later'] ];
			} else {
				unset( $payment_gateways['financial_aid_pay_later'] );
			}
		} else {
			if ( array_key_exists( 'financial_aid_pay_later', $payment_gateways ) ) {
				unset( $payment_gateways['financial_aid_pay_later'] );
			}
		}

		return $payment_gateways;
	}

	public function checkout_posted_data( $data ) {
		foreach ( $data as $key => $value ) {
			$_value = WC()->session->get( 'wc_student_registration_checkout_field_' . $key );

			if ( $_value ) {
				$data['key'] = $_value;
			}
		}

		return $data;
	}

	public function checkout_validation( $data, $errors ) {
		if ( ! $this->is_last_step() ) {
			$errors = array_filter( $errors->get_error_messages(), function( $key ) {
				return in_array( $key, [ 'terms', 'shipping', 'payment', 'payment_method', 'shipping_method', 'ship_to_different_address', 'woocommerce_checkout_update_totals' ] );
			}, ARRAY_FILTER_USE_KEY );

			if ( empty( $errors ) ) {
				// Go to next step
				$next = $this->next_step();

				wp_send_json( array( 'result' => 'success', 'redirect' => esc_url( add_query_arg( [ 'wcsr_step' => $next ], home_url() ) ) ) );
			}
		}
	}


	function checkout_endpoint() {
		if ( ! $this->is_last_step() ) {
			return esc_url( add_query_arg( [ 'wcsr_step' => $this->get_step() + 1 ], home_url() ) );
		}
	
		return wc_get_page_permalink( 'checkout' );
	}

	/**
	 * Display checkout fields based on our current checkout flow step
	 *
	 * @param array $fields
	 * @return array
	 */
	public function step_checkout_fields( $fields ) {
		$fields = array_filter( $fields, function( $value ) {
			return (bool) sizeof( $value );
		} );

		$this->set_num_steps( sizeof( $fields ) );

		if ( $this->is_last_step() && ! $this->is_first_step() ) {
			return $fields;
		}

		$sections = array_keys( $fields );

		$step = $this->get_step();

		if ( $step ) {
			$fields = [ $sections[ $step - 1 ] => $fields[ $sections[ $step - 1 ] ] ];
		}

		return $fields;
	}

	/**
	 * Clear users session after payment
	 *
	 * @return void
	 */
	public function payment_complete( $order_id, $posted_data, $order ) {
		global $current_user;

		WC()->session->set( 'wc_student_registration_checkout_step', null );
		WC()->session->set( 'wc_student_registration_checkout_num_steps', null );
		WC()->session->set( 'wc_student_registration_school_district', null );
		WC()->session->set( 'wc_student_registration_deposit_option', null );

		$financial_aid = WC()->session->get( 'wc_student_registration_checkout_field_financial_aid' ) ?? 'no';

		$order->update_meta_data( 'financial_aid', $financial_aid );
		$order->save();

		setcookie( 'wcsr_school_district', '', time() - 3600, '/' );
		unset( $_COOKIE['wcsr_school_district'] );
                
		setcookie( 'wc_deposit_option', '', time() - 3600, '/' );
		unset( $_COOKIE['wc_deposit_option'] );

		if ( empty( get_user_parents_guardians( get_current_user_id() ) ) ) {
			wp_insert_post( [
				'post_type'			=> 'parent_guardian',
				'post_status'		=> 'publish',
				'post_author'		=> get_current_user_id(),
				'meta_input'		=> [
					'parent_guardian_first_name'		=> $current_user->first_name,
					'parent_guardian_last_name'			=> $current_user->last_name,
					'parent_guardian_email'				=> $current_user->user_email,
					'parent_guardian_phone'				=> WC()->session->get( 'wc_student_registration_checkout_field_billing_phone' ) ?? '', 
					'parent_guardian_relationship'		=> WC()->session->get( 'wc_student_registration_checkout_field_billing_relationship' ) ?? '', 
				]
			] );
		}
                //WC()->session->set( 'wc_student_registration_checkout_field_financial_aid', null );
		wc_empty_cart();
	}

	/**
	 * Change order received URL redirect
	 *
	 * @param string $order_received_url
	 * @return string
	 */
	public function order_received_url( $order_received_url ) {
		return trailingslashit( wc_get_page_permalink( 'myaccount' ) ) . 'class-registrations/?new_order=true';
	}

	/**
	 * Do not allow deposits on financial aid orders
	 *
	 * @param WC_Cart $cart
	 * @return void
	 */
	public function cart_loaded_from_session( $cart ) {
            $flag = true;
            
            if( $flag ) {
            	$financial_aid = WC()->session->get( 'wc_student_registration_checkout_field_financial_aid' );

		if ( sizeof( $cart->cart_contents ) > 0 ) {
			foreach ( $cart->cart_contents as $cart_item_key => $cart_item ) {
                            
				if ( $financial_aid == 'yes' ) {
					$cart->cart_contents[ $cart_item_key ]['is_deposit'] = false;
				} else {
                                    if( $cart->cart_contents[ $cart_item_key ]['deposit_amount'] ) {
                                	$cart->cart_contents[ $cart_item_key ]['is_deposit'] = true;
                                    }
                                }
                        }
		}
            }
	}

	/**
	 * Add out of district fees
	 *
	 * @return void
	 */
	public function calculate_totals() {
		$school_district = WC()->session->get( 'wc_student_registration_school_district' );

		$fee = get_out_of_district_fee();
		$total_fees = 0;

		foreach ( WC()->cart->get_cart() as $item ) {
			if ( is_a( $item['data'], 'WC_Student_Registration\\WC_Product_Class' ) ) {
				$total_fees += $item['quantity'] * $fee;
			}
		}

		if ( $school_district && $school_district == get_out_of_district_id() ) {
			WC()->cart->add_fee( 'Out of District Fee', $total_fees );
		}
	}

	/**
	 * Go to next checkout step (in session) and reload page
	 *
	 * @return void
	 */
	public function reload() {
		if ( isset( $_GET['wcsr_step'] ) ) {
			$step = $_GET['wcsr_step'] === 'back' ? $this->get_step() - 1 : absint( $_GET['wcsr_step'] );
			$load_step = false;

			remove_filter( 'woocommerce_checkout_fields', [ $this, 'step_checkout_fields' ], 1000 );

			$sections = array_values( $this->get_checkout_fields() );

			do {
				$conditions = isset( $sections[ $step - 1 ]['condition'] ) ? (array) $sections[ $step - 1 ]['condition'] : null;

				if ( ! $conditions ) {
					break;
				}

				$matched = 0;

				foreach ( $conditions as $key => $value ) {
					if ( WC()->session->get( 'wc_student_registration_checkout_field_' . $key ) == $value ) {
						$matched++;
					}
				}

				if ( $matched === sizeof( $conditions ) ) {
					break;
				}

				$step++;

			} while ( ! $load_step );

            $this->set_step( $step );

            wp_redirect( wc_get_checkout_url() );
            exit;
		}
	}

	/**
	 * Check step buttons in footer
	 *
	 * @return void
	 */
	function checkout_buttons() {
		?>

		<div class="woocommerce-checkout-step-footer">

			<?php 
			if ( ! $this->is_first_step() ) : ?>

				<a href="<?php print esc_url( add_query_arg( [ 'wcsr_step' => $this->get_step() - 1 ] ) ); ?>" class="button alt" value="<?php esc_attr_e( 'Back', 'wc-student-registration' ); ?>" data-value="<?php esc_attr_e( 'Back', 'wc-student-registration' ); ?>">
					<?php esc_html_e( 'Back', 'wc-student-registration' ); ?>
				</a>

			<?php
			endif;

			if ( ! $this->is_last_step() ) : ?>

			<button type="submit" class="button alt" value="<?php esc_attr_e( 'Next', 'wc-student-registration' ); ?>" data-value="<?php esc_attr_e( 'Next', 'wc-student-registration' ); ?>">
				<?php esc_html_e( 'Next', 'wc-student-registration' ); ?>
			</button>

			<?php
			endif;
			?>

		</div>

		<?php
	}
  
}