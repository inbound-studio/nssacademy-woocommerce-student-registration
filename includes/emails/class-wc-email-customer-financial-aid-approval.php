<?php
/**
 * Financial aid approval email send to customer
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration\Emails;

use function WC_Student_Registration\Functions\get_order_financial_aid_amount;

/**
 * An email sent to customers after financial aid approval
 *
 * @class       WC_Email_Customer_Financial_Aid_Approval
 * @version     3.5.0
 * @package     WooCommerce/Classes/Emails
 * @extends     WC_Email
 */
class WC_Email_Customer_Financial_Aid_Approval extends \WC_Email {

    /**
     * Constructor.
     */
    public function __construct() {
        $this->id                   = 'wc_email_customer_financial_aid_approval';
        $this->customer_email       = true;
        $this->title                = __( 'Financial Aid - Approval', 'wc-student-registration' );
        $this->description          = __( 'Email sent to customer after financial aid has been approved.', 'wc-student-registration' );
        $this->template_html        = 'emails/customer-rep-financial-aid-approval.php';
		$this->template_plain       = 'emails/plain/customer-financial-aid-approval.php';
		$this->placeholders         = [ '{pay-now}' => '' ];

        // Call parent constructor.
        parent::__construct();
    }

    /**
     * Get email subject.
     *
     * @return string
     */
    public function get_default_subject() {
        return __( 'Financial Aid Approved', 'wc-student-registration' );
    }

    /**
     * Get email heading.
     *
     * @return string
     */
    public function get_default_heading() {
        return __( 'Financial Aid Approved', 'wc-student-registration' );
    }

    /**
     * Trigger the sending of this email.
     *
     * @param int            $order_id The order ID.
     * @param WC_Order|false $order Order object.
     */
    public function trigger( $order_id, $order = false ) {
        $this->setup_locale();

        if ( $order_id && ! is_a( $order, 'WC_Order' ) ) {
            $order = wc_get_order( $order_id );
        }

        if ( is_a( $order, 'WC_Order' ) ) {
            $this->object                     = $order;
            $this->recipient                  = $this->object->get_billing_email();
            $this->placeholders['{pay-now}']  = $order->get_checkout_payment_url();
        }

        if ( $this->is_enabled() && $this->get_recipient() && get_order_financial_aid_amount( $order ) > 0 && $order->get_meta( 'financial_aid' ) === 'yes' ) {
            $this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
        }

        $this->restore_locale();
    }

    /**
     * Get content html.
     *
     * @return string
     */
    public function get_content_html() {
        return wc_get_template_html(
            $this->template_html,
            array(
                'order'              => $this->object,
                'email_heading'      => $this->get_heading(),
                'additional_content' => $this->get_additional_content(),
                'sent_to_admin'      => false,
                'plain_text'         => false,
                'email'              => $this,
            )
        );
    }

    /**
     * Get content plain.
     *
     * @return string
     */
    public function get_content_plain() {
        return wc_get_template_html(
            $this->template_plain,
            array(
                'order'              => $this->object,
                'email_heading'      => $this->get_heading(),
                'additional_content' => $this->get_additional_content(),
                'sent_to_admin'      => false,
                'plain_text'         => true,
                'email'              => $this,
            )
        );
    }
}