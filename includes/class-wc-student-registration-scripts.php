<?php
/**
 * WC_Student_Registration_Scripts class file
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration;

class WC_Student_Registration_Scripts {

    public function __construct() {
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
    }

    public function enqueue_scripts() {
        $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

        wp_dequeue_style( 'woocommerce-layout' );
        wp_dequeue_style( 'woocommerce-smallscreen' );
        wp_dequeue_style( 'woocommerce-general' );

        wp_enqueue_style( 'featherlight', WC_STUDENT_REGISTRATION_PLUGIN_URL . 'assets/css/featherlight' . $suffix . '.css', array(), null );

        wp_enqueue_style( 'jquery-ui-datepicker-style', '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css' );
        wp_enqueue_style( 'wc-student-registration-frontend', WC_STUDENT_REGISTRATION_PLUGIN_URL . 'assets/css/frontend' . $suffix . '.css', array(), filemtime( WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'assets/css/frontend' . $suffix . '.css' ) );

        wp_enqueue_script( 'jquery-ui' );
        wp_enqueue_script( 'jquery-ui-datepicker' );
        wp_register_script( 'featherlight', WC_STUDENT_REGISTRATION_PLUGIN_URL . 'assets/js/featherlight' . $suffix . '.js', array( 'jquery' ), true );
        wp_enqueue_script( 'wc-student-registration-frontend', WC_STUDENT_REGISTRATION_PLUGIN_URL . 'assets/js/frontend.js', array( 'jquery', 'featherlight' ), filemtime( WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'assets/js/frontend.js' ), true );

        wp_localize_script( 'wc-student-registration-frontend', 'wcsr', [
            'ajaxurl'           => admin_url( 'admin-ajax.php' ),
            'nonce'             => wp_create_nonce(),
        ] );
    }

    public function admin_enqueue_scripts() {
        $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

        wp_enqueue_script( 'wc-student-registration-admin', WC_STUDENT_REGISTRATION_PLUGIN_URL . 'assets/js/admin' . $suffix . '.js', array( 'jquery', 'selectWoo' ), filemtime( WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'assets/js/admin' . $suffix . '.js' ), true );

        wp_localize_script( 'wc-student-registration-admin', 'wcsr', [
            'ajaxurl'           => admin_url( 'admin-ajax.php' ),
            'nonce'             => wp_create_nonce(),
        ] );
    }

}

return new WC_Student_Registration_Scripts;
