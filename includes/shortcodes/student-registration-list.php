<?php
/**
 * Student registration list shortcode
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration\Shortcodes;

use function WC_Student_Registration\Functions\get_all_students;
use function WC_Student_Registration\Functions\get_students_in_school_district;
use function WC_Student_Registration\Functions\get_director_all_students;
use function WC_Student_Registration\Functions\get_director_students_in_school_district;
use function WC_Student_Registration\Functions\get_district_rep_district;
use function WC_Student_Registration\Functions\get_student_order;
use function WC_Student_Registration\Functions\get_student_registered_class;
use function WC_Student_Registration\Functions\get_student_registered_class_teacher;
use function WC_Student_Registration\Functions\get_student_parent_guardian_1;
use function WC_Student_Registration\Functions\get_student_order_phone;
use function WC_Student_Registration\Functions\get_student_order_email;
use function WC_Student_Registration\Functions\get_student_order_address;
use function WC_Student_Registration\Functions\get_student_order_tuition_balance;
use function WC_Student_Registration\Functions\get_student_order_financial_aid;
use function WC_Student_Registration\Functions\get_student_order_year;
use function WC_Student_Registration\Functions\get_all_products;
use function WC_Student_Registration\Functions\get_all_students_by_year;
use function WC_Student_Registration\Functions\get_order_financial_aid_amount;
use function WC_Student_Registration\Functions\get_order_pending_financial_aid_amount;
use function WC_Student_Registration\Functions\get_student_emergency_phone;
use function WC_Student_Registration\Functions\get_student_emergency_email;
use function WC_Student_Registration\Functions\get_student_registered_class_id;
use function WC_Student_Registration\Functions\get_student_order_without_html_address;
use function WC_Student_Registration\Functions\get_district_all_students;
use function WC_Student_Registration\Functions\get_district_students_in_school_district;
use function WC_Student_Registration\Functions\get_student_emergency_name;


/**
 * Displays a list of student registrations for district reps to view
 *
 * @param array $atts
 * @param string $content
 * @return string
 */
function student_registration_list( $atts, $content = '' ) {
    // Permissions check
    if ( ! current_user_can( 'edit_workflow' ) && ! current_user_can( 'review_financial_aid' ) ) {
        return sprintf( '<p>%s</p>', __( 'You do not have permission to view this content.', 'wc-student-registration' ) );
    }

    if ( current_user_can( 'manage_options' ) ) {
        $students = get_all_students();
    } else {
        $students = get_students_in_school_district( get_district_rep_district( get_current_user_id() ) );
    }

    ob_start();
    ?>

    <p class="student-registration-export">
        <a href="<?php print add_query_arg( [ '_wpnonce' => wp_create_nonce(), 'export-wcsr-reg-list' => 1 ] ); ?>" class="button student-registration-export-trigger fas fa-file-export">
            <?php esc_html_e( 'Export', 'wc-student-registration' ); ?>
        </a>
    </p>

    <table class="student-registration-list">
        <thead>
            <tr>
                <th><?php _e( 'Account Name', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Student Name', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Student Class', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'T-Shirt Size', 'wc-student-registration' ); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if ( $students ) : ?>

                <?php foreach ( $students as $student ) : if ( ! get_student_order( $student ) ) continue; 
                $order = get_student_order( $student->ID,true );
                if( $order ) {
                    $class_id = '';
                    $class_array = array();
                    foreach ($order as $orders_key => $orders_value) {
                        $order = wc_get_order( $orders_value ); 
                        foreach ($order->get_items() as $item_id => $item ) {
                            if( $item->get_meta( 'Student' ) == $student->ID ) { 
                                $class_id       = $item->get_product_id();
                                $class_id       = !empty( $class_id ) ? $class_id : '';
                                $class_array[]  = get_the_title($class_id);
                            }
                        }
                    }
                    $stu_regi_class = $class_array ? implode(',', $class_array) : '';
                } else {
                    $stu_regi_class = '';
                }
                ?>

                <tr>
                    <td><a href="#" data-person-type="account" data-person="<?php print get_the_author_meta( 'ID', $student->get_account() ); ?>"><?php esc_html_e( get_the_author_meta( 'display_name', $student->get_account() ), 'wc-student-registration' ); ?></a></td>
                    <td><a href="#" class="student-info" data-person="<?php print esc_attr( $student->ID ); ?>"><?php esc_html_e( $student->get_full_name(), 'wc-student-registration' ); ?></a></td>
                    <td><?php esc_html_e( $student->get_registered_class() ? $stu_regi_class : '', 'wc-student-registration' ); ?></td>
                    <td><?php esc_html_e( get_post_meta( $student->ID, 'student_shirt_size', true ), 'wc-student-registration' ); ?></td>
                </tr>

                <?php endforeach; ?>

            <?php else : ?>

            <tr>
                <td colspan="6"><?php _e( 'No students found.', 'wc-student-registration' ); ?></td>
            </tr>

            <?php endif; ?>
        </tbody>
    </table>

    <?php 
    return ob_get_clean();
}
add_shortcode( 'wc-student-registration-list', __NAMESPACE__ . '\student_registration_list' );

/**
 * Displays a list of student registrations for district reps to view
 *
 * @param array $atts
 * @param string $content
 * @return string
 */
function student_district_registration_list( $atts, $content = '' ) {
    // Permissions check
    if ( ! current_user_can( 'edit_workflow' ) && ! current_user_can( 'review_financial_aid' ) ) {
        return sprintf( '<p>%s</p>', __( 'You do not have permission to view this content.', 'wc-student-registration' ) );
    }
$current_year_change = date("Y"); 
	$filter_year        = $_GET['filter_year'] ? $_GET['filter_year'] : $current_year_change;
  //  $filter_year        = $_GET['filter_year'];
    $search_student     = $_GET['search_student'];
  
    $paged = get_query_var( 'paged' ) ? get_query_var('paged') : 1 ;
    $posts_per_page = ( !empty($filter_year) || !empty( $search_student ) ) ? 50 : 50 ;
    
    if ( current_user_can( 'manage_options' ) ) {
        $all_students   = get_district_all_students( -1 , $search_student , $filter_year  );
        $students       = get_district_all_students( $posts_per_page , $search_student , $filter_year );
    } else {
       
        $all_students   = get_district_students_in_school_district( get_district_rep_district( get_current_user_id()), -1 , $search_student , $filter_year );
        $students       = get_district_students_in_school_district( get_district_rep_district( get_current_user_id()), $posts_per_page , $search_student , $filter_year );
    }
 
    $post_count = count($all_students);
    $num_pages = ceil($post_count / $posts_per_page);
    if($paged > $num_pages || $paged < 1){
        $paged = $num_pages;
    }
    $page_slug = get_post_field( 'post_name' );
    ob_start();
    ?>
    <div class="student-reg-export-form-wrap">
        <p class="student-registration-export">
            <a href="<?php print add_query_arg( [ '_wpnonce' => wp_create_nonce(), 'export-wcsr-district-reg-list' => 1 ] ); ?>" class="button student-registration-export-trigger fas fa-file-export">
                <?php esc_html_e( 'Export', 'wc-student-registration' ); ?>
            </a>
        </p>
        <form method="get" action="/<?php echo $page_slug;?>/">
            <input type="text" name="search_student" placeholder="Search.." id="search_student" value="<?php echo  $search_student;?>" >
            <select name="filter_year">
                <option value="">Select Year</option>
                <?php $this_year = date("Y"); 
                for ($year = $this_year; $year >= $this_year - 20 ; $year--) { ?>
                    <option value="<?php echo $year;?>" <?php echo ( $filter_year == $year ) ? 'selected' : '';?>><?php echo $year;?></option>
                <?php } ?>
            </select>
            <input type="submit" value="Filter">
        </form>
    </div>
    

    <table class="student-registration-list">
        <thead>
            <tr>
                <th><?php _e( 'Student Name', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Course', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Teacher', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Grade', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'District and School', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Parent First/Last Name', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Phone Number', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Email', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Transportation', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Applying for aid', 'wc-student-registration' ); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if ( $students ) :
                $count = 0;
                foreach ( $students as $student_key => $student ) : 
                    $stu_orders     = get_student_order($student,true);
                    if( $stu_orders ) {
                        foreach ($stu_orders as $stu_orders_key => $stu_orders_value) {
                            $order = wc_get_order( $stu_orders_value ); 
                            $class_id = '';
                            foreach ($order->get_items() as $item_id => $item ) {
                                if( $item->get_meta( 'Student' ) == $student->ID ) { 
                                    $count++;
                                    $class_id = $item->get_product_id();
                                    $class_id       = !empty( $class_id ) ? $class_id : '';
                                    $flag = true;
                                    if( !empty( $filter_course ) ) {
                                        if( $filter_course != $class_id ) {
                                            $flag = false;
                                        }
                                    }
                                    if( $flag ) {

                                        $teacher        = get_student_registered_class_teacher( $class_id );
                                        $parent_guardian_1 = $item->get_meta( 'Parent/Guardian 1' );
                                        $parent_1       = trim( sprintf( '%s %s', get_post_meta( $parent_guardian_1, 'parent_guardian_first_name', true ), get_post_meta( $parent_guardian_1, 'parent_guardian_last_name', true ) ) );
                                        $parent_1       = empty( $parent_1 ) ? 'No name set' : $parent_1;
                                        $parent_1_id    = $parent_guardian_1;
                                        $phone          = get_student_emergency_phone( $student );
                                        $email          = get_student_emergency_email( $student );
                                        $address        = get_student_order_address( $student );
                                        $product        = $item->get_product();
                                        $tuition_bal    = wc_price (  $product->get_price() );
                                        $financial_aid  = ucfirst( get_student_order_financial_aid( $student,$stu_orders_value ) ? get_student_order_financial_aid( $student,$stu_orders_value ) : 'no' );
                                        ?>
                                            <tr>
                                                <td>
                                                    <!--<a href="#" class="student-info" data-person="<?php // print esc_attr( $student->ID ); ?>">-->
                                                        <?php esc_html_e( $student->get_full_name(), 'wc-student-registration' ); ?>
                                                    <!--</a>-->
                                                </td>
                                                <td><?php esc_html_e( $class_id ? get_the_title( $class_id ) : '', 'wc-student-registration' ); ?></td>
                                                <td><?php esc_html_e( $teacher ? $teacher : '', 'wc-student-registration' ); ?></td>
                                                <td><?php esc_html_e( $student->get_grade() ? $student->get_grade() : '', 'wc-student-registration' ); ?></td>
                                                <td><?php esc_html_e( $student->get_district() ? $student->get_district() : '', 'wc-student-registration' ); ?> - <?php esc_html_e( $student->get_school() ? $student->get_school() : '', 'wc-student-registration' ); ?></td>
                                                <td><?php esc_html_e( $parent_1 ? $parent_1 : '', 'wc-student-registration' ); ?></td>
                                                <td><?php esc_html_e( $phone ? $phone : '', 'wc-student-registration' ); ?></td>
                                                <td><?php esc_html_e( $email ? $email : '', 'wc-student-registration' ); ?></td>
                                                <td><?php esc_html_e( $student->get_transportation() ? $student->get_transportation() : '', 'wc-student-registration' ); ?></td>
                                                <td><?php esc_html_e( $financial_aid ? $financial_aid : '', 'wc-student-registration' ); ?></td>
                                            </tr>
                                    <?php }
                                }
                            }
                        }
                    } ?>
                <?php endforeach; 
                if($post_count > $posts_per_page ) { ?>
                    <tr>
                        <td colspan="14">
                            <div class="pagination-section">
                                <ul class="pagination justify-content-center">
                                    <?php
                                        $pagignation = paginate_links(array(
                                            'current' => $paged,
                                            'total' => $num_pages,
                                            'type' => 'array',
                                            'prev_text' => '<span aria-hidden="true"><<</span>',
                                            'next_text' => '<span aria-hidden="true">>></span>',
                                        ));
                                        if (!empty($pagignation)) {
                                            foreach ($pagignation as $pagignation_val) {
                                                $pagignation_val = str_replace('page-numbers', 'page-numbers page-link', $pagignation_val);
                                                echo '<li class="page-item">';
                                                echo $pagignation_val;
                                                echo '</li>';
                                            }
                                        }
                                    ?>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
            <?php else : ?>
                <tr>
                    <td colspan="14"><?php _e( 'No students found.', 'wc-student-registration' ); ?></td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>

    <?php 
    return ob_get_clean();
}
add_shortcode( 'wc-student-district-registration-list', __NAMESPACE__ . '\student_district_registration_list' );

/**
 * Displays a list of student registrations for directors dashboard to view
 *
 * @param array $atts
 * @param string $content
 * @return string
 */
function director_student_registration_list( $atts, $content = '' ) {
    // Permissions check
    if ( !current_user_can('administrator') ) {
        return sprintf( '<p>%s</p>', __( 'You do not have permission to view this content.', 'wc-student-registration' ) );
    }
    /*
    * Add order month dropdown
    */
    global $wpdb, $wp_locale;
    $extra_checks = "AND post_status != 'auto-draft'";
    if (! isset($_GET['post_status']) || 'trash' !== $_GET['post_status']) {
        $extra_checks .= " AND post_status != 'trash'";
    } elseif (isset($_GET['post_status'])) {
        $extra_checks = $wpdb->prepare(' AND post_status = %s', $_GET['post_status']);
    }
    $months = $wpdb->get_results($wpdb->prepare("
            SELECT DISTINCT YEAR( post_date ) AS year, MONTH( post_date ) AS month
            FROM $wpdb->posts
            WHERE post_type = %s
            $extra_checks
            ORDER BY post_date DESC
    ", 'shop_order'));
    $month_count = count($months);
   
    if ( !empty( $months ) ) {
        $monthArray = array();
        foreach ($months as $months_key => $months_value) {
            $date_val = date('Yn');
            $month_val = $months_value->year.$months_value->month;
            if( $month_val <= $date_val ) {
                $monthArray[] = $months_value;
            }
        }
        $months = $monthArray;
        $m = isset($_GET['filter_month']) ? $_GET['filter_month'] : $months[0]->year.'-'.zeroise($months[0]->month,2);
    }
    
    $current_year_change = date("Y"); 
    $filter_course      = $_GET['filter_course'];
    $filter_year        = $_GET['filter_year'] ? $_GET['filter_year'] : $current_year_change;
    $filter_month       = $_GET['filter_month'] ? $_GET['filter_month'] : $m;
    $search_student     = $_GET['search_student'];
    
    $paged = get_query_var( 'paged' ) ? get_query_var('paged') : 1 ;
    $posts_per_page = ( !empty($filter_year) || !empty( $search_student )  || !empty( $filter_course ) || !empty( $filter_month ) ) ? 100 : 100 ;
    
    if ( current_user_can( 'manage_options' ) ) {
        $all_students   = get_director_all_students( -1 , $search_student , $filter_course , $filter_year, $filter_month  );
        $students       = get_director_all_students( $posts_per_page , $search_student , $filter_course , $filter_year, $filter_month );
    } else {
        $all_students   = '';
        $students       = '';
//        $all_students   = get_director_students_in_school_district( get_district_rep_district( get_current_user_id()), -1 , $search_student , $filter_course , $filter_year );
//        $students       = get_director_students_in_school_district( get_district_rep_district( get_current_user_id()), $posts_per_page , $search_student , $filter_course , $filter_year );
    }
    $post_count = count($all_students);
    $num_pages = ceil($post_count / $posts_per_page);
    if($paged > $num_pages || $paged < 1){
        $paged = $num_pages;
    }
    $page_slug = get_post_field( 'post_name' );
    ob_start();
    if( $students ) { ?>
        <div class="student-export-reports">
            <a href="<?php print add_query_arg( [ '_wpnonce' => wp_create_nonce(), 'export-wcsr-director-master-list' => 1 ] ); ?>" class="button student-registration-export-trigger fas fa-file-export">
                <?php esc_html_e( 'Export Master List', 'wc-student-registration' ); ?>
            </a>
            <a href="<?php print add_query_arg( [ '_wpnonce' => wp_create_nonce(), 'export-wcsr-director-master-list' => 1 , 'export-wcsr-emergency-contact' => 1 ] ); ?>" class="button student-registration-export-trigger fas fa-file-export">
                <?php esc_html_e( 'Export Emergency Contact', 'wc-student-registration' ); ?>
            </a>
            <a href="<?php print add_query_arg( [ '_wpnonce' => wp_create_nonce(), 'export-wcsr-director-master-list' => 1 , 'export-wcsr-teacher-info' => 1 ] ); ?>" class="button student-registration-export-trigger fas fa-file-export">
                <?php esc_html_e( 'Export Teacher Information', 'wc-student-registration' ); ?>
            </a>
            <a href="<?php print add_query_arg( [ '_wpnonce' => wp_create_nonce(), 'export-wcsr-director-master-list' => 1 , 'export-wcsr-financial-aid' => 1 ] ); ?>" class="button student-registration-export-trigger fas fa-file-export">
                <?php esc_html_e( 'Export Financial Aid', 'wc-student-registration' ); ?>
            </a>
            <a href="<?php print add_query_arg( [ '_wpnonce' => wp_create_nonce(), 'export-wcsr-director-master-list' => 1 , 'export-wcsr-t-shirt' => 1 ] ); ?>" class="button student-registration-export-trigger fas fa-file-export">
                <?php esc_html_e( 'Export T-Shirt', 'wc-student-registration' ); ?>
            </a>
        </div>
    <?php } ?>
    <div class="student-reg-export-form-wrap">
        <?php if( $students ) { ?>
            <p class="student-registration-export">
                <a href="<?php print add_query_arg( [ '_wpnonce' => wp_create_nonce(), 'export-wcsr-director-reg-list' => 1 ] ); ?>" class="button student-registration-export-trigger fas fa-file-export">
                    <?php esc_html_e( 'Export', 'wc-student-registration' ); ?>
                </a>
            </p>
        <?php } ?>
        <form method="get" action="/<?php echo $page_slug;?>/">
            <input type="text" name="search_student" placeholder="Search.." id="search_student" value="<?php echo  $search_student;?>" >
            <select name="filter_course">
                <option value="">Select Course</option>
                <?php $products = get_all_products();
                foreach ($products as $pro_key => $pro_val ) { ?>
                    <option value="<?php echo $pro_val;?>" <?php echo ( $pro_val == $filter_course ) ? 'selected' : '';?>><?php echo get_the_title( $pro_val );?></option>
                <?php } ?>
            </select>
            <!--Years-->
            <?php /*
            <select name="filter_year">
                <option value="">Select Year</option> 
                <?php $this_year = date("Y"); 
                for ($year = $this_year; $year >= $this_year - 20 ; $year--) { ?>
                    <option value="<?php echo $year;?>" <?php echo ( $filter_year == $year ) ? 'selected' : '';?>><?php echo $year;?></option>
                <?php } ?>
            </select> */ ?>
			
            <?php 
                if ( !empty( $months ) ) {
                    $m = isset($_GET['filter_month']) ? $_GET['filter_month'] : $months[0]->year.'-'.zeroise($months[0]->month,2);
                    ?>
                    <select name="filter_month" id="filter-by-date">
<!--                        <option<?php //selected($m, 0); ?> value="0"><?php //_e('All'); ?></option>-->
                        <?php
                            foreach ($months as $arc_row) {
                                if (0 == $arc_row->year) {
                                    continue;
                                }

                                $month = zeroise($arc_row->month, 2);
                                $year = $arc_row->year;
                                printf(
                                    "<option %s value='%s'>%s</option>\n",
                                    selected($m, $year .'-'. $month, false),
                                    esc_attr($arc_row->year .'-'. $month), /* translators: 1: month name, 2: 4-digit year */
                                    sprintf(__('%1$s %2$d'), $wp_locale->get_month($month), $year)
                                );
                            }
                        ?>
                   </select>
                <?php } 
            ?>
            <input type="submit" value="Filter">
        </form>
    </div>
    <table class="student-registration-list" id="director-student-reg-list">
        <thead>
            <tr>
                <th><?php _e( '', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Student Name', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Course', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Teacher', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Grade', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'District and School', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Parent First/Last Name', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Phone Number', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Email', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Address', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Transportation', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Tuition Balance', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Applying for aid', 'wc-student-registration' ); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if ( $students ) :
                foreach ( $students as $student_key => $student ) : 
                    $stu_orders     = get_student_order($student,true,true);
    
                    if( $stu_orders ) {
                        foreach ($stu_orders as $stu_orders_key => $stu_orders_value) {
                            $order = wc_get_order( $stu_orders_value ); 
                            $class_id = '';
                            foreach ($order->get_items() as $item_id => $item ) {
                                if( $item->get_meta( 'Student' ) == $student->ID ) { 
                                    $class_id = $item->get_product_id();
                                    $class_id       = !empty( $class_id ) ? $class_id : '';
                                    $flag = true;
                                    if( !empty( $filter_course ) ) {
                                        if( $filter_course != $class_id ) {
                                            $flag = false;
                                        }
                                    }
                                    if( $flag ) {

                                        $teacher        = get_student_registered_class_teacher( $class_id );
                                        $parent_guardian_1 = $item->get_meta( 'Parent/Guardian 1' );
                                        $parent_1       = trim( sprintf( '%s %s', get_post_meta( $parent_guardian_1, 'parent_guardian_first_name', true ), get_post_meta( $parent_guardian_1, 'parent_guardian_last_name', true ) ) );
                                        $parent_1       = empty( $parent_1 ) ? 'No name set' : $parent_1;
                                        $parent_1_id    = $parent_guardian_1;
                                        $phone          = get_student_emergency_phone( $student );
                                        $email          = get_student_emergency_email( $student );
                                        $address        = get_student_order_address( $student );
                                        $product        = $item->get_product();
                                        $tuition_bal    = wc_price (  $product->get_price() );
                                        $financial_aid  = ucfirst( get_student_order_financial_aid( $student,$stu_orders_value ) ? get_student_order_financial_aid( $student,$stu_orders_value ) : 'no' );
                                        ?>
                                            <tr>
                                                <td>
                                                    <a href="javascript:;" class="student-info edit-info" data-id="<?php print esc_attr( $student->ID ); ?>" data-order-id="<?php print esc_attr( $stu_orders_value ); ?>" data-pro-id="<?php print esc_attr( $class_id ); ?>" data-parent-id="<?php print esc_attr( $parent_1_id ); ?>">
                                                        Edit
                                                    </a>
                                                    <img src="<?php echo WC_STUDENT_REGISTRATION_PLUGIN_URL.'assets/img/loader.gif';?>" class="loader" style="display: none;">
                                                </td>
                                                <td><?php esc_html_e( $student->get_full_name(), 'wc-student-registration' ); ?></td>
                                                <td><?php esc_html_e( $class_id ? get_the_title( $class_id ) : '', 'wc-student-registration' ); ?></td>
                                                <td><?php esc_html_e( $teacher ? $teacher : '', 'wc-student-registration' ); ?></td>
                                                <td><?php esc_html_e( $student->get_grade() ? $student->get_grade() : '', 'wc-student-registration' ); ?></td>
                                                <td><?php esc_html_e( $student->get_district() ? $student->get_district() : '', 'wc-student-registration' ); ?> - <?php esc_html_e( $student->get_school() ? $student->get_school() : '', 'wc-student-registration' ); ?></td>
                                                <td><?php esc_html_e( $parent_1 ? $parent_1 : '', 'wc-student-registration' ); ?></td>
                                                <td><?php esc_html_e( $phone ? $phone : '', 'wc-student-registration' ); ?></td>
                                                <td><?php esc_html_e( $email ? $email : '', 'wc-student-registration' ); ?></td>
                                                <td><?php echo $address ? $address : ''; ?></td>
                                                <td><?php esc_html_e( $student->get_transportation() ? $student->get_transportation() : '', 'wc-student-registration' ); ?></td>
                                                <td><?php echo $tuition_bal ? $tuition_bal : ''; ?></td>
                                                <td><?php esc_html_e( $financial_aid ? $financial_aid : '', 'wc-student-registration' ); ?></td>
                                            </tr>
                                    <?php }
                                }
                            }
                        }
                    } ?>
                <?php endforeach; 
                if($post_count > $posts_per_page ) { ?>
                    <tr>
                        <td colspan="14">
                            <div class="pagination-section">
                                <ul class="pagination justify-content-center">
                                    <?php
                                        $pagignation = paginate_links(array(
                                            'current' => $paged,
                                            'total' => $num_pages,
                                            'type' => 'array',
                                            'prev_text' => '<span aria-hidden="true"><<</span>',
                                            'next_text' => '<span aria-hidden="true">>></span>',
                                        ));
                                        if (!empty($pagignation)) {
                                            foreach ($pagignation as $pagignation_val) {
                                                $pagignation_val = str_replace('page-numbers', 'page-numbers page-link', $pagignation_val);
                                                echo '<li class="page-item">';
                                                echo $pagignation_val;
                                                echo '</li>';
                                            }
                                        }
                                    ?>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
            <?php else : ?>
                <tr>
                    <td colspan="14"><?php _e( 'No students found.', 'wc-student-registration' ); ?></td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
    <?php 
    return ob_get_clean();
}
add_shortcode( 'wc-directors-student-registration-list', __NAMESPACE__ . '\director_student_registration_list' );