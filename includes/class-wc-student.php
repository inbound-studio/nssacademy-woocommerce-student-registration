<?php
/**
 * Student object
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration;

class WC_Student {

	/**
	 * Student post ID
	 *
	 * @var integer
	 */
	public $ID;

	/**
	 * Student post ID
	 *
	 * @var integer
	 */
	public $id;

	/**
	 * Instance of student WP_Post object
	 *
	 * @var WP_Post
	 */
	protected $post;

	public function __construct( $id ) {
		$this->ID    = $id;
		$this->id    = $id;
		$this->post  = get_post( $id );
	}

	/**
	 * Returns WP_Post object of this student
	 *
	 * @return WP_Post
	 */
	public function get_post() {
		return $this->post;
	}

	/**
	 * Return associated WP user account ID
	 *
	 * @return integer
	 */
	public function get_account() {
		return absint( $this->get_post()->post_author );
	}

	/**
	 * Return student first name
	 *
	 * @return string
	 */
	public function get_first_name() {
		return get_post_meta( $this->ID, 'student_first_name', true );
	}

	/**
	 * Return student last name
	 *
	 * @return string
	 */
	public function get_last_name() {
		return get_post_meta( $this->ID, 'student_last_name', true );
	}

	/**
	 * Return student full name
	 *
	 * @return string
	 */
	public function get_full_name() {
		$name = trim( sprintf( '%s %s', $this->get_first_name(), $this->get_last_name() ) );

		if ( empty( $name ) ) {
			$name = __( 'No name set', 'wc-student-registration' );
		}

		return $name;
	}

	/**
	 * Return student currently registered class
	 *
	 * @return string
	 */
	public function get_registered_class() {
		return get_post_meta( $this->ID, 'registered_class', true );
	}

	/**
	 * Return student gender
	 *
	 * @return string
	 */
	public function get_gender() {
		return get_post_meta( $this->ID, 'student_gender', true );
	}

	/**
	 * Return student currently grade
	 *
	 * @return string
	 */
	public function get_grade() {
		return get_post_meta( $this->ID, 'student_grade', true );
	}

	/**
	 * Return student currently District
	 *
	 * @return string
	 */
	public function get_district() {
                $district = get_post_meta( $this->ID, 'student_school_district', true );
                $district = !empty( $district ) ? get_term( $district )->name : '';
		return $district;
	}

	/**
	 * Return student currently school
	 *
	 * @return string
	 */
	public function get_school() {
		return get_post_meta( $this->ID, 'student_school_name', true );
	}

	/**
	 * Return student transportation
	 *
	 * @return string
	 */
	public function get_transportation() {
		return get_post_meta( $this->ID, 'student_method_transport', true );
	}

	/**
	 * Return student dob
	 *
	 * @return string
	 */
	public function get_dob() {
		$dob =  get_post_meta( $this->ID, 'student_dob', true );
                $dob =  date('F j, Y', strtotime($dob));
                return $dob;
	}

	/**
	 * Return student ethnicity
	 *
	 * @return string
	 */
	public function get_ethnicity() {
		return get_post_meta( $this->ID, 'student_ethnicity', true );
	}

	/**
	 * Return student shirt_size
	 *
	 * @return string
	 */
	public function get_shirt_size() {
		return get_post_meta( $this->ID, 'student_shirt_size', true );
	}

	/**
	 * Return student health_concerns_conditional
	 *
	 * @return string
	 */
	public function get_health_concerns_conditional() {
		return get_post_meta( $this->ID, 'student_health_concerns_conditional', true );
	}

	/**
	 * Return student health_concerns
	 *
	 * @return string
	 */
	public function get_health_concerns() {
		return get_post_meta( $this->ID, 'student_health_concerns', true );
	}

	/**
	 * Return student medications_conditional
	 *
	 * @return string
	 */
	public function get_medications_conditional() {
		return get_post_meta( $this->ID, 'student_medications_conditional', true );
	}

	/**
	 * Return student reason_medication
	 *
	 * @return string
	 */
	public function get_reason_medication() {
		return get_post_meta( $this->ID, 'student_reason_medication', true );
	}

	/**
	 * Return student medications
	 *
	 * @return string
	 */
	public function get_medications() {
		return get_post_meta( $this->ID, 'student_medications', true );
	}

	/**
	 * Return student diabetes_conditional
	 *
	 * @return string
	 */
	public function get_diabetes_conditional() {
		return get_post_meta( $this->ID, 'student_diabetes_conditional', true );
	}

	/**
	 * Return student strengths
	 *
	 * @return string
	 */
	public function get_strengths() {
		return get_post_meta( $this->ID, 'student_strengths', true );
	}

	/**
	 * Return student skills_to_improve
	 *
	 * @return string
	 */
	public function get_skills_to_improve() {
		return get_post_meta( $this->ID, 'student_skills_to_improve', true );
	}

	/**
	 * Return student strengths
	 *
	 * @return string
	 */
	public function get_additional_comments() {
		return get_post_meta( $this->ID, 'student_additional_comments', true );
	}

	/**
	 * Return student authorize_medicate
	 *
	 * @return string
	 */
	public function get_authorize_medicate() {
		return get_post_meta( $this->ID, 'authorize_medicate_student', true );
	}

}
