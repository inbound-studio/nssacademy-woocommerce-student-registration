<?php
/**
 * WC_Student_Registration_AJAX class file
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration;

use function WC_Student_Registration\Functions\get_student_order;
use function WC_Student_Registration\Functions\get_student_registered_class;
use function WC_Student_Registration\Functions\get_student_registered_class_teacher;
use function WC_Student_Registration\Functions\get_student_parent_guardian_1;
use function WC_Student_Registration\Functions\get_student_order_phone;
use function WC_Student_Registration\Functions\get_student_order_email;
use function WC_Student_Registration\Functions\get_student_order_address;
use function WC_Student_Registration\Functions\get_student_order_tuition_balance;
use function WC_Student_Registration\Functions\get_student_order_financial_aid;
use function WC_Student_Registration\Functions\get_student_order_year;
use function WC_Student_Registration\Functions\get_all_products;
use function WC_Student_Registration\Functions\get_all_students_by_year;
use function WC_Student_Registration\Functions\get_order_financial_aid_amount;
use function WC_Student_Registration\Functions\get_order_pending_financial_aid_amount;
use function WC_Student_Registration\Functions\get_all_teacher;
use function WC_Student_Registration\Functions\get_all_district;
use function WC_Student_Registration\Functions\get_all_parents_guardians;
use function WC_Student_Registration\Functions\get_order_parents_guardians_options_array;
use function WC_Student_Registration\Functions\get_student_emergency_phone;
use function WC_Student_Registration\Functions\get_student_emergency_email;


class WC_Student_Registration_AJAX {

    public function __construct() {
        add_action( 'wp_ajax_wcsr_get_person', [ $this, 'get_person'] );
        add_action( 'wp_ajax_wcsr_get_students', [ $this, 'get_students'] );
        add_action( 'wp_ajax_wcsr_save_student', [ $this, 'save_student'] );
		add_action( 'wp_ajax_wcsr_save_parent_guardian', [ $this, 'save_parent_guardian'] );
		add_action( 'wp_ajax_wcsr_save_parent_guardian2', [ $this, 'save_parent_guardian2'] );
        add_action( 'wp_ajax_wcsr_edit_student', [ $this, 'edit_student'] );
        add_action( 'wp_ajax_wcsr_update_student', [ $this, 'update_student'] );
    }

    /**
     * Returns a persons info
     *
     * @return void
     */
    public function get_person() {
        if ( ! wp_verify_nonce( $_REQUEST['nonce'] ) ) {
            wp_send_json_error( new \WP_Error( '001', __( 'Invalid nonce', 'wc-student-registration' ) ) );
        }

        $person = $_REQUEST['person'] ?? 0;
        $type   = $_REQUEST['type'] ?? 'student';

        switch ( $type ) {
            case 'student' :
            default : 
                $keys = [
                    'student_first_name',
                    'student_last_name',
                    'registered_class',
                    'student_gender',
                    'student_dob',
                    'student_ethnicity',
                    'student_shirt_size',
                    'student_school_name',
                    'student_school_district',
                    'student_grade',
                    'student_health_concerns_conditional',
                    'student_health_concerns',
                    'student_medications_conditional',
                    'student_reason_medication',
                    'student_medications',
                    'student_diabetes_conditional',
                    'student_method_transport',
                    'student_strengths',
                    'student_skills_to_improve',
                    'student_additional_comments',
                    'student_emergency_contacts',
                    'authorize_medicate_student',
                    'consent_media_release_student'
                ];

                $_meta = get_post_meta( $person );
                break;

            case 'parent-guardian' :
                $keys = [
                    'parent_guardian_first_name',
                    'parent_guardian_last_name',
                    'parent_guardian_relationship',
                    'parent_guardian_phone',
                    'parent_guardian_email',
                ];

                $_meta = get_post_meta( $person );
                break;

            case 'account' :
                $keys = [
                    'first_name',
                    'last_name',
                    'billing_phone',
                    'billing_relationship',
                    'carpool'
                ];

                $_meta = get_user_meta( $person );
                break;
        }

        $meta  = [];
        ?>

        <table cellspacing="10" cellpadding="10" border="1" class="person-info-table">
            <tbody>
                <?php
                foreach ( $keys as $key ) {
                    foreach ( $_meta as $_key => $_value ) {
                        if ( $key === $_key ) {
                            switch ( $key ) {
                                case 'registered_class' :
                                    if( !empty( current( $_value ) ) ) {
                                        $value_array = explode(',', current( $_value )) ;
                                        $value_title = array();
                                        foreach ($value_array as $val) {
                                            if( !empty( $val ) ) {
                                                $title = get_the_title( $val );
                                                $value_title[] = $title;
                                            }
                                        }
                                        $value = implode(',', $value_title);
                                    } else {
                                        $value = '';
                                    }
                                    break;

                                case 'student_school_district' :
                                    $value = get_term( current( $_value ), 'school_district' )->name;
                                    break;

                                default :
                                    $value = current( $_value );
                            }
                            ?>

                            <tr>
                                <td><?php print esc_html( ucfirst( preg_replace( '/[-_]/', ' ', $key ) ) ); ?></td>
                                <td><?php print esc_html( $value ); ?></td>
                            </tr>

                            <?php
                        }
                    }
                }
                ?>
            </tbody>
        </table>

        <?php
    }

    /**
     * Return array of students searched by a term
     *
     * @return array
     */
    public function get_students() {
        ob_start();

		check_ajax_referer();

		if ( ! current_user_can( 'edit_shop_orders' ) ) {
			wp_die( -1 );
		}

		$term  = isset( $_GET['term'] ) ? (string) wc_clean( wp_unslash( $_GET['term'] ) ) : '';
		$limit = -1;

		if ( empty( $term ) ) {
			wp_die();
		}

		$students = [];

        if ( 3 > strlen( $term ) ) {
            $limit = 20;
        }
        
        $students = get_posts( [
            'post_type'         => 'student',
            'post_status'       => 'publish',
            'posts_per_page'    => $limit,
            's'                 => $term
        ] );

		$found_students = [];

		foreach ( $students as $student ) {
            $student = new WC_Student( $student->ID );
            
            if ( $student ) {
                $found_students[ $student->ID ] = $student->get_full_name();
            }
        }
        
		wp_send_json( $found_students );
    }

    /**
     * Save a student on an order line item
     *
     * @return array
     */
    public function save_student() {
        ob_start();

		check_ajax_referer();

		if ( ! current_user_can( 'edit_shop_orders' ) ) {
			wp_die( -1 );
		}

		$student = isset( $_REQUEST['student'] ) ? (int) $_REQUEST['student'] : '';
                $item_id = isset( $_REQUEST['item_id'] ) ? (int) $_REQUEST['item_id'] : '';
        
                $stu_class = get_post_meta( $student, 'registered_class', true );
                $pro_id     = wc_get_order_item_meta( $item_id, '_product_id', true );
                if( !empty( $stu_class ) ) {
                    $stu_class_array = explode(',', $stu_class);
                    if( !in_array($pro_id, $stu_class_array) ) {
                        $stu_class = $stu_class .','.$pro_id;
                    }
                } else {
                    $stu_class = $pro_id;
                }
                update_post_meta( $student, 'registered_class', $stu_class );
                $update = wc_update_order_item_meta( $item_id, 'Student', $student );
		
		wp_send_json( $update );
    }
  
    /**
     * Save a parent_guardian1 on an order line item
     *
     * @return array
     */
    public function save_parent_guardian() {
        ob_start();

		check_ajax_referer();

		if ( ! current_user_can( 'edit_shop_orders' ) ) {
			wp_die( -1 );
		}

		//Add parent1 :: s
		$parentguardian1 = isset( $_REQUEST['parentguardian1'] ) ? (int) $_REQUEST['parentguardian1'] : '';
		if(!empty($parentguardian1)){
                $parentguardian1_item_id = isset( $_REQUEST['item_id'] ) ? (int) $_REQUEST['item_id'] : '';
        
                $parentguardian1_class = get_post_meta( $parentguardian1, 'registered_class', true );
                $parentguardian1_pro_id     = wc_get_order_item_meta( $parentguardian1_item_id, '_product_id', true );
                if( !empty( $parentguardian1_class ) ) {
                    $parentguardian1_stu_class_array = explode(',', $parentguardian1_class);
                    if( !in_array($parentguardian1_pro_id, $parentguardian1_stu_class_array) ) {
                        $parentguardian1_stu_class = $parentguardian1_stu_class .','.$parentguardian1_pro_id;
                    }
                } else {
                    $parentguardian1_stu_class = $parentguardian1_pro_id;
                }
                update_post_meta( $parentguardian1, 'registered_class', $parentguardian1_stu_class );
                $update = wc_update_order_item_meta( $parentguardian1_item_id, 'Parent/Guardian 1', $parentguardian1 );
		//Add parent1 :: e
		
		wp_send_json( $update );
		}
    }
	/**
     * Save a parent_guardian2 on an order line item
     *
     * @return array
     */
    public function save_parent_guardian2() {
        ob_start();

		check_ajax_referer();

		if ( ! current_user_can( 'edit_shop_orders' ) ) {
			wp_die( -1 );
		}

		//Add parent2 :: s
		$parentguardian2 = isset( $_REQUEST['parentguardian2'] ) ? (int) $_REQUEST['parentguardian2'] : '';
		if(!empty($parentguardian2)){
                $parentguardian2_item_id = isset( $_REQUEST['item_id'] ) ? (int) $_REQUEST['item_id'] : '';
        
                $parentguardian2_class = get_post_meta( $parentguardian2, 'registered_class', true );
                $parentguardian2_pro_id     = wc_get_order_item_meta( $parentguardian2_item_id, '_product_id', true );
                if( !empty( $parentguardian2_class ) ) {
                    $parentguardian2_stu_class_array = explode(',', $parentguardian2_class);
                    if( !in_array($parentguardian2_pro_id, $parentguardian2_stu_class_array) ) {
                        $parentguardian2_stu_class = $parentguardian2_stu_class .','.$parentguardian2_pro_id;
                    }
                } else {
                    $parentguardian2_stu_class = $parentguardian2_pro_id;
                }
                update_post_meta( $parentguardian2, 'registered_class', $parentguardian2_stu_class );
                $update = wc_update_order_item_meta( $parentguardian2_item_id, 'Parent/Guardian 2', $parentguardian2 );
		//Add parent2 :: e
		
		wp_send_json( $update );
		}
    }
    /**
     * edit a student form
     *
     * @return array
     */
    public function edit_student() {
        if ( ! wp_verify_nonce( $_REQUEST['nonce'] ) ) {
            wp_send_json_error( new \WP_Error( '001', __( 'Invalid nonce', 'wc-student-registration' ) ) );
        }
        $stu_id  = $_REQUEST['stu_id'] ? $_REQUEST['stu_id'] : '';
        $order_id  = $_REQUEST['order_id'] ? $_REQUEST['order_id'] : '';
        $pro_id  = $_REQUEST['pro_id'] ? $_REQUEST['pro_id'] : '';
        $parent_id  = $_REQUEST['parent_id'] ? $_REQUEST['parent_id'] : '';
        $student = new WC_Student( $stu_id );
        
        ?>
        <!--<form class="director-edit-stu-form" id="director-edit-stu-form">-->
            <table cellspacing="10" cellpadding="10" border="1" class="person-info-table director-edit-stu-form" id="person-info-table">
                <tbody>
                    <tr>
                        <td colspan="2">
                            <div class="error-msg-box"></div>
                            <div class="success-msg-box"></div>
                        </td>
                    </tr>
                    <?php
                        $fname          = get_post_meta( $stu_id, 'student_first_name', true );
                        $lname          = get_post_meta( $stu_id, 'student_last_name', true );
                        $grade          = get_post_meta( $stu_id, 'student_grade', true );
                        $district       = get_post_meta( $stu_id, 'student_school_district', true );
                        $school         = get_post_meta( $stu_id, 'student_school_name', true );
                        $transportation = get_post_meta( $stu_id, 'student_method_transport', true );
                        $class_id       = $pro_id;
                        $class_id       = !empty( $class_id ) ? $class_id : '';
                        $teacher        = get_student_registered_class_teacher( $class_id , true );
                        $parent_1       = $parent_id;
                        //$parent_1       = get_student_parent_guardian_1( $student , true,$order_id );
                        $address        = get_student_order_address( $student );
                        $order_id       = $order_id;
                        $phone          = get_student_emergency_phone( $student );
                        $email          = get_student_emergency_email( $student );
                        ?>

                        <tr>
                            <td>
                                <label for="fname">Name:</label>
                            </td>
                            <td>
                                <input type="text" name="fname" value="<?php echo $fname;?>" id="fname" placeholder="First Name" required="">
                                <input type="text" name="lname" value="<?php echo $lname;?>" id="lname" placeholder="Last Name" required="">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="teachers">Teacher:</label>
                            </td>
                            <td>
                                <select name="teachers" multiple="" id="teachers" required="">
                                    <?php $teachers = get_all_teacher();
                                    foreach ($teachers as $teacher_key => $teacher_val ) { ?>
                                        <option value="<?php echo $teacher_val;?>" <?php echo ( in_array( $teacher_val,$teacher )  ) ? 'selected' : '';?>>
                                            <?php echo get_the_title( $teacher_val );?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="grade">Grade:</label>
                            </td>
                            <td>
                                <select name="grade" id="grade" required="">
                                    <option value="1st Grade" <?php echo ( $grade == '1st Grade' ) ? 'selected' : '';?>>
                                        1st Grade
                                    </option>
                                    <option value="2nd Grade" <?php echo ( $grade == '2nd Grade' ) ? 'selected' : '';?>>
                                        2nd Grade
                                    </option>
                                    <option value="3rd Grade" <?php echo ( $grade == '3rd Grade' ) ? 'selected' : '';?>>
                                        3rd Grade
                                    </option>
                                    <option value="4th Grade" <?php echo ( $grade == '4th Grade' ) ? 'selected' : '';?>>
                                        4th Grade
                                    </option>
                                    <option value="5th Grade" <?php echo ( $grade == '5th Grade' ) ? 'selected' : '';?>>
                                        5th Grade
                                    </option>
                                    <option value="6th Grade" <?php echo ( $grade == '6th Grade' ) ? 'selected' : '';?>>
                                        6th Grade
                                    </option>
                                    <option value="7th Grade" <?php echo ( $grade == '7th Grade' ) ? 'selected' : '';?>>
                                        7th Grade
                                    </option>
                                    <option value="8th Grade" <?php echo ( $grade == '8th Grade' ) ? 'selected' : '';?>>
                                        8th Grade
                                    </option>
                                    <option value="9th Grade" <?php echo ( $grade == '9th Grade' ) ? 'selected' : '';?>>
                                        9th Grade
                                    </option>
                                    <option value="10th Grade" <?php echo ( $grade == '10th Grade' ) ? 'selected' : '';?>>
                                        10th Grade
                                    </option>
                                    <option value="11th Grade" <?php echo ( $grade == '11th Grade' ) ? 'selected' : '';?>>
                                        11th Grade
                                    </option>
                                </select>
                            </td>
                        </tr>
                        </tr>
                        <tr>
                            <td>
                                <label for="districts">District:</label>
                            </td>
                            <td>
                                <select name="districts" id="districts" required="">
                                    <?php $districts = get_all_district();
                                    foreach ($districts as $district_key => $district_val ) { ?>
                                        <option value="<?php echo $district_val;?>" <?php echo ( $district_val == $district ) ? 'selected' : '';?>>
                                            <?php echo get_term( $district_val )->name;?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="school">School:</label>
                            </td>
                            <td>
                                <input type="text" name="school" value="<?php echo $school;?>" id="school" required="">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="parent_fname">Parent Name:</label>
                            </td>
                            <td>
                                <select name="parent" id="parent" required="">
                                    <?php $parents = get_order_parents_guardians_options_array( $order_id );
                                    foreach ($parents as $parent_key => $parent_val ) {
                                        ?>
                                        <option value="<?php echo $parent_key;?>" <?php echo ( $parent_key == $parent_1 ) ? 'selected' : '';?>>
                                            <?php echo $parent_val;?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="phone">Phone:</label>
                            </td>
                            <td>
                                <input type="tel" name="phone" value="<?php echo $phone ?>" id="phone" required="">
                                <span>Separate with comma (,)</span>
                            </td>
                        </tr> 
                        <tr>
                            <td>
                                <label for="email">Email:</label>
                            </td>
                            <td>
                                <input type="email" name="email" value="<?php echo $email?>" id="email" required=""> 
                                <span>Separate with comma (,)</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="address">Transportation:</label>
                            </td>
                            <td>
                                <label for="Driving">
                                    <input type="radio" name="transportation" value="Driving" id="Driving" class="transportation" <?php echo ( $transportation == 'Driving' ) ? 'checked' : '';?>>
                                    Driving
                                </label>
                                <label for="District Bussing">
                                    <input type="radio" name="transportation" value="District Bussing" id="District Bussing" class="transportation"  <?php echo ( $transportation == 'District Bussing' ) ? 'checked' : '';?>>
                                    District Bussing
                                </label>
                                <label for="Carpooling">
                                    <input type="radio" name="transportation" value="Carpooling" id="Carpooling" class="transportation" <?php echo ( $transportation == 'Carpooling' ) ? 'checked' : '';?>>
                                    Carpooling
                                </label>
                                <label for="Other">
                                    <input type="radio" name="transportation" value="Other" id="Other" class="transportation"  <?php echo ( $transportation == 'Other' ) ? 'checked' : '';?>>
                                    Other
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="hidden" name="stu_id" value="<?php echo $stu_id?>" id="stu_id">
                                <input type="hidden" name="product_id" value="<?php echo $class_id?>" id="product_id">
                                <input type="hidden" name="order_id" value="<?php echo $order_id?>" id="order_id">
                                <input type="button" name="submit_btn" value="<?php echo 'Submit'?>" id="submit_btn">
                                <input type="button" name="cancel_btn" value="<?php echo 'Cancel'?>" id="cancel_btn">
                                <img src="<?php echo WC_STUDENT_REGISTRATION_PLUGIN_URL.'assets/img/loader.gif';?>" class="loader" style="display: none;">
                            </td>
                        </tr>
                </tbody>
            </table>
        <!--</form>-->
    <?php }
    
    
    /**
     * Update a student on an director dashboard list
     *
     * @return array
     */
    public function update_student() {
            
            if ( ! wp_verify_nonce( $_REQUEST['nonce'] ) ) {
                wp_send_json_error( new \WP_Error( '001', __( 'Invalid nonce', 'wc-student-registration' ) ) );
            }

            if ( ! current_user_can( 'administrator' ) ) {
                    die( 'You Can not edit this information...' );
            }

            $stu_id      = $_POST['stu_id'];
            $pro_id      = $_POST['pro_id'];
            $order_id    = $_POST['order_id'];

            if( !empty( $stu_id ) ) {
                    
                    $fname      = $_POST['fname'];
                    $lname      = $_POST['lname'];
                    $grade      = $_POST['grade'];
                    $teacher    = $_POST['teachers'];
                    $district   = $_POST['districts'];
                    $school     = $_POST['school'];
                    $parent     = $_POST['parent'];
                    $phone      = $_POST['phone'];
                    $email      = $_POST['email'];
                    $transportation      = $_POST['transportation'];
                    
                    update_post_meta( $stu_id, 'student_first_name',$fname );
                    update_post_meta( $stu_id, 'student_last_name',$lname );
                    update_post_meta( $stu_id, 'student_grade',$grade );
                    update_post_meta( $stu_id, 'student_school_district',$district );
                    update_post_meta( $stu_id, 'student_school_name',$school );
                    update_post_meta( $stu_id, 'student_method_transport',$transportation );
                    
                    update_post_meta( $pro_id, 'teacher',$teacher );
                    
                    $order = wc_get_order( $order_id );
                    $student = $stu_id;
                    if ( $student && get_post_type( $student ) !== false ) {
                            if ( $order && $pro_id ) {
                             
                                    foreach ( $order->get_items() as $item ) {
                                        if( $item->get_product_id() == $pro_id ) { 
                                                $item->update_meta_data( 'Parent/Guardian 1', $parent, true );
                                                $item->save_meta_data();
                                        }
                                    }
                            } 
                    } 
                    
                    $field_key = "student_emergency_contacts";
                    $value = array();
                    $phone = trim($phone);
                    $phone = trim($phone,',');
                    $email = trim($email);
                    $email = trim($email,',');
                    $phone = explode(',', $phone);
                    $email = explode(',', $email);
                    
                    
                    foreach ($phone as $phone_key => $phone_value) {
                        $value[$phone_key] = array("phone" => $phone_value);
                    }
                    update_field( $field_key, $value, $stu_id );
                    foreach ($email as $email_key => $email_value) {
                        $value[$email_key] = array("email" => $email_value);
                    }
                   
                    update_field( $field_key, $value, $stu_id );
//                    update_post_meta( $order_id, '_billing_email', $email );
//                    update_post_meta( $order_id, '_billing_phone', $phone );
                  
                    $return_data = array(
                            'error' => 0,
                            'notice' => 'Student details updated successfully',
                    );
                    echo json_encode($return_data);
            } else {
                    $return_data = array(
                            'error' => 1,
                            'notice' => 'Ooops, something went wrong, please try again later.',
                    );
                    echo json_encode($return_data);
            }
            
            exit();
		
    }
    
}

return new WC_Student_Registration_AJAX;