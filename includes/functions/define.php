<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace WC_Student_Registration\Functions;

$pre_reg_time = get_pre_reg_timestamp();
define( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PRE_REG_TIMESTAMP', $pre_reg_time );

$reg_time = get_reg_timestamp();
define( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_REG_TIMESTAMP', $reg_time );

$exp_pre_reg_time = get_explorimg_pre_reg_timestamp();
define( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_EXPLORING_PRE_REG_TIMESTAMP', $exp_pre_reg_time );

$exp_reg_time = get_exploring_reg_timestamp();
define( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_EXPLORING_REG_TIMESTAMP', $exp_reg_time );

$pre_reg_pass = get_pre_reg_password();
define( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PRE_REG_PASSWORD', $pre_reg_pass );

$reg_pass = get_reg_password();
define( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PASSWORD', $reg_pass );