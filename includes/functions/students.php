<?php
/**
 * Functions
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration\Functions;

use WC_Student_Registration\WC_Student;
use WC_Student_Registration\WC_Parent_Guardian;

/**
 * Returns an array of WP_Post
 *
 * @param integer $user_id
 * @return array
 */
function get_user_students( $user_id ) {
	global $wpdb;

	$students = $wpdb->get_col( $wpdb->prepare( "
		SELECT 			ID
		FROM   			$wpdb->posts
		WHERE			post_status = 'publish'
		AND				post_type = 'student'
		AND 			post_author = '%d'
	", absint( $user_id ) ) );

	return array_map( __NAMESPACE__ . '\get_student', $students );
}

/**
 * Returns a student object
 *
 * @param integer $id
 * @return WC_Student
 */
function get_student( $id ) {
	return new WC_Student( $id );
}

/**
 * Returns array of students to populate a select dropdown
 *
 * @param integer $user_id
 * @return array
 */
function get_students_options_array( $user_id ) {
	$students = get_user_students( $user_id );

	$options = [];

	foreach ( $students as $student ) {
		$options[ $student->ID ] = $student->get_full_name();
	}

	return $options;
}

/**
 * Returns an array of school districts to populate a select dropdown
 *
 * @return array
 */
function get_school_districts_options_array() {
	return [ '' => '' ] + get_terms( [ 
		'taxonomy' 		=> 'school_district', 
		'hide_empty' 	=> false, 
		'fields' 		=> 'id=>name' 
	] );
}

/**
 * Get a given users parents/guardians
 *
 * @param integer $user_id
 * @return array
 */
function get_user_parents_guardians( $user_id ) {
	global $wpdb;

	$parents_guardians = $wpdb->get_col( $wpdb->prepare( "
		SELECT 			ID
		FROM   			$wpdb->posts
		WHERE			post_status = 'publish'
		AND				post_type = 'parent_guardian'
		AND 			post_author = '%d'
	", absint( $user_id ) ) );

	return array_map( __NAMESPACE__ . '\get_parent_guardian', $parents_guardians );
}


/**
 * Returns a parent/guardian object
 *
 * @param integer $id
 * @return WC_Parent_Guardian
 */
function get_parent_guardian( $id ) {
	return new WC_Parent_Guardian( $id );
}

/**
 * Returns array of parents/guardians to populate a select dropdown
 *
 * @param integer $user_id
 * @return array
 */
function get_parents_guardians_options_array( $user_id ) {
	$parents_guardians = get_user_parents_guardians( $user_id );

	$options = [];

	foreach ( $parents_guardians as $parent_guardian ) {
		$options[ $parent_guardian->ID ] = $parent_guardian->get_full_name();
	}

	return $options;
}

/**
 * Returns all students
 *
 * @return array
 */
function get_all_students() {
	$all_students_id    = get_all_students_orders();
	$stu_ids = empty( $all_students_id ) ? [ 0 ] : $all_students_id;
    	$students = get_posts( [
		'numberposts'		=> -1,
		'post_type'		=> 'student',
		'post_status'		=> 'publish',
			'post__in' => $stu_ids,
        	'fields'		=> 'ids'
	] );

	return array_map( __NAMESPACE__ . '\get_student', $students );
}

/**
 * Returns array of students in a given school district
 *
 * @param integer|array $district
 * @return array
 */
function get_students_in_school_district( $district ) {
	$district = (array) $district;
	$all_students_id    = get_all_students_orders();
	$stu_ids = empty( $all_students_id ) ? [ 0 ] : $all_students_id;
	$students = get_posts( [
		'numberposts'		=> -1,
		'post_type'		=> 'student',
		'post_status'		=> 'publish',
		'post__in' => $stu_ids,
		'fields'		=> 'ids',
		'tax_query'		=> [
			[
				'taxonomy'	=> 'school_district',
				'field'		=> 'term_id',
				'terms'		=> $district
			]
		]
	] );

	return array_map( __NAMESPACE__ . '\get_student', $students );
}

/**
 * Returns all students
 *
 * @return array
 */
function get_director_all_students( $no_post = -1 , $search_student = '', $filter_course = '' , $filter_year = '', $filter_month = '' ) {
        $paged              = get_query_var( 'paged' ) ? get_query_var('paged') : 1 ;
        $all_students_id    = get_all_students_orders();
        $args = array(
            'numberposts'       => $no_post,
            'post_type'		=> 'student',
            'post_status'	=> 'publish',
            'orderby'           => 'modified',
            'order'             => 'DESC',
            'paged'             => $paged,
            'meta_key'          => 'registered_class',
            'meta_value'        => '',
            'meta_compare'      => '!=',
            'fields'		=> 'ids'
        );
        if( empty( $search_student ) && empty( $filter_course ) && empty( $filter_year ) ) {
            $args['post__in'] = empty( $all_students_id ) ? [ 0 ] : $all_students_id;
        }
        if( !empty( $search_student ) ) {
            $search_student = trim( $search_student );
            $tax_args = array(
                'taxonomy'      => array( 'school_district' ), 
                'fields'        => 'ids',
                'name__like'    => $search_student
            ); 
            $school_district = get_terms( $tax_args );
            $products = get_all_products( $search_student );
            $stu_name = explode(' ', $search_student);
            $args['meta_query']['relation'] = 'AND';
            $args['meta_query'][0]['relation'] = 'OR';
            if( !empty( $stu_name ) ) {
                foreach ($stu_name as $stu_key => $stu_value) {
                    $args['meta_query'][0][] =  array(
                            'key'     => 'student_first_name',
                            'value'   => $stu_value,
                            'compare' => 'LIKE',
                    );
                    $args['meta_query'][0][] =  array(
                            'key'     => 'student_last_name',
                            'value'   => $stu_value,
                            'compare' => 'LIKE',
                    ); 
                }
            }
            
            // if( !empty( $school_district ) ) {
            //     $args['meta_query'][0][] =  array(
            //             'key'     => 'student_school_district',
            //             'value'   => $school_district,
            //             'compare' => 'IN',
            //     );
            // }
            // if( !empty( $products ) ) {
            //     $args['meta_query'][0][] =  array(
            //             'key'     => 'registered_class',
            //             'value'   => $products,
            //             'compare' => 'IN',
            //     );
            // }
        }

        if( !empty( $filter_course ) || !empty( $filter_year ) || !empty( $filter_month ) ) {
            $students = get_all_students_by_year( $filter_year,$filter_course,$filter_month );
            $args['post__in'] = empty( $students ) ? [ 0 ] : $students;
        }

        $students = get_posts( $args );

	return array_map( __NAMESPACE__ . '\get_student', $students );
}

/**
 * Returns array of students in a given school district
 *
 * @param integer|array $district
 * @return array
 */
function get_director_students_in_school_district( $district , $no_post = -1 , $search_student = '', $filter_course = '' , $filter_year = '',$filter_month = '' ) {
	$district           = (array) $district;
        $paged              = get_query_var( 'paged' ) ? get_query_var('paged') : 1 ;
        $all_students_id    = get_all_students_orders();
        
        $args = array( 
            'numberposts'       => $no_post,
            'post_type'		=> 'student',
            'post_status'	=> 'publish',
            'paged'             => $paged,
            'orderby'           => 'modified',
            'order'             => 'DESC',
            'meta_key'          => 'registered_class',
            'meta_value'        => '',
            'meta_compare'      => '!=',
            'fields'		=> 'ids',
            'tax_query'		=> [
                    [
                            'taxonomy'	=> 'school_district',
                            'field'	=> 'term_id',
                            'terms'	=> $district
                    ]
            ] 
        );
        if( empty( $search_student ) && empty( $filter_course ) && ( empty( $filter_year ) || empty( $filter_month ) ) ) {
            $args['post__in'] = empty( $all_students_id ) ? [ 0 ] : $all_students_id;
        }
        if( !empty( $search_student ) ) {
            $search_student = trim( $search_student );
            $tax_args = array(
                'taxonomy'      => array( 'school_district' ), 
                'fields'        => 'ids',
                'name__like'    => $search_student
            ); 
            $school_district = get_terms( $tax_args );
            $products = get_all_products( $search_student );
            $stu_name = explode(' ', $search_student);
            $args['meta_query']['relation'] = 'AND';
            $args['meta_query'][0]['relation'] = 'OR';
            if( !empty( $stu_name ) ) {
                foreach ($stu_name as $stu_key => $stu_value) {
                    $args['meta_query'][0][] =  array(
                            'key'     => 'student_first_name',
                            'value'   => $stu_value,
                            'compare' => 'LIKE',
                    );
                    $args['meta_query'][0][] =  array(
                            'key'     => 'student_last_name',
                            'value'   => $stu_value,
                            'compare' => 'LIKE',
                    ); 
                }
            }
            
            if( !empty( $school_district ) ) {
                $args['meta_query'][0][] =  array(
                        'key'     => 'student_school_district',
                        'value'   => $school_district,
                        'compare' => 'IN',
                );
            }
            if( !empty( $products ) ) {
                $args['meta_query'][0][] =  array(
                        'key'     => 'registered_class',
                        'value'   => $products,
                        'compare' => 'IN',
                );
            }
        }
        if( !empty( $filter_course ) || !empty( $filter_year ) || !empty( $filter_month )  ) {
            $students = get_all_students_by_year( $filter_year,$filter_course,$filter_month );
            $args['post__in'] = empty( $students ) ? [ 0 ] : $students;
        }
        
	$students = get_posts( $args );

	return array_map( __NAMESPACE__ . '\get_student', $students );
}


/**
 * Returns all students
 *
 * @return array
 */
function get_district_all_students( $no_post = -1 , $search_student = '', $filter_year = '' ) {
        $paged              = get_query_var( 'paged' ) ? get_query_var('paged') : 1 ;
        $all_students_id    = get_all_students_orders();
        $args = array(
            'numberposts'       => $no_post,
            'post_type'		=> 'student',
            'post_status'	=> 'publish',
            'orderby'           => 'modified',
            'order'             => 'DESC',
            'paged'             => $paged,
            'meta_key'          => 'registered_class',
            'meta_value'        => '',
            'meta_compare'      => '!=',
            'fields'		=> 'ids'
        );
        
        if( empty( $search_student ) && empty( $filter_course ) && empty( $filter_year ) ) {
            $args['post__in'] = empty( $all_students_id ) ? [ 0 ] : $all_students_id;
        }
        
        if( !empty( $search_student ) ) {
            $search_student = trim( $search_student );
            $tax_args = array(
                'taxonomy'      => array( 'school_district' ), 
                'fields'        => 'ids',
                'name__like'    => $search_student
            ); 
            $school_district = get_terms( $tax_args );
            $products = get_all_products( $search_student );
            $stu_name = explode(' ', $search_student);
            $args['meta_query']['relation'] = 'AND';
            $args['meta_query'][0]['relation'] = 'OR';
            if( !empty( $stu_name ) ) {
                foreach ($stu_name as $stu_key => $stu_value) {
                    $args['meta_query'][0][] =  array(
                            'key'     => 'student_first_name',
                            'value'   => $stu_value,
                            'compare' => 'LIKE',
                    );
                    $args['meta_query'][0][] =  array(
                            'key'     => 'student_last_name',
                            'value'   => $stu_value,
                            'compare' => 'LIKE',
                    ); 
                }
            }
        }
        if( !empty( $filter_year ) ) {
            $students = get_all_students_by_year( $filter_year );
            $args['post__in'] = empty( $students ) ? [ 0 ] : $students;
        }

        $students = get_posts( $args );

	return array_map( __NAMESPACE__ . '\get_student', $students );
}

/**
 * Returns array of students in a given school district
 *
 * @param integer|array $district
 * @return array
 */
function get_district_students_in_school_district( $district , $no_post = -1 , $search_student = '', $filter_year = '' ) {
	$district           = (array) $district;
        $paged              = get_query_var( 'paged' ) ? get_query_var('paged') : 1 ;
        $all_students_id    = get_all_students_orders();
        
        $args = array( 
            'numberposts'       => $no_post,
            'post_type'		=> 'student',
            'post_status'	=> 'publish',
            'paged'             => $paged,
            'orderby'           => 'modified',
            'order'             => 'DESC',
            'meta_key'          => 'registered_class',
            'meta_value'        => '',
            'meta_compare'      => '!=',
            'fields'		=> 'ids',
            'tax_query'		=> [
                    [
                            'taxonomy'	=> 'school_district',
                            'field'	=> 'term_id',
                            'terms'	=> $district
                    ]
            ] 
        );
        if( empty( $search_student ) && empty( $filter_course ) && empty( $filter_year ) ) {
            $args['post__in'] = empty( $all_students_id ) ? [ 0 ] : $all_students_id;
        }
        if( !empty( $search_student ) ) {
            $search_student = trim( $search_student );
            $tax_args = array(
                'taxonomy'      => array( 'school_district' ), 
                'fields'        => 'ids',
                'name__like'    => $search_student
            ); 
            $school_district = get_terms( $tax_args );
            $products = get_all_products( $search_student );
            $stu_name = explode(' ', $search_student);
            $args['meta_query']['relation'] = 'AND';
            $args['meta_query'][0]['relation'] = 'OR';
            if( !empty( $stu_name ) ) {
                foreach ($stu_name as $stu_key => $stu_value) {
                    $args['meta_query'][0][] =  array(
                            'key'     => 'student_first_name',
                            'value'   => $stu_value,
                            'compare' => 'LIKE',
                    );
                    $args['meta_query'][0][] =  array(
                            'key'     => 'student_last_name',
                            'value'   => $stu_value,
                            'compare' => 'LIKE',
                    ); 
                }
            }
        }
        if( !empty( $filter_year ) ) {
            $students = get_all_students_by_year( $filter_year );
            $args['post__in'] = empty( $students ) ? [ 0 ] : $students;
        }
        
	$students = get_posts( $args );

	return array_map( __NAMESPACE__ . '\get_student', $students );
}


/**
 * Get the order attached to the student
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_order( $student, $enable = false,$is_filter_month = false  ) {
    global $wpdb;

    /*
    * Add order month dropdown
    */
    global $wpdb, $wp_locale;
    $extra_checks = "AND post_status != 'auto-draft'";
    if (! isset($_GET['post_status']) || 'trash' !== $_GET['post_status']) {
        $extra_checks .= " AND post_status != 'trash'";
    } elseif (isset($_GET['post_status'])) {
        $extra_checks = $wpdb->prepare(' AND post_status = %s', $_GET['post_status']);
    }
    $months = $wpdb->get_results($wpdb->prepare("
            SELECT DISTINCT YEAR( post_date ) AS year, MONTH( post_date ) AS month
            FROM $wpdb->posts
            WHERE post_type = %s
            $extra_checks
            ORDER BY post_date DESC
    ", 'shop_order'));
    $month_count = count($months);
    if ( !empty( $months ) ) {
        $monthArray = array();
        foreach ($months as $months_key => $months_value) {
            $date_val = date('Yn');
            $month_val = $months_value->year.$months_value->month;
            if( $month_val <= $date_val ) {
                $monthArray[] = $months_value;
            }
        }
        $months = $monthArray;
        $m = isset($_GET['filter_month']) ? $_GET['filter_month'] : $months[0]->year.'-'.zeroise($months[0]->month,2);
    }
    
    $current_year_change = date("Y"); 
    $filter_year        = $_GET['filter_year'] ? $_GET['filter_year'] : $current_year_change;
    $filter_month       = $_GET['filter_month'] ? $_GET['filter_month'] : $m;
    
    if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
            $student = $student->ID;
    }
    
    if( $is_filter_month && !empty( $filter_month ) ){
        $filter_month   = explode('-', $filter_month);
        $filter_mon     = $filter_month[1];
        $filter_year    = $filter_month[0];
        $filter_month_course = " AND MONTH({$wpdb->posts}.post_date) = '{$filter_mon}' AND YEAR({$wpdb->posts}.post_date) = '{$filter_year}' ";
        $order_ids = $wpdb->get_results( $wpdb->prepare( "
           SELECT order_id
           FROM {$wpdb->prefix}woocommerce_order_itemmeta 
           JOIN {$wpdb->prefix}woocommerce_order_items 
               ON {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id
               AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = 'Student'
           JOIN {$wpdb->posts} 
               ON {$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID
               AND {$wpdb->posts}.post_type = 'shop_order'
               AND {$wpdb->posts}.post_status NOT IN ( 'wc-cancelled', 'wc-refunded', 'wc-failed','trash' )
               $filter_month_course
           WHERE meta_key = 'Student'
           AND   meta_value = '%s'
           ORDER BY {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id DESC
           ;
       ", $student ) );
    } else if( !empty( $filter_year )){
        $order_ids = $wpdb->get_results( $wpdb->prepare( "
           SELECT order_id
           FROM {$wpdb->prefix}woocommerce_order_itemmeta 
           JOIN {$wpdb->prefix}woocommerce_order_items 
               ON {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id
               AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = 'Student'
           JOIN {$wpdb->posts} 
               ON {$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID
               AND {$wpdb->posts}.post_type = 'shop_order'
               AND {$wpdb->posts}.post_status NOT IN ( 'wc-cancelled', 'wc-refunded', 'wc-failed','trash' )
               AND YEAR({$wpdb->posts}.post_date) = '{$filter_year}'
           WHERE meta_key = 'Student'
           AND   meta_value = '%s'
           ORDER BY {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id DESC
           ;
       ", $student ) );
   }else{
	$order_ids = $wpdb->get_results( $wpdb->prepare( "
		SELECT order_id
		FROM {$wpdb->prefix}woocommerce_order_itemmeta 
		JOIN {$wpdb->prefix}woocommerce_order_items 
			ON {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id
			AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = 'Student'
		JOIN {$wpdb->posts} 
			ON {$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID
			AND {$wpdb->posts}.post_type = 'shop_order'
			AND {$wpdb->posts}.post_status NOT IN ( 'wc-cancelled', 'wc-refunded', 'wc-failed','trash' )
		WHERE meta_key = 'Student'
		AND   meta_value = '%s'
		ORDER BY {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id DESC
		;
	", $student ) );
   }
	if ( $order_ids ) {
            if( $enable ) {
                $orderidArray = array();
                foreach ($order_ids as $order_ids_key => $order_ids_value) {
                    $order_id = $order_ids_value->order_id;
                    $orderidArray[] = $order_id;
                }
                $orderidArray = array_unique($orderidArray);
                return $orderidArray;
            } else {
                $order_id = $order_ids[0]->order_id;
                return wc_get_order( $order_id );
            }
        }

	return false;
}

/**
 * Returns student registration line item
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_registration_line_item( $student,$order_id = '') {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

        if( !$order_id) {
            $order = get_student_order( $student );
        } else {
            $order = wc_get_order( $order_id );
        }

	if ( $order ) {
		foreach ( $order->get_items() as $item ) {
			$product = $item->get_product();

			if ( $student == $item->get_meta( 'Student' ) ) {
				return $item;
			}
		}
	}

	return false;
}

/**
 * Returns student registered class
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_registered_class( $student,$order_id = '' ) {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$order_line_item = get_student_registration_line_item( $student,$order_id );

	if ( $order_line_item ) {
		$product = $order_line_item->get_product();

		return $product;
	}

	return false;
}

/**
 * Returns student Product teacher
 *
 * @param mixed $product
 * @return mixed
 */
function get_student_registered_class_teacher( $product ,$enable_array = false ) {
        $teacher = get_post_meta( $product, 'teacher', true );
        
        if( !empty( $teacher ) ) {
            
            $teacher_name = '';
            $teacher_nameArray = array();
            foreach ($teacher as $teacher_key => $teacher_value) {
                if( $enable_array ) {
                    $teacher_nameArray[] =  $teacher_value;
                } else {
                    $teacher_name =  get_the_title( $teacher_value ).  ', ' . $teacher_name ;
                }
            }
            if( $enable_array ) {
                return $teacher_nameArray;
            } else {
                $teacher_name = trim($teacher_name, ', ');
       
                return $teacher_name;
            }
            
        }
        return false;
}

/**
 * Returns student parent/guardian 1
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_parent_guardian_1( $student , $enable_id = false ,$order_id = '' ) {
        if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$registration_line = get_student_registration_line_item( $student ,$order_id);

	if ( $registration_line ) {
		$parent_guardian_1 = get_parent_guardian( $registration_line->get_meta( 'Parent/Guardian 1' ) );
                if( $enable_id ) {
                    $value = $parent_guardian_1->ID;
                } else {
                    $value = $parent_guardian_1->get_full_name();
                }
		return $value;
	}

	return false;
}

/**
 * Returns student registration phone
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_order_phone( $student ) {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$order = get_student_order( $student );

	if ( $order ) {
		$data           = $order->get_data();
                $billing_phone  = $data['billing']['phone'];
                return $billing_phone;
        }

	return false;
}

/**
 * Returns student registration email
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_order_email( $student ) {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$order = get_student_order( $student );

	if ( $order ) {
		$data               = $order->get_data();
                $billing_email      = $data['billing']['email'];
                return $billing_email;
        }

	return false;
}

/**
 * Returns student registration address
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_order_address( $student ) {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$order = get_student_order( $student );

	if ( $order ) {
                $customer_id = $order->get_customer_id(); 
                $customer = new \WC_Customer( $customer_id );
                $billing_address       = $customer->get_billing_address_1().' '.$customer->get_billing_address_2();
                $billing_address      .= empty( $billing_address ) ? '' : '<br>';
                $billing_address      .= $customer->get_billing_city();
                $billing_address      .= empty( $billing_address ) ? '' : ' ';
                $billing_address      .= $customer->get_billing_state();
                $billing_address      .= empty( $billing_address ) ? '' : '<br>';
                $billing_address      .= $customer->get_billing_postcode();
                $billing_address      .= empty( $billing_address ) ? '' : ' ';
                $billing_address      .= $customer->get_billing_country();
		$billing_address       = preg_match('/[A-Za-z]/', $billing_address) ? $billing_address : '-';
                return $billing_address;
        }

	return false;
}

/**
 * Returns student registration address
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_order_without_html_address( $student ) {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$order = get_student_order( $student );

	if ( $order ) {
                $customer_id = $order->get_customer_id(); 
                $customer = new \WC_Customer( $customer_id );
		$billing_address       = $customer->get_billing_address_1().' '.$customer->get_billing_address_2();
                $billing_address      .= !empty( $billing_address ) ? ' ' : '';
                $billing_address      .= $customer->get_billing_city();
                $billing_address      .= !empty( $billing_address ) ? ' ' : '';
                $billing_address      .= $customer->get_billing_state();
                $billing_address      .= !empty( $billing_address ) ? ' ' : '';
                $billing_address      .= $customer->get_billing_postcode();
                $billing_address      .= !empty( $billing_address ) ? ' ' : '';
                $billing_address      .= $customer->get_billing_country();
                $billing_address       = preg_match('/[A-Za-z]/', $billing_address) ? $billing_address : '-';
                return $billing_address;
        }

	return false;
}

/**
 * Returns student registration tuition balance
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_order_tuition_balance( $student,$order_id = '' ) {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	if( !$order_id) {
            $order = get_student_order( $student );
        } else {
            $order = wc_get_order( $order_id );
        }


	if ( $order ) {
		foreach ( $order->get_items() as $item ) {
			$product = $item->get_product();

			if ( $student == $item->get_meta( 'Student' ) ) {
                                return $item->get_total();
			}
		}
	}

	return false;
}

/**
 * Returns student registration financial aid
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_order_financial_aid( $student,$order_id='' ) {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	if( !$order_id) {
            $order = get_student_order( $student );
        } else {
            $order = wc_get_order( $order_id );
        }


	if ( $order ) {
		$financial_aid    = $order->get_meta( 'financial_aid' );
                return $financial_aid;
        }

	return false;
}

/**
 * Returns student registration order year
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_order_year( $student ) {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$order = get_student_order( $student );

	if ( $order ) {
		$year    = $order->get_date_created()->date("Y");
                return $year;
        }

	return false;
}

/**
 * Returns all products
 *
 * @return array
 */
function get_all_products( $search_student = '' ) {
        $pro_args = [
		'numberposts'		=> -1,
		'post_type'		=> 'product',
		'post_status'		=> 'publish',
        	'fields'		=> 'ids'
	];
        if( !empty( $search_student ) ) {
            if( function_exists('title_filter') ) {
                    add_filter( 'posts_where', 'title_filter', 10, 2);
                    $pro_args['search_prod_title'] = $search_student;
                    $products = get_posts( $pro_args );
                    remove_filter( 'posts_where', 'title_filter', 10, 2);
            } else {
                $pro_args['s'] = $search_student;
                $products = get_posts( $pro_args );
            }
            return $products;
        }
    	$products = get_posts( $pro_args );
	return $products;
}

/**
 * Returns all students by year
 *
 * @return array
 */
function get_all_students_by_year( $year = '', $filter_course = '',$filter_month = '' ) {
        global $wpdb;
        /* if( !empty( $year ) && !empty( $filter_course ) ) {
            $students = $wpdb->get_results( $wpdb->prepare( "
                SELECT *
                FROM {$wpdb->prefix}woocommerce_order_itemmeta 
                JOIN {$wpdb->prefix}woocommerce_order_items 
                        ON {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id
                        AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = '_product_id'
                JOIN {$wpdb->posts} 
                        ON {$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID
                        AND {$wpdb->posts}.post_type = 'shop_order'
                        AND {$wpdb->posts}.post_status NOT IN ( 'wc-cancelled', 'wc-refunded', 'wc-failed','trash' )
                        AND YEAR({$wpdb->posts}.post_date) = '{$year}'
                WHERE meta_key = '_product_id'
                AND   meta_value = {$filter_course}
                ORDER BY {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id DESC
                ;"
            ));
        } else if( empty( $year ) && !empty( $filter_course ) ) {
            $students = $wpdb->get_results( $wpdb->prepare( "
                SELECT *
                FROM {$wpdb->prefix}woocommerce_order_itemmeta 
                JOIN {$wpdb->prefix}woocommerce_order_items 
                        ON {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id
                        AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = '_product_id'
                JOIN {$wpdb->posts} 
                        ON {$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID
                        AND {$wpdb->posts}.post_type = 'shop_order'
                        AND {$wpdb->posts}.post_status NOT IN ( 'wc-cancelled', 'wc-refunded', 'wc-failed','trash' )
                WHERE meta_key = '_product_id'
                AND   meta_value = {$filter_course}
                ORDER BY {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id DESC
                ;"
            ));
        } else if( !empty( $year ) && empty( $filter_course ) ) {
            $students = $wpdb->get_results( $wpdb->prepare( "
                SELECT *
                FROM {$wpdb->prefix}woocommerce_order_itemmeta 
                JOIN {$wpdb->prefix}woocommerce_order_items 
                        ON {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id
                        AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = 'Student'
                JOIN {$wpdb->posts} 
                        ON {$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID
                        AND {$wpdb->posts}.post_type = 'shop_order'
                        AND {$wpdb->posts}.post_status NOT IN ( 'wc-cancelled', 'wc-refunded', 'wc-failed','trash' )
                        AND YEAR({$wpdb->posts}.post_date) = '{$year}'
                WHERE meta_key = 'Student'
                AND   meta_value IS NOT NULL 
                ORDER BY {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id DESC
                ;"
            ));
        } */
      
        if( !empty( $year ) && empty( $filter_month ) ) {
            if( !empty( $year ) && !empty( $filter_course ) ) { 
                $filter_month_course = "AND YEAR({$wpdb->posts}.post_date) = '{$year}'
                    WHERE meta_key = '_product_id'
                    AND   meta_value = {$filter_course} ";
                $order_meta_key = '_product_id';
            } else if( empty( $year ) && !empty( $filter_course ) ) {
                $filter_month_course = " WHERE meta_key = '_product_id'
                    AND   meta_value = {$filter_course} ";
                $order_meta_key = '_product_id';
            } else if( !empty( $year ) && empty( $filter_course ) ) { 
                $filter_month_course = "AND YEAR({$wpdb->posts}.post_date) = '{$year}'
                    WHERE meta_key = 'Student'
                    AND meta_value IS NOT NULL  ";
                $order_meta_key = 'Student';
            }
            $query = "SELECT *
            FROM {$wpdb->prefix}woocommerce_order_itemmeta 
            JOIN {$wpdb->prefix}woocommerce_order_items 
                    ON {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id
                    AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = '{$order_meta_key}'
            JOIN {$wpdb->posts} 
                    ON {$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID
                    AND {$wpdb->posts}.post_type = 'shop_order'
                    AND {$wpdb->posts}.post_status NOT IN ( 'wc-cancelled', 'wc-refunded', 'wc-failed','trash' )
                    $filter_month_course
            ORDER BY {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id DESC
            ;";
            $students = $wpdb->get_results( $wpdb->prepare( $query )); 
        }
        if( !empty( $filter_month ) || empty( $year ) ) { 
            if( !empty( $filter_month ) && !empty( $filter_course ) ) { 
                $filter_month   = explode('-', $filter_month);
                $filter_mon     = $filter_month[1];
                $filter_year    = $filter_month[0];
                $filter_month_course = " AND MONTH({$wpdb->posts}.post_date) = '{$filter_mon}' AND YEAR({$wpdb->posts}.post_date) = '{$filter_year}'
                    WHERE meta_key = '_product_id'
                    AND   meta_value = {$filter_course} ";
                $order_meta_key = '_product_id';
            } else if( empty( $filter_month ) && !empty( $filter_course ) ) {
                $filter_month_course = " WHERE meta_key = '_product_id'
                    AND   meta_value = {$filter_course} ";
                $order_meta_key = '_product_id';
            } else if( !empty( $filter_month ) && empty( $filter_course ) ) { 
                $filter_month = explode('-', $filter_month);
                $filter_mon   = $filter_month[1];
                $filter_year  = $filter_month[0];
                $filter_month_course = "AND MONTH({$wpdb->posts}.post_date) = '{$filter_mon}' AND YEAR({$wpdb->posts}.post_date) = '{$filter_year}'
                    WHERE meta_key = 'Student'
                    AND meta_value IS NOT NULL  ";
                $order_meta_key = 'Student';
            }
            $query = "SELECT *
            FROM {$wpdb->prefix}woocommerce_order_itemmeta 
            JOIN {$wpdb->prefix}woocommerce_order_items 
                    ON {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id
                    AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = '{$order_meta_key}'
            JOIN {$wpdb->posts} 
                    ON {$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID
                    AND {$wpdb->posts}.post_type = 'shop_order'
                    AND {$wpdb->posts}.post_status NOT IN ( 'wc-cancelled', 'wc-refunded', 'wc-failed','trash' )
                    $filter_month_course
            ORDER BY {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id DESC
            ;";
            $students = $wpdb->get_results( $wpdb->prepare( $query )); 
        } 
       
        if ( !empty( $students ) ) {
            $student_ids = array();
            foreach ($students as $student_key => $student_value) {
                $item_id    = $student_value->order_item_id;
                $stu_id     = wc_get_order_item_meta( $item_id, 'Student', true );
                $student_ids[] = $stu_id;
            }
            return $student_ids;
	}
	return false;
}

/**
 * Returns checkout process complete all students 
 *
 * @return array
 */
function get_all_students_orders() {
        global $wpdb;

        $students = $wpdb->get_results( $wpdb->prepare( "
            SELECT meta_value
            FROM {$wpdb->prefix}woocommerce_order_itemmeta 
            JOIN {$wpdb->prefix}woocommerce_order_items 
                    ON {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id
                    AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = 'Student'
            JOIN {$wpdb->posts} 
                    ON {$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID
                    AND {$wpdb->posts}.post_type = 'shop_order'
                    AND {$wpdb->posts}.post_status NOT IN ( 'wc-cancelled', 'wc-refunded', 'wc-failed','trash')
            WHERE meta_key = 'Student'
            AND   meta_value IS NOT NULL 
            ORDER BY {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id DESC
            ;"
        ));

	if ( !empty( $students ) ) {

            $student_ids = array();
            foreach ($students as $student_key => $student_value) {
                $student_ids[] = $student_value->meta_value;
            }
            return $student_ids;
	}

	return false;
}

/**
 * Returns all Teachers
 *
 * @return array
 */
function get_all_teacher() {
        $teacher_args = [
		'numberposts'		=> -1,
		'post_type'		=> 'teacher',
		'post_status'		=> 'publish',
        	'fields'		=> 'ids'
	];
        $teachers = get_posts( $teacher_args );
	return $teachers;
}

/**
 * Returns all district
 *
 * @return array
 */
function get_all_district() {
        $district_args = [
		'taxonomy'      =>  'school_district',
                'hide_empty'    => false,
        	'fields'	=> 'ids'
	];
        $districts = get_terms( $district_args );
	return $districts;
}


/**
 * Get  all parents/guardians
 *
 * @param integer $user_id
 * @return array
 */
function get_all_parents_guardians( ) {
	$parent_args = [
		'numberposts'		=> -1,
		'post_type'		=> 'parent_guardian',
		'post_status'		=> 'publish',
        	'fields'		=> 'ids'
	];
        $parents = get_posts( $parent_args );
	return $parents;
	
}


/**
 * Get  emergency phone
 *
 * @param integer $user_id
 * @return array
 */
function get_student_emergency_phone( $student ) {
        if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}
        
	$contacts = get_field( 'student_emergency_contacts',$student );
       
        if( !empty( $contacts ) ) {
            $phone_string = '';
            foreach ($contacts as $contact_key => $contact_value) {
                $phone = $contact_value['phone'];
                $phone_string = $phone_string .' , '.$phone;
            }
            $phone_string = trim($phone_string, ', ');
            return $phone_string;
        }
        
	return false;
}

/**
 * Get  emergency email
 *
 * @param integer $user_id
 * @return array
 */
function get_student_emergency_email( $student ) {
        if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}
        
	$contacts = get_field( 'student_emergency_contacts',$student );
        
        if( !empty( $contacts ) ) {
            
            $email_string = '';
            
            foreach ($contacts as $contact_key => $contact_value) {
                $email = $contact_value['email'];
                $email_string = $email_string .' , '.$email;
            }
            
            $email_string = trim($email_string, ', ');
            return $email_string;
            
        }
        
	return false;
}

/**
 * Get  emergency name
 *
 * @param integer $user_id
 * @return array
 */
function get_student_emergency_name( $student ) {
        if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}
        
	$contacts = get_field( 'student_emergency_contacts',$student );
        
        if( !empty( $contacts ) ) {
            
            $email_string = '';
            
            foreach ($contacts as $contact_key => $contact_value) {
                $email = $contact_value['name'];
                $email_string = $email_string .' , '.$email;
            }
            
            $email_string = trim($email_string, ', ');
            return $email_string;
            
        }
        
	return false;
}

/**
 * Get the order attached to the student
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_has_order( $student,$item_val,$orderid ) {
	global $wpdb;

	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$order_id = $wpdb->get_results( $wpdb->prepare( "
		SELECT order_id
		FROM {$wpdb->prefix}woocommerce_order_itemmeta 
		JOIN {$wpdb->prefix}woocommerce_order_items 
			ON {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id
			AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = 'Student'
		JOIN {$wpdb->posts} 
			ON {$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID
			AND {$wpdb->posts}.post_type = 'shop_order'
			AND {$wpdb->posts}.post_status NOT IN ( 'wc-cancelled', 'wc-refunded', 'wc-failed','trash' )
		WHERE ( meta_key = 'Student' AND meta_value = {$student} ) 
                ORDER BY {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id DESC;
	" ) );
        if( !empty( $order_id ) ) {
            $flag = false;
            foreach ($order_id as $order_key => $order_value) {
                $order = wc_get_order( $order_value->order_id );
                if( $orderid != $order_value->order_id ) {
                    foreach ( $order->get_items() as $item ) {
                            
                            $product = $item->get_product();
                            if ( $student == $item->get_meta( 'Student' ) ) {
                                    if( $item->get_product_id() == $item_val ) {
                                        $flag = true;
                                        break;
                                    }
                            }
                          
                    }
                }
            }
            return $flag;
        }

	return false;
}

/**
 * Returns array of parents/guardians to populate a select dropdown
 *
 * @param integer $user_id
 * @return array
 */
function get_order_parents_guardians_options_array( $order_id ) {
      
        if( !empty( $order_id ) ) {
        
            $order      = wc_get_order( $order_id );
            $user_id    = $order->get_user_id();
         
            $parents_guardians = get_user_parents_guardians( $user_id );

            $options = [];

            foreach ( $parents_guardians as $parent_guardian ) {
                    $options[ $parent_guardian->ID ] = $parent_guardian->get_full_name();
            }
            return $options;
        }
        
	return false;
}
/**
 * Returns student registered class
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_registered_class_id( $student,$order_id = '',$pro_id ) {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$order_line_item = get_student_registration_line_item( $student,$order_id );

	if ( $order_line_item ) {
		$product = $order_line_item->get_product();

		return $product;
	}

	return false;
}

/**
 * Returns student registration address
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_order_user_address( $student ) {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$order = get_student_order( $student );

	if ( $order ) {
                $customer_id = $order->get_customer_id(); 
                $customer = new \WC_Customer( $customer_id );
		$billing_address       = $customer->get_billing_address_1().' '.$customer->get_billing_address_2();
                $billing_address       = !empty($billing_address) ? $billing_address : '-';
                return $billing_address;
        }

	return false;
}

/**
 * Returns student registration address
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_order_user_city( $student ) {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$order = get_student_order( $student );

	if ( $order ) {
                $customer_id = $order->get_customer_id(); 
                $customer = new \WC_Customer( $customer_id );
		$billing_address      = $customer->get_billing_city();
                $billing_address      = !empty($billing_address) ? $billing_address : '-';
                return $billing_address;
        }

	return false;
}

/**
 * Returns student registration address
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_order_user_state( $student ) {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$order = get_student_order( $student );

	if ( $order ) {
                $customer_id = $order->get_customer_id(); 
                $customer = new \WC_Customer( $customer_id );
		$billing_address      = $customer->get_billing_state();
                $billing_address      = !empty($billing_address) ? $billing_address : '-';
                return $billing_address;
        }

	return false;
}

/**
 * Returns student registration address
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_order_user_country( $student ) {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$order = get_student_order( $student );

	if ( $order ) {
                $customer_id = $order->get_customer_id(); 
                $customer = new \WC_Customer( $customer_id );
		$billing_address      = $customer->get_billing_country();
                $billing_address      = !empty($billing_address) ? $billing_address : '-';
                return $billing_address;
        }

	return false;
}

/**
 * Returns student registration address
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_order_user_postal_code( $student ) {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$order = get_student_order( $student );

	if ( $order ) {
                $customer_id = $order->get_customer_id(); 
                $customer = new \WC_Customer( $customer_id );
                $billing_address      = $customer->get_billing_postcode();
                $billing_address      = !empty($billing_address) ? $billing_address : '-';
                return $billing_address;
        }

	return false;
}