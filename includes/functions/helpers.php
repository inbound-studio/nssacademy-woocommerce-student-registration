<?php
/**
 * Helper functions
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration\Functions;

/**
 * Returns array of integration settings
 *
 * @return array
 */
function get_settings() {
    return get_option( 'woocommerce_wc-student-registration_settings' );
}

/**
 * Returns the page ID for the financial aid district rep orders
 *
 * @return int
 */
function get_financial_aid_orders_page_id() {
    $settings = get_settings();

    return (int) $settings['financial_aid_page_id'] ?? null;
}

/**
 * Returns the out of school district fee
 *
 * @return float
 */
function get_out_of_district_fee() {
	$settings = get_settings();

    return (float) $settings['out_of_district_fee'] != '' ? $settings['out_of_district_fee'] : 25.00;
}

/**
 * Returns the financial aid director email
 *
 * @return string
 */
function get_financial_aid_director_email() {
	$settings = get_settings();

    return (string) $settings['financial_aid_director_email'] != '' ? $settings['financial_aid_director_email'] : get_option( 'admin_email' );
}

/**
 * Returns whether or not the product is in stock against the held stock
 *
 * @param WC_Product $product
 * @return boolean
 */
function product_in_stock( $product ) {
	if ( ! is_a( $product, 'WC_Product' ) ) {
		$product = wc_get_product( $product );
	}

	$current_session_order_id   = isset( WC()->session->order_awaiting_payment ) ? absint( WC()->session->order_awaiting_payment ) : 0;
	$product_qty_in_cart        = WC()->cart->get_cart_item_quantities();
	$required_stock             = isset( $product_qty_in_cart[ $product->get_stock_managed_by_id() ] ) ? $product_qty_in_cart[ $product->get_stock_managed_by_id() ] : 1;
	$held_stock                 = wc_get_held_stock_quantity( $product, $current_session_order_id ) ?? 0;

	if ( ( $product->managing_stock() || $product->backorders_allowed() ) && $product->get_stock_quantity() < ( $held_stock + $required_stock ) ) {
		return false;
	}

	return true;
}

/**
 * Creates a financial aid coupon
 *
 * @param mixed $amount
 * @return string
 */
function get_financial_aid_discount_coupon( $amount ) {
	$chars  = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$code   = '';
	for ( $i = 0; $i < $length; $i++ ) {
		$code .= $chars[ mt_rand( 0, strlen( $chars ) - 1 ) ];
	}

	$coupon_code = 'wcsrfa' . $code;

	$new_coupon_id = wp_insert_post( [
		'post_title'    => $coupon_code,
		'post_content'  => '',
		'post_status'   => 'publish',
		'post_author'   => 1,
		'post_type'		=> 'shop_coupon',
		'post_excerpt'  => __( 'Financial aid discount', 'wc-student-registration' )
	] );
	$coupon_update = array(
		'ID'         => $new_coupon_id,
		'post_title' => $coupon_code.'-'.$new_coupon_id
	);
	wp_update_post( $coupon_update );
	$coupon_code = $coupon_code.'-'.$new_coupon_id;
	update_post_meta( $new_coupon_id, 'discount_type', 'fixed_cart' );
	update_post_meta( $new_coupon_id, 'coupon_amount', $amount );
	update_post_meta( $new_coupon_id, 'individual_use', 'no' );
	update_post_meta( $new_coupon_id, 'product_ids', '' );
	update_post_meta( $new_coupon_id, 'exclude_product_ids', '' );
	update_post_meta( $new_coupon_id, 'usage_limit', '1' );
	update_post_meta( $new_coupon_id, 'usage_limit_per_user', '1' );
	update_post_meta( $new_coupon_id, 'limit_usage_to_x_items', '1' );
	update_post_meta( $new_coupon_id, 'expiry_date', strtotime( '+60 days' ) );
	update_post_meta( $new_coupon_id, 'apply_before_tax', 'yes' );
	update_post_meta( $new_coupon_id, 'free_shipping', 'no' );

	return $coupon_code;
}

/**
 * Returns the pre_reg_timestamp
 *
 * @return float
 */
function get_pre_reg_timestamp() {
        $settings   = get_settings();
        $d          = date($settings['pre_registration_time']);

        return $settings['pre_registration_time'] != '' ? ($d) : '';
}
/**
 * Returns the registration_time
 *
 * @return float
 */
function get_reg_timestamp() {
        $settings   = get_settings();
        $d          = date($settings['registration_time']);
        return $settings['registration_time'] != '' ? ($d) : '';
}

/**
 * Returns the pre_reg_timestamp
 *
 * @return float
 */
function get_explorimg_pre_reg_timestamp() {
        $settings   = get_settings();
        $d          = date($settings['exploring_pre_registration_time']);

        return $settings['exploring_pre_registration_time'] != '' ? ($d) : '';
}
/**
 * Returns the registration_time
 *
 * @return float
 */
function get_exploring_reg_timestamp() {
        $settings   = get_settings();
        $d          = date($settings['exploring_registration_time']);
        return $settings['exploring_registration_time'] != '' ? ($d) : '';
}
/**
 * Returns the pre_registration_password
 *
 * @return float
 */
function get_pre_reg_password() {
        $settings = get_settings();

        return $settings['pre_registration_password'] != '' ? ($settings['pre_registration_password']) : '';
}
/**
 * Returns the registration_password
 *
 * @return float
 */
function get_reg_password() {
        $settings = get_settings();

        return $settings['registration_password'] != '' ? ($settings['registration_password']) : '';
}