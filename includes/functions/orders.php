<?php
/**
 * Order functions
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration\Functions;

/**
 * Returns whether a given order has a class registration or not
 *
 * @param mixed $order
 * @return boolean
 */
function order_has_class( $order ) {
    if ( ! is_a( $order, 'WC_Order' ) ) {
        $order = wc_get_order( $order );
    }

    if ( ! $order ) {
        return false;
    }

    foreach ( $order->get_items() as $item ) {
        $product = is_callable( [ $item, 'get_product' ] ) ? $item->get_product() : false;

        if ( is_a( $product, 'WC_Student_Registration\\WC_Product_Class' ) ) {
            return true;
        }
    }

    return false;
}

/**
 * Return orders with a given school district association
 *
 * @param integer $school_district
 * @return array
 */
function get_school_district_orders( $school_district = '',$filter_district,$posts_per_page,$enable, $filter_year = '' ) {
    $paged              = get_query_var( 'paged' ) ? get_query_var('paged') : 1 ;
    $orderArray         = get_all_order_has_student();
   
    $args = [
        'post_status'       => [
            'wc-processing', 'wc-completed', 'wc-pending', 'pending', 'wc-partial-payment', 'wc-pending-fa', 'pending-fa'
            //'wc-completed','wc-pending', 'pending', 'wc-partial-payment', 'wc-pending-fa', 'pending-fa'
        ],
        'paged'             => $paged,
        'numberposts'       => $posts_per_page,
        'financial_aid'     => 'yes',
    ];

    $args['post__in'] = empty( $orderArray ) ? [ 0 ] : $orderArray;
    
    if ( $enable ) {
		 $filterArray = get_all_order_has_district_by_year( $filter_year,$school_district );
        //$filterArray        = get_all_order_has_district( $school_district);
        $args['post__in'] = empty( $filterArray ) ? [ 0 ] : $filterArray;
	
    }
	
	   if( !empty( $filter_district ) || !empty( $filter_year )  ) {
            $filterArray = get_all_order_has_district_by_year( $filter_year,$filter_district );
            $args['post__in'] = empty( $filterArray ) ? [ 0 ] : $filterArray;
        }
  
    $orders = wc_get_orders( $args );

    return $orders;
}

/**
 * Returns a given orders school district
 *
 * @param mixed $order
 * @return integer|boolean
 */
function get_order_school_district( $order ) {
	if ( ! $order instanceof \WC_Order ) {
		$order = wc_get_order( $order );
	}

	if ( $order ) {
        foreach ( $order->get_items() as $item ) {
			$school_district = $item->get_meta( 'school_district' );

			if ( $school_district ) {
				return absint( $school_district );
			}
		}
	}

	return false;
}

/**
 * Returns a given orders students
 *
 * @param mixed $order
 * @return array
 */
function get_order_students( $order ) {
	if ( ! $order instanceof \WC_Order ) {
		$order = wc_get_order( $order );
    }
    
    $students = [];

	if ( $order ) {
        foreach ( $order->get_items() as $item ) {
			$students[] = $item->get_meta( 'Student' );
		}
	}

	return array_filter( $students );
}

/**
 * Return the given financial aid amount for an order
 *
 * @param mixed $order
 * @return mixed
 */
function get_order_financial_aid_amount( $order ) {
    if ( ! $order instanceof \WC_Order ) {
		$order = wc_get_order( $order );
	}

	if ( $order ) {
        return (float) $order->get_meta( 'financial_aid_amount' ) != '' ? $order->get_meta( 'financial_aid_amount' ) : 0;
    }

    return false;
}

/**
 * Return the pending financial aid amount for an order
 *
 * @param mixed $order
 * @return mixed
 */
function get_order_pending_financial_aid_amount( $order ) {
    if ( ! $order instanceof \WC_Order ) {
		$order = wc_get_order( $order );
	}

	if ( $order ) {
        return (float) $order->get_meta( 'pending_financial_aid_amount' ) != '' ? $order->get_meta( 'pending_financial_aid_amount' ) : 0;
    }

    return false;
}

/**
 * Returns whether the given order needs review by a director
 *
 * @param mixed $order
 * @return boolean
 */
function get_order_financial_aid_needs_review( $order ) {
    if ( ! $order instanceof \WC_Order ) {
		$order = wc_get_order( $order );
	}

	if ( $order ) {
        return (bool) $order->get_meta( 'pending_financial_aid_needs_review' ) != '' ? $order->get_meta( 'pending_financial_aid_needs_review' ) : false;
    }

    return false;
}

/**
 * Returns financial aid notes from director
 *
 * @param mixed $order
 * @return string
 */
function get_order_financial_aid_notes( $order ) {
    if ( ! $order instanceof \WC_Order ) {
		$order = wc_get_order( $order );
	}

	if ( $order ) {
        return $order->get_meta( 'financial_aid_notes' );
    }

    return '';
}

/**
 * Applies a coupon for financial aid to an order
 *
 * @param mixed $order
 * @param mixed $amount
 * @return boolean
 */
function update_order_financial_aid_discount( $order, $amount ) {
    if ( ! $order instanceof \WC_Order ) {
		$order = wc_get_order( $order );
	}

	if ( $order ) {
        $coupons = $order->get_coupons();

        foreach ( $coupons as $coupon ) {
            $code = $coupon->get_code();

            if ( substr( $code, 0, 6 ) === 'wcsrfa' ) {
                $order->remove_coupon( $code );
            }
        }

        if ( $amount > 0 ) {
            $coupon_code = get_financial_aid_discount_coupon( $amount );
            $order->apply_coupon( $coupon_code );
            $order->save();

            return true;
        }
    }

    return false;
}

/**
 * Get the order attached to the student
 *
 * @param mixed $student
 * @return mixed
 */
function get_order_has_student( $order_id ) {
	global $wpdb;

	$order_id = $wpdb->get_results( $wpdb->prepare( "
		SELECT order_id
		FROM {$wpdb->prefix}woocommerce_order_itemmeta 
		JOIN {$wpdb->prefix}woocommerce_order_items 
			ON {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id
			AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = 'Student'
		JOIN {$wpdb->posts} 
			ON {$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID
			AND {$wpdb->posts}.post_type = 'shop_order'
			AND {$wpdb->posts}.post_status NOT IN ( 'wc-cancelled', 'wc-refunded', 'wc-failed','trash' )
		WHERE order_id = {$order_id} AND ( meta_key = 'Student' AND meta_value IS NOT NULL )
                ORDER BY {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id DESC;
	" ) );
        if( !empty( $order_id ) ) {
            return $order_id[0]->order_id;
        }

	return false;
}


/**
 * Get the all order attached to the student
 *
 * @param mixed $student
 * @return mixed
 */
function get_all_order_has_student() {
	global $wpdb;

	$order_ids = $wpdb->get_results( $wpdb->prepare( "
		SELECT order_id
		FROM {$wpdb->prefix}woocommerce_order_itemmeta 
		JOIN {$wpdb->prefix}woocommerce_order_items 
			ON {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id
			AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = 'Student'
		JOIN {$wpdb->posts} 
			ON {$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID
			AND {$wpdb->posts}.post_type = 'shop_order'
			AND {$wpdb->posts}.post_status NOT IN ( 'wc-cancelled', 'wc-refunded', 'wc-failed','trash' )
		WHERE meta_key = 'Student' AND meta_value IS NOT NULL
                ORDER BY {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id DESC;
	" ) );
        if ( !empty( $order_ids ) ) {
            $order_idArray = array();
            foreach ($order_ids as $order_key => $order_value) {
                $order_id = $order_value->order_id;
                $order = wc_get_order( $order_id );
                if( $order->get_meta( 'financial_aid' ) == 'yes' ) {
                    $order_idArray[] = $order_id;
                }
            }
            return $order_idArray;
	}

	return false;
}

/**
 * Get the all order attached to the student by year
 *
 * @param mixed $student
 * @return mixed
 */ 
function get_all_order_has_district_by_year($year = '', $filter_district = '') {
	global $wpdb;
if(empty($filter_district)){
$order_ids = $wpdb->get_results( $wpdb->prepare( "
		SELECT order_id
		FROM {$wpdb->prefix}woocommerce_order_itemmeta 
		JOIN {$wpdb->prefix}woocommerce_order_items 
			ON {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id
			AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = 'Student'
		JOIN {$wpdb->posts} 
			ON {$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID
			AND {$wpdb->posts}.post_type = 'shop_order'
			AND {$wpdb->posts}.post_status NOT IN ( 'wc-cancelled', 'wc-refunded', 'wc-failed','trash' )
		WHERE meta_key = 'Student' AND meta_value IS NOT NULL
		AND   post_date BETWEEN '{$year}-01-01' AND '{$year}-12-31'
                ORDER BY {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id DESC;
	" ) );
        if ( !empty( $order_ids ) ) {
            $order_idArray = array();
            foreach ($order_ids as $order_key => $order_value) {
                $order_id = $order_value->order_id;
                $order = wc_get_order( $order_id );
                if( $order->get_meta( 'financial_aid' ) == 'yes' ) {
                    $order_idArray[] = $order_id;
                }
            }
            return $order_idArray;
	}
	return false;
}else{
	
	
	$order_ids = $wpdb->get_results( $wpdb->prepare( "
		SELECT order_id
		FROM {$wpdb->prefix}woocommerce_order_itemmeta 
		JOIN {$wpdb->prefix}woocommerce_order_items 
			ON {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id
			AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = 'Student'
		JOIN {$wpdb->posts} 
			ON {$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID
			AND {$wpdb->posts}.post_type = 'shop_order'
			AND {$wpdb->posts}.post_status NOT IN ( 'wc-cancelled', 'wc-refunded', 'wc-failed','trash' )
		WHERE meta_key = 'Student' AND meta_value IS NOT NULL
		 AND   post_date BETWEEN '{$year}-01-01' AND '{$year}-12-31'
                ORDER BY {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id DESC;
	" ) );

        if ( !empty( $order_ids ) ) {
            $order_idArray = array();
            foreach ($order_ids as $order_key => $order_value) {
                $order_id = $order_value->order_id;
                $order = wc_get_order( $order_id );
                if( $order->get_meta( 'financial_aid' ) == 'yes' && $order->get_meta( 'school_district' ) == $filter_district ) {
                    $order_idArray[] = $order_id;
                }
            }
            return $order_idArray;
	}

	return false;
}
}
		
/**
 * Get the all order attached to the student
 *
 * @param mixed $student
 * @return mixed
 */
function get_all_order_has_district( $filter_district) {
	global $wpdb;

	
	
	$order_ids = $wpdb->get_results( $wpdb->prepare( "
		SELECT order_id
		FROM {$wpdb->prefix}woocommerce_order_itemmeta 
		JOIN {$wpdb->prefix}woocommerce_order_items 
			ON {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id
			AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = 'Student'
		JOIN {$wpdb->posts} 
			ON {$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID
			AND {$wpdb->posts}.post_type = 'shop_order'
			AND {$wpdb->posts}.post_status NOT IN ( 'wc-cancelled', 'wc-refunded', 'wc-failed','trash' )
		WHERE meta_key = 'Student' AND meta_value IS NOT NULL
                ORDER BY {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id DESC;
	" ) );

        if ( !empty( $order_ids ) ) {
            $order_idArray = array();
            foreach ($order_ids as $order_key => $order_value) {
                $order_id = $order_value->order_id;
                $order = wc_get_order( $order_id );
                if( $order->get_meta( 'financial_aid' ) == 'yes' && $order->get_meta( 'school_district' ) == $filter_district ) {
                    $order_idArray[] = $order_id;
                }
            }
            return $order_idArray;
	}

	return false;
}

function get_district_remaining_financial_amount( $district_id ){
    if( !empty( $district_id ) ) {
        $order_district_term = get_term( $district_id, 'school_district' );
        if ( $order_district_term ) {
            $fd_amount              = get_term_meta( $order_district_term->term_id, 'financial_aid_amount', true );
            $fd_approve_amount      = get_term_meta( $order_district_term->term_id, 'financial_aid_approve_amount', true );
            $fd_amount              = (int)$fd_amount;
            $fd_approve_amount      = ((int)$fd_approve_amount);
            
            $fd_remaining_amount    = ( $fd_amount ) - ( $fd_approve_amount );
            
            return $fd_remaining_amount;
        }
    }
    return false;
}
function get_district_financial_amount( $district_id ){
    if( !empty( $district_id ) ) {
        $order_district_term = get_term( $district_id, 'school_district' );
        if ( $order_district_term ) {
            $fd_amount              = get_term_meta( $order_district_term->term_id, 'financial_aid_amount', true );
            $fd_amount = (int)$fd_amount;
            return $fd_amount;
        }
    }
    return false;
}