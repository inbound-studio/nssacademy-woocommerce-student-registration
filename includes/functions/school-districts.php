<?php
/**
 * School district functions
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration\Functions;

/**
 * Returns a school district name
 *
 * @param integer $school_district
 * @return string
 */
function get_school_district_name( $school_district ) {
    if ( ! is_a( $school_district, 'WP_Term' ) ) {
        $school_district = get_term( $school_district, 'school_district' );
    }

    if ( ! $school_district || is_wp_error( $school_district ) ) {
        return '';
    }

    return $school_district->name;
}

/**
 * Returns the out of district term ID
 *
 * @return mixed
 */
function get_out_of_district_id() {
    $out_of_district = get_term_by( 'slug', 'out-of-district', 'school_district' );

    if ( $out_of_district && ! is_wp_error( $out_of_district ) ) {
        return $out_of_district->term_id;
    }

    return false;
}