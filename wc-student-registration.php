<?php
/**
 * Plugin Name: WooCommerce Student Registration
 * Description: Adds support for student registration against products.
 * Version: 1.0.2
 * Author: David Jensen
 * Author URI: https://dkjensen.com
 * Text Domain: wc-student-registration
 *
 * @package WooCommerce Student Registration
 */

use WC_Student_Registration\WC_Student_Registration;

define( 'WC_STUDENT_REGISTRATION_VER', '1.0.2' );
define( 'WC_STUDENT_REGISTRATION_PLUGIN_NAME', 'WooCommerce Student Registration' );
define( 'WC_STUDENT_REGISTRATION_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'WC_STUDENT_REGISTRATION_PLUGIN_URL', plugin_dir_url( __FILE__ ) );


// Load Composer
require WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'vendor/autoload.php';
require WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/class-wc-student-registration.php';

add_action( 'before_woocommerce_init', function() {
    WC_Student_Registration::instance();
} );

register_activation_hook( __FILE__, function() {
    wp_clear_scheduled_hook( 'woocommerce_cancel_unpaid_orders' );
} );