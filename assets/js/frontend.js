(function($) {

    function conditionsMet() {
        var $this = $(this);
        var matched    = 0;
        var conditions = JSON.parse( $this.attr('data-condition') );

        $.each( conditions, function( key, value ) {
            var $field = $('[name="' + key + '"]');
            var field_value = $field.val();

            if( $field.attr('type') == 'radio' || $field.attr('type') == 'checkbox' ) {
                field_value = $field.filter(':checked').val();
            }

            if( field_value == value ) {
                matched++;
            }
        } );

        if( matched >= Object.keys(conditions).length ) {
            $this.closest('.form-row').show();
        }else {
            $this.closest('.form-row').hide();
        }
    }

    function loopConditionsMet() {
        $(':input[data-condition]').each( conditionsMet );
    }

    $(document).on('ready', function() {

        $(':input').on('change', loopConditionsMet ).trigger('change');

    } );

    $( '.cart' ).on( 'submit', function( e ) {
        console.log('in');
        var $pass_field = $( this ).find( '[name="course_registration_password"]:first' );
        var currentTime = Math.round( ( new Date() ).getTime() / 1000 );
        var passwordOK  = false;
        var errorMsg    = '';
        var pro_id =  $(this).find('#pro_id').val();
        console.log(pro_id);
        if( pro_id == '5397' || pro_id == '5423' || pro_id == '6236' ||  pro_id == '6235' ||  pro_id == '6234' ) {
            checkwcsrPreRegPasswordTime = wcsrExpPreRegPasswordTime;
            checkwcsrRegPasswordTime = wcsrExpRegPasswordTime;
        } else {
            checkwcsrPreRegPasswordTime = wcsrPreRegPasswordTime;
            checkwcsrRegPasswordTime = wcsrRegPasswordTime;
        }
        console.log(checkwcsrPreRegPasswordTime);
        console.log(checkwcsrRegPasswordTime);
        var date = new Date( checkwcsrRegPasswordTime * 1000 );
		var h =  date.getHours(), m = date.getMinutes();
		m = m < 10 ? '0'+m : m;
		var _time = (h > 12) ? (h-12 + ':' + m +' PM') : (h + ':' + m +' AM');
        if ( $pass_field.length ) {
            if ( wcsrPreRegPassword.length && $pass_field.val() == wcsrPreRegPassword ) {
                if ( currentTime < checkwcsrPreRegPasswordTime ) {
                    var date = new Date( checkwcsrPreRegPasswordTime * 1000 );
                    var h =  date.getHours(), m = date.getMinutes();
                    m = m < 10 ? '0'+m : m;
                    var _time = (h > 12) ? (h-12 + ':' + m +' PM') : (h + ':' + m +' AM');
                    console.log( _time );
                    errorMsg = 'This password is not yet active until ' + date.toDateString() + ' at ' + ' ' +_time;
                } else {
                    passwordOK = true;
                }
            }

            if ( ! passwordOK ) {
                if ( wcsrRegPassword.length && $pass_field.val() == wcsrRegPassword ) {
                    if ( currentTime < checkwcsrRegPasswordTime ) {
                        var date = new Date( checkwcsrRegPasswordTime * 1000 );
                        var h =  date.getHours(), m = date.getMinutes();
                        m = m < 10 ? '0'+m : m;
                        var _time = (h > 12) ? (h-12 + ':' + m +' PM') : (h + ':' + m +' AM');
                        console.log( _time );
                        errorMsg = 'This password is not yet active until ' + date.toDateString() + ' at '+ ' ' +_time;
                    } else {
                        passwordOK = true; 
                    }
                }
            }

            if ( passwordOK ) {
                var cookieDate = new Date;
                cookieDate.setFullYear( cookieDate.getFullYear() + 1 );

                document.cookie ='wcsr_course_registration_password=yes; expires=' + cookieDate.toGMTString() + '; path=/';

                return true;
            } else if ( ! errorMsg.length ) {
                errorMsg = 'Invalid password.';
            }

            if ( errorMsg.length ) {
                e.preventDefault();

                $( this ).siblings( '.woocommerce-error' ).remove();

                $( this ).before( '<p class="woocommerce-error">' + errorMsg + '</p>' );
            }
        }
    } );

    $( '[data-person]' ).on( 'click', function( e ) {
        e.preventDefault();

        var person = $( this ).data( 'person' );
        var type   = $( this ).data( 'person-type' ) || 'student';

        $.post( {
            url : wcsr.ajaxurl,
            data : {
                action : 'wcsr_get_person',
                person : person,
                type   : type,
                nonce  : wcsr.nonce
            },
            success : function( response ) {
                $.featherlight( $( response ) );
            }
        } );
    } );

    $(document).on( 'click', '.edit-info' ,function( e ) {
        e.preventDefault();
        var stu_id = $( this ).data( 'id' );
        var order_id = $( this ).data( 'order-id' );
        var pro_id = $( this ).data( 'pro-id' );
        var parent_id = $( this ).data( 'parent-id' );
        var btn = $( this );
        $.post( {
            url : wcsr.ajaxurl,
            data : {
                action : 'wcsr_edit_student',
                stu_id : stu_id,
                order_id : order_id,
                pro_id : pro_id,
                parent_id : parent_id,
                nonce  : wcsr.nonce
            },
            beforeSend : function ( xhr ) {
                btn.next('.loader').show();
                $("#director-student-reg-list").css('pointer-events','none');
            },
            success : function( response ) {
                btn.next('.loader').hide();
                $.featherlight( $( response ), {
                        closeOnClick:false,
                        closeIcon : ''
                });
            } 
        } );
    } );
    $(document).on( 'click','.director-edit-stu-form #cancel_btn', function( ) {
        $.featherlight.close();
        $("#director-student-reg-list").css('pointer-events','initial');
    });
    $(document).on( 'click','.director-edit-stu-form #submit_btn', function( ) {
        var btn = $( this );
        var table = $( '#person-info-table' );
        jQuery('.error-msg-box').html('');
        jQuery('.success-msg-box').html('');
        var fname           = $('.director-edit-stu-form #fname').val();
        var lname           = $('.director-edit-stu-form #lname').val();
        var teachers        = $('.director-edit-stu-form #teachers').val();
        var grade           = $('.director-edit-stu-form #grade').val();
        var districts       = $('.director-edit-stu-form #districts').val();
        var school          = $('.director-edit-stu-form #school').val();
        var parent          = $('.director-edit-stu-form #parent').val();
        var phone           = $('.director-edit-stu-form #phone').val();
        var email           = $('.director-edit-stu-form #email').val();
        var transportation  = $('.director-edit-stu-form input[name="transportation"]:checked').val();
        var stu_id          = $('.director-edit-stu-form #stu_id').val();
        var pro_id          = $('.director-edit-stu-form #product_id').val();
        var order_id        = $('.director-edit-stu-form #order_id').val();
        
        $.post( {
            url : wcsr.ajaxurl,
            data : {
                action : 'wcsr_update_student',
                fname : fname,
                lname : lname,
                teachers : teachers,
                grade : grade,
                districts : districts,
                school : school,
                parent : parent,
                phone : phone,
                email : email,
                transportation : transportation,
                stu_id : stu_id,
                pro_id : pro_id,
                order_id : order_id,
                nonce  : wcsr.nonce
            },
            beforeSend : function ( xhr ) {
                    table.find('.loader').show();
                    $('.director-edit-stu-form #cancel_btn').css('pointer-events','none');
            },
            success : function( response ) {
                table.find('.loader').hide();
                $('.director-edit-stu-form #cancel_btn').css('pointer-events','initial');
                var obj = jQuery.parseJSON( response );
                if(obj.error == 1){
                    jQuery('.error-msg-box').html( obj.notice );
                } else {
                    location.reload();
                    jQuery('.success-msg-box').html( obj.notice );
                }
                console.log(document.URL);
                $('#director-student-reg-list').load(document.URL +  ' #director-student-reg-list');
            }
        } );
    } );

    $( document ).on( 'gform_confirmation_loaded gform_post_render', function( event, formId ) {
        var current = $.featherlight.current();

        if ( typeof current !== 'undefined' && current ) {
            current.close();

            setTimeout( function() {
                current.open();
            }, 500 );
            
        }
    } );

})(jQuery);
