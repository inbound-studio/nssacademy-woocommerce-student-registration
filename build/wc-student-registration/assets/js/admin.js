( function( $ ) {

    $( '.options_group.pricing, .form-field._manage_stock_field' ).addClass( 'show_if_class' );

    $( '.save-student' ).on( 'click', function(e) {
        e.preventDefault();

        var $this = $( this );
        var $student_field = $this.siblings( '.student-search' );

        $this.prop( 'disabled', true );
        $this.siblings( '.spinner' ).addClass( 'is-active' );

        $.ajax( {
            url: wcsr.ajaxurl,
            dataType: 'json',
            data: {
                student: $student_field.val(),
                item_id: $student_field.data( 'item-id' ),
                action:   'wcsr_save_student',
                _wpnonce: wcsr.nonce,
            },
            complete: function() {
                $this.siblings( '.spinner' ).removeClass( 'is-active' );
            },
            error: function() {
                $this.prop( 'disabled', false );
            },
            success: function() {
                $this.html( 'Reloading...' );
                location.reload();
            },
            cache: false
        } );
    } );

    // Ajax customer search boxes
    $( ':input.student-search' ).filter( ':not(.enhanced)' ).each( function() {
        var select2_args = {
            //allowClear:  $( this ).data( 'allow_clear' ) ? true : false,
            placeholder: $( this ).data( 'placeholder' ),
            //minimumInputLength: $( this ).data( 'minimum_input_length' ) ? $( this ).data( 'minimum_input_length' ) : '1',
            escapeMarkup: function( m ) {
                return m;
            },
            /*
            ajax: {
                url:         wcsr.ajaxurl,
                dataType:    'json',
                delay:       1000,
                data:        function( params ) {
                    return {
                        term:     params.term,
                        action:   'wcsr_get_students',
                        _wpnonce: wcsr.nonce,
                    };
                },
                processResults: function( data ) {
                    var terms = [];
                    if ( data ) {
                        $.each( data, function( id, text ) {
                            terms.push({
                                id: id,
                                text: text
                            });
                        });
                    }
                    return {
                        results: terms
                    };
                },
                cache: true
            }
            */
        };

        $( this ).selectWoo( select2_args ).addClass( 'enhanced' );

        if ( $( this ).data( 'sortable' ) ) {
            var $select = $(this);
            var $list   = $( this ).next( '.select2-container' ).find( 'ul.select2-selection__rendered' );

            $list.sortable({
                placeholder : 'ui-state-highlight select2-selection__choice',
                forcePlaceholderSize: true,
                items       : 'li:not(.select2-search__field)',
                tolerance   : 'pointer',
                stop: function() {
                    $( $list.find( '.select2-selection__choice' ).get().reverse() ).each( function() {
                        var id     = $( this ).data( 'data' ).id;
                        var option = $select.find( 'option[value="' + id + '"]' )[0];
                        $select.prepend( option );
                    } );
                }
            });
        }
    });

} )( jQuery );