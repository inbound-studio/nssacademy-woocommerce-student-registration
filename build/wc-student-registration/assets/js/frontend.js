(function($) {

    function conditionsMet() {
        var $this = $(this);
        var matched    = 0;
        var conditions = JSON.parse( $this.attr('data-condition') );

        $.each( conditions, function( key, value ) {
            var $field = $('[name="' + key + '"]');
            var field_value = $field.val();

            if( $field.attr('type') == 'radio' || $field.attr('type') == 'checkbox' ) {
                field_value = $field.filter(':checked').val();
            }

            if( field_value == value ) {
                matched++;
            }
        } );

        if( matched >= Object.keys(conditions).length ) {
            $this.closest('.form-row').show();
        }else {
            $this.closest('.form-row').hide();
        }
    }

    function loopConditionsMet() {
        $(':input[data-condition]').each( conditionsMet );
    }

    $(document).on('ready', function() {

        $(':input').on('change', loopConditionsMet ).trigger('change');

    } );

    $( '.cart' ).on( 'submit', function( e ) {
        var $pass_field = $( this ).find( '[name="course_registration_password"]:first' );
        var currentTime = Math.round( ( new Date() ).getTime() / 1000 );
        var passwordOK  = false;
        var errorMsg    = '';

        if ( $pass_field.length ) {
            if ( wcsrPreRegPassword.length && $pass_field.val() == wcsrPreRegPassword ) {
                if ( currentTime < wcsrPreRegPasswordTime ) {
                    var date = new Date( wcsrPreRegPasswordTime * 1000 );

                    errorMsg = 'This password is not yet active until ' + date.toDateString() + ' at ' + date.getHours() + ':' + ( date.getMinutes() < 10 ? 0 : '' ) + date.getMinutes() + 'am';
                } else {
                    passwordOK = true;
                }
            }

            if ( ! passwordOK ) {
                if ( wcsrRegPassword.length && $pass_field.val() == wcsrRegPassword ) {
                    if ( currentTime < wcsrRegPasswordTime ) {
                        var date = new Date( wcsrRegPasswordTime * 1000 );

                        errorMsg = 'This password is not yet active until ' + date.toDateString() + ' at ' + date.getHours() + ':' + ( date.getMinutes() < 10 ? 0 : '' ) + date.getMinutes() + 'am';
                    } else {
                        passwordOK = true;
                    }
                }
            }

            if ( passwordOK ) {
                var cookieDate = new Date;
                cookieDate.setFullYear( cookieDate.getFullYear() + 1 );

                document.cookie ='wcsr_course_registration_password=yes; expires=' + cookieDate.toGMTString() + '; path=/';

                return true;
            } else if ( ! errorMsg.length ) {
                errorMsg = 'Invalid password.';
            }

            if ( errorMsg.length ) {
                e.preventDefault();

                $( this ).siblings( '.woocommerce-error' ).remove();

                $( this ).before( '<p class="woocommerce-error">' + errorMsg + '</p>' );
            }
        }
    } );

    $( '[data-person]' ).on( 'click', function( e ) {
        e.preventDefault();

        var person = $( this ).data( 'person' );
        var type   = $( this ).data( 'person-type' ) || 'student';

        $.post( {
            url : wcsr.ajaxurl,
            data : {
                action : 'wcsr_get_person',
                person : person,
                type   : type,
                nonce  : wcsr.nonce
            },
            success : function( response ) {
                $.featherlight( $( response ) );
            }
        } );
    } );

    $( document ).on( 'gform_confirmation_loaded gform_post_render', function( event, formId ) {
        var current = $.featherlight.current();

        if ( typeof current !== 'undefined' && current ) {
            current.close();

            setTimeout( function() {
                current.open();
            }, 500 );
            
        }
    } );

})(jQuery);
