<?php
/**
 * Main WC_Student_Registration class file
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration;

class WC_Student_Registration {

    /**
	 * Plugin object
	 */
    private static $instance;

    /**
     * Instance of checkout
     *
     * @var WC_Student_Registration_Checkout
     */
    public $checkout;

    /**
     * Insures that only one instance of WC_Student_Registration exists in memory at any one time.
     *
     * @return WC_Student_Registration The one true instance of WC_Student_Registration
     */
    public static function instance() {
        if ( ! class_exists( 'WooCommerce' ) ) {
            return;
        }

        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof WC_Student_Registration ) ) {
            self::$instance = new WC_Student_Registration;
			self::$instance->includes();
			self::$instance->setup();

            do_action_ref_array( 'wc_student_registration_loaded', self::$instance );
        }

        return self::$instance;
    }

    /**
     * Setup hooks
     *
     * @return void
     */
    public function setup() {
        add_filter( 'woocommerce_integrations', [ $this, 'integrations' ] );
        add_filter( 'woocommerce_email_classes', [ $this, 'emails' ] );
        add_filter( 'woocommerce_payment_gateways', [ $this, 'gateways' ] );
    }

    /**
     * Register new WC integration
     *
     * @param array $integrations
     * @return array
     */
    public function integrations( $integrations ) {
        if ( class_exists( '\WC_Integration' ) ) {
            require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/settings/class-wc-student-registration-settings.php';
            
            $integrations[] = 'WC_Student_Registration\\WC_Student_Registration_Settings';  
        }
        
        return $integrations;
    }

    /**
     * Register new WC emails
     *
     * @param array $emails
     * @return array
     */
    public function emails( $emails ) {
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/emails/class-wc-email-district-rep-financial-aid.php';
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/emails/class-wc-email-district-rep-financial-aid-reviewed.php';
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/emails/class-wc-email-customer-financial-aid-approval.php';
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/emails/class-wc-email-customer-financial-aid-denial.php';
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/emails/class-wc-email-customer-financial-aid-review.php';
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/emails/class-wc-email-director-financial-aid.php';

        $emails['WC_Email_District_Rep_Financial_Aid'] = new \WC_Student_Registration\Emails\WC_Email_District_Rep_Financial_Aid;
        $emails['WC_Email_District_Rep_Financial_Aid_Reviewed'] = new \WC_Student_Registration\Emails\WC_Email_District_Rep_Financial_Aid_Reviewed;
        $emails['WC_Email_Customer_Financial_Aid_Approval'] = new \WC_Student_Registration\Emails\WC_Email_Customer_Financial_Aid_Approval;
        $emails['WC_Email_Customer_Financial_Aid_Denial'] = new \WC_Student_Registration\Emails\WC_Email_Customer_Financial_Aid_Denial;
        $emails['WC_Email_Customer_Financial_Aid_Review'] = new \WC_Student_Registration\Emails\WC_Email_Customer_Financial_Aid_Review;
        $emails['WC_Email_Director_Financial_Aid'] = new \WC_Student_Registration\Emails\WC_Email_Director_Financial_Aid;
    
        return $emails;
    }

    /**
     * Register new WC payment gateway
     *
     * @param array $methods
     * @return array
     */
    public function gateways( $methods ) {
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/gateways/class-wc-gateway-financial-aid.php';

        $methods[] = 'WC_Student_Registration\\Gateways\\WC_Gateway_Financial_Aid';

        return $methods;
    }


    /**
     * Include the goodies
     *
     * @return void
     */
    public function includes() {
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/woocommerce-hooks.php';
        
        // Functions
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/functions/helpers.php';
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/functions/students.php';
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/functions/district-reps.php';
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/functions/orders.php';
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/functions/school-districts.php';

        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/class-wc-student-registration-scripts.php';
		require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/class-wc-product-class.php';
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/class-wc-student.php';

        // Shortcodes
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/shortcodes/student-registration-list.php';
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/shortcodes/student-registration-orders.php';

        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/class-wc-student-registration-ajax.php';

        if ( ! is_admin() ) {
            require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/class-wc-student-registration-checkout.php';

            self::$instance->checkout = new WC_Student_Registration_Checkout;
            require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/class-wc-student-registration-password.php';
        }

        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/class-wc-student-registration-form-handler.php';
        require_once WC_STUDENT_REGISTRATION_PLUGIN_DIR . 'includes/class-wc-parent-guardian.php';
        
    }


    /**
     * Throw error on object clone
     *
     * @return void
     */
    public function __clone() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wc-student-registration' ), '1.0.0' );
    }


    /**
     * Disable unserializing of the class
     *
     * @return void
     */
    public function __wakeup() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wc-student-registration' ), '1.0.0' );
    }

}
