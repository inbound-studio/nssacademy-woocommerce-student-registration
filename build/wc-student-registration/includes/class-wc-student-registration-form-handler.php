<?php
/**
 * WC_Student_Registration_Form_Handler class file
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration;

use function WC_Student_Registration\Functions\get_district_rep_district;
use function WC_Student_Registration\Functions\get_students_in_school_district;
use function WC_Student_Registration\Functions\get_all_students;
use function WC_Student_Registration\Functions\get_financial_aid_orders_page_id;
use function WC_Student_Registration\Functions\get_financial_aid_discount_coupon;
use function WC_Student_Registration\Functions\get_order_pending_financial_aid_amount;
use function WC_Student_Registration\Functions\update_order_financial_aid_discount;
use function WC_Student_Registration\Functions\get_school_district_name;
use function WC_Student_Registration\Functions\get_student_registration_line_item;
use function WC_Student_Registration\Functions\get_parent_guardian;
use function WC_Student_Registration\Functions\get_student_order;
use function WC_Student_Registration\Functions\get_student_registered_class;

class WC_Student_Registration_Form_Handler {

    public function __construct() {
        add_action( 'wp', [ $this, 'submit_financial_aid' ] );
        add_action( 'wp', [ $this, 'submit_financial_aid_review' ] );
        add_action( 'wp', [ $this, 'export_student_registration_list' ] );

        add_action( 'admin_init', [ $this, 'export_student_registrations' ] );
    }

    /**
     * District rep submit financial aid form
     *
     * @return void
     */
    function submit_financial_aid() {
        global $wpdb;

        if ( is_admin() || empty( $_POST['wcsr_action'] ) || $_POST['wcsr_action'] !== 'submit_financial_aid' ) {
            return;
        }

        if ( ! wp_verify_nonce( $_POST['_wpnonce'] ) ) {
            wp_die( __( 'Invalid nonce, please try again.', 'wc-student-registration' ) );
        }

        if ( ! current_user_can( 'edit_workflow' ) ) {
            wp_die( __( 'You do not have permission to view this content.', 'wc-student-registration' ) );
        }
        
        if ( ! isset( $_POST['financial_aid_amount'] ) || ! is_numeric( $_POST['financial_aid_amount'] ) ) {
            wp_die( __( 'Invalid financial aid amount.', 'wc-student-registration' ) );
        }

        if ( ! isset( $_POST['order_id'] ) ) {
            wp_die( __( 'Invalid order ID.', 'wc-student-registration' ) );
        }

        
        $amount = $_POST['financial_aid_amount'] ?? 0;
        $order  = wc_get_order( $_POST['order_id'] );

        if ( ! $order ) {
            wp_die( __( 'Order not found', 'wc-student-registration' ) );
        }

        $order->update_meta_data( 'financial_aid_district_rep', get_current_user_id() );

        $emails = new \WC_Emails;

        if ( $amount > 0 ) {
            $order->update_meta_data( 'pending_financial_aid_amount', $amount );
            $order->update_meta_data( 'pending_financial_aid_needs_review', 1 );
            $emails->emails['WC_Email_Director_Financial_Aid']->trigger( $order->get_id(), $order );
        } else {
            $order->update_meta_data( 'financial_aid_amount', $amount );
            $order->update_status( 'pending', sprintf( __( 'Financial aid amount: %s', 'wc-student-registration' ), wc_price( $amount ) ) );
            $emails->emails['WC_Email_Customer_Financial_Aid_Denial']->trigger( $order->get_id(), $order );
        }

        $order->save();

        wp_redirect( get_permalink( get_financial_aid_orders_page_id() ) );
        exit;
    }

    /**
     * Director submit financial aid review
     *
     * @return void
     */
    function submit_financial_aid_review() {
        if ( is_admin() || empty( $_POST['wcsr_action'] ) || $_POST['wcsr_action'] !== 'submit_financial_aid_review' ) {
            return;
        }

        if ( ! wp_verify_nonce( $_POST['_wpnonce'] ) ) {
            wp_die( __( 'Invalid nonce, please try again.', 'wc-student-registration' ) );
        }

        if ( ! current_user_can( 'review_financial_aid' ) ) {
            wp_die( __( 'You do not have permission to view this content.', 'wc-student-registration' ) );
        }
        
        if ( ! isset( $_POST['financial_aid_review'] ) || ! in_array( $_POST['financial_aid_review'], [ 'approved', 'denied' ] ) ) {
            wp_die( __( 'Invalid financial aid response.', 'wc-student-registration' ) );
        }

        if ( ! isset( $_POST['order_id'] ) ) {
            wp_die( __( 'Invalid order ID.', 'wc-student-registration' ) );
        }

        $status = $_POST['financial_aid_review'];
        $order  = wc_get_order( $_POST['order_id'] );
        $notes  = isset( $_POST['financial_aid_notes'] ) ? $_POST['financial_aid_notes'] : '';

        if ( ! $order ) {
            wp_die( __( 'Order not found', 'wc-student-registration' ) );
        }

        $amount = get_order_pending_financial_aid_amount( $order );

        $order->update_meta_data( 'financial_aid_notes', $notes );
        $order->update_meta_data( 'pending_financial_aid_needs_review', 0 );
        $order->save();

        $emails = new \WC_Emails;

        if ( $status === 'approved' ) {
            $order->update_meta_data( 'financial_aid_amount', $amount );
            $order->update_status( 'pending', sprintf( __( 'Financial aid amount: %s', 'wc-student-registration' ), wc_price( $amount ) ) );
            update_order_financial_aid_discount( $order, $amount );
            $order->save();

            $emails->emails['WC_Email_Customer_Financial_Aid_Approval']->trigger( $order->get_id(), $order );
        } else {
            $emails->emails['WC_Email_District_Rep_Financial_Aid_Reviewed']->trigger( $order->get_id(), $order );
        }

        wp_redirect( get_permalink( get_financial_aid_orders_page_id() ) );
        exit;
    }

    /**
     * Export class registration list
     *
     * @return void
     */
    function export_student_registration_list() {
        global $wpdb;

        if ( is_admin() || empty( $_GET['export-wcsr-reg-list'] ) ) {
            return;
        }

        if ( ! wp_verify_nonce( $_GET['_wpnonce'] ) ) {
            wp_die( __( 'Invalid nonce, please try again.', 'wc-student-registration' ) );
        }

        if ( ! current_user_can( 'edit_workflow' ) ) {
            wp_die( __( 'You do not have permission to view this content.', 'wc-student-registration' ) );
        }

        header( 'Content-Description: File Transfer' );
        header( 'Content-Type: application/csv' );
        header( 'Content-Disposition: attachment; filename=student-registration-list.csv' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );

        $handle = fopen( 'php://output', 'w' );

        // Return single or array of districts
        $district = ! current_user_can( 'manage_options' ) ? get_district_rep_district( get_current_user_id() ) : get_terms( [ 'taxonomy' => 'school_district', 'hide_empty' => false, 'fields' => 'ids' ] );

        $students = get_students_in_school_district( $district );

        $keys = [
            'student_first_name',
            'student_last_name',
            'registered_class',
            'student_gender',
            'student_dob',
            'student_ethnicity',
            'student_shirt_size',
            'student_school_name',
            'student_school_district',
            'student_grade',
            'student_health_concerns_conditional',
            'student_health_concerns',
            'student_medications_conditional',
            'student_reason_medication',
            'student_medications',
            'student_diabetes_conditional',
            'student_method_transport',
            'student_strengths',
            'student_skills_to_improve',
            'student_additional_comments',
            'student_emergency_contacts',
            'authorize_medicate_student',
            'consent_media_release_student'
        ];

        ob_clean();

        fputcsv( $handle, $keys );

        foreach ( $students as $student ) {
            $meta  = [];
            $_meta = get_post_meta( $student->ID );

            foreach ( $keys as $key ) {
                $set = false;

                foreach ( $_meta as $_key => $_value ) {
                    if ( $key === $_key ) {

                        switch ( $key ) {
                            case 'registered_class' :
                                $meta[] = get_the_title( current( $_value ) );
                                break;

                            default :
                                $meta[] = current( $_value );
                        }
                             
                        $set = true;
                    }
                }

                if ( ! $set ) {
                    $meta[] = '-';
                }
            }

            fputcsv( $handle, $meta );
        }

        ob_flush();
        fclose( $handle );
        exit;
    }


    /**
     * Export student class registrations
     *
     * @return void
     */
    function export_student_registrations() {
        global $wpdb;

        if ( ! is_admin() || empty( $_GET['export-wcsr-students-registratios'] ) ) {
            return;
        }

        if ( ! wp_verify_nonce( $_GET['_wpnonce'] ) ) {
            wp_die( __( 'Invalid nonce, please try again.', 'wc-student-registration' ) );
        }

        if ( ! current_user_can( 'manage_options' ) ) {
            wp_die( __( 'You do not have permission to view this content.', 'wc-student-registration' ) );
        }

        header( 'Content-Description: File Transfer' );
        header( 'Content-Type: application/csv' );
        header( 'Content-Disposition: attachment; filename=students-class-registrations.csv' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );

        $handle = fopen( 'php://output', 'w' );

        // Return single or array of districts
        $district = get_terms( [ 'taxonomy' => 'school_district', 'hide_empty' => false, 'fields' => 'ids' ] );

        $students = get_all_students();

        $keys = [
            'student_first_name',
            'student_last_name',
            'registered_class',
            'student_school_district',
            'student_parents_1_name',
            'student_parents_1_phone',
            'student_parents_1_email',
            'student_parents_2_name',
            'student_parents_2_phone',
            'student_parents_2_email',
            'related_order',
            'order_status',
        ];

        ob_clean();

        fputcsv( $handle, $keys );

        foreach ( $students as $student ) {
            $meta  = [];
            $_meta = wp_parse_args( get_post_meta( $student->ID ),
                array_map( function( $value ) {
                    return [ '' ];
                }, array_flip( $keys ) )
            );

            $registration_line = get_student_registration_line_item( $student->ID );

            if ( ! $registration_line ) {
                continue;
            }

            foreach ( $_meta as $_key => $_value ) {
                if ( in_array( $_key, $keys ) ) {
                    $value = '';

                    switch ( $_key ) {
                        case 'registered_class' :
                            $class_name = get_the_title( current( $_value ) );

                            if ( ! $class_name ) {
                                $class = get_student_registered_class( $student->ID );
                                $class_name = $class ? $class->get_name() : '';
                            }
                            
                            $value = $class_name ? $class_name : '-';
                            break;

                        case 'student_school_district' :
                            $value = get_school_district_name( current( $_value ) );
                            break;

                        case 'student_parents_1_name' :
                            $parent_guardian_1 = get_parent_guardian( $registration_line->get_meta( 'Parent/Guardian 1' ) );
                            $value = $parent_guardian_1->get_full_name();
                            break;

                        case 'student_parents_1_phone' :
                            $parent_guardian_1 = get_parent_guardian( $registration_line->get_meta( 'Parent/Guardian 1' ) );
                            $value = $parent_guardian_1->get_phone();
                            break;

                        case 'student_parents_1_email' :
                            $parent_guardian_1 = get_parent_guardian( $registration_line->get_meta( 'Parent/Guardian 1' ) );
                            $value = $parent_guardian_1->get_email();
                            break;

                        case 'student_parents_2_name' :
                            $parent_guardian_2 = get_parent_guardian( $registration_line->get_meta( 'Parent/Guardian 2' ) );
                            $value = $parent_guardian_2->get_full_name();
                            break;

                        case 'student_parents_2_phone' :
                            $parent_guardian_2 = get_parent_guardian( $registration_line->get_meta( 'Parent/Guardian 2' ) );
                            $value = $parent_guardian_2->get_phone();
                            break;

                        case 'student_parents_2_email' :
                            $parent_guardian_2 = get_parent_guardian( $registration_line->get_meta( 'Parent/Guardian 2' ) );
                            $value = $parent_guardian_2->get_email();
                            break;

                        case 'related_order' :
                            $order = get_student_order( $student->ID );
                            $value = $order ? $order->get_id() : '';
                            break;

                        case 'order_status' :
                            $order = get_student_order( $student->ID );
                            $value = $order ? $order->get_status() : '';
                            break;

                        default :
                            $value = current( $_value );
                    }

                    if ( ! $value ) {
                        $value = '-';
                    }

                    $meta[] = $value;
                }
            }

            fputcsv( $handle, $meta );
        }

        ob_flush();
        fclose( $handle );
        exit;
    }
}

return new WC_Student_Registration_Form_Handler;