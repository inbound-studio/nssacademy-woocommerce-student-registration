<?php
/**
 * Financial aid email sent to district rep
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration\Emails;

use function WC_Student_Registration\Functions\order_has_class;
use function WC_Student_Registration\Functions\get_order_school_district;
use function WC_Student_Registration\Functions\get_district_reps_by_school_district;
use function WC_Student_Registration\Functions\get_school_district_name;
use function WC_Student_Registration\Functions\get_district_reps_edit_order_link;

/**
 * An email sent to district reps when an order is placed and needs financial aid review
 *
 * @class       WC_Email_District_Rep_Financial_Aid
 * @version     3.5.0
 * @package     WooCommerce/Classes/Emails
 * @extends     WC_Email
 */
class WC_Email_District_Rep_Financial_Aid extends \WC_Email {

    /**
     * Constructor.
     */
    public function __construct() {
        $this->id                   = 'wc_email_district_rep_financial_aid';
        $this->customer_email       = false;
        $this->title                = __( 'Financial Aid', 'wc-student-registration' );
        $this->description          = __( 'Email sent to district reps when an order is placed and needs financial aid review', 'wc-student-registration' );
        $this->template_html        = 'emails/district-rep-financial-aid.php';
		$this->template_plain       = 'emails/plain/district-rep-financial-aid.php';
		$this->placeholders         = [ '{wc-student-registration-edit-financial-aid}' => '' ];

        // Call parent constructor.
        parent::__construct();
    }

    /**
     * Get email subject.
     *
     * @return string
     */
    public function get_default_subject() {
        return __( 'Financial Aid Request', 'wc-student-registration' );
    }

    /**
     * Get email heading.
     *
     * @return string
     */
    public function get_default_heading() {
        return __( 'Financial Aid Request', 'wc-student-registration' );
    }

    /**
     * Get email content
     *
     * @return string
     */
    public function get_default_additional_content() {
        $content  = __( 'A new class registration requires financial aid approval:', 'wc-student-registration' ) . "\n\n";
        $content .= '{wc-student-registration-edit-financial-aid}';

        return $content;
    }

    /**
     * Returns district rep as recipient
     *
     * @return void
     */
    public function get_recipient() {
        if ( is_a( $this->object, 'WC_Order' ) ) {
            $order = $this->object;

            $school_district = get_order_school_district( $order );

            if ( $school_district ) {
                $district_reps = get_district_reps_by_school_district( $school_district );

                if ( $district_reps ) {
                    return wp_list_pluck( $district_reps, 'user_email' );
                }

                $order->add_order_note( sprintf( __( 'Could not find any district reps associated with school district %s. Sending to admin', 'wc-student-registration' ), get_school_district_name( $school_district ) ) );
            
                return get_option( 'admin_email' );
            }
        }

        return false;
    }

    /**
     * Trigger the sending of this email.
     *
     * @param int            $order_id The order ID.
     * @param WC_Order|false $order Order object.
     */
    public function trigger( $order_id, $order = false ) {
        $this->setup_locale();

        if ( $order_id && ! is_a( $order, 'WC_Order' ) ) {
            $order = wc_get_order( $order_id );
        }

        if ( is_a( $order, 'WC_Order' ) ) {
            $this->object                                                        = $order;
            $this->placeholders['{wc-student-registration-edit-financial-aid}']  = get_district_reps_edit_order_link( $order );
        }

        if ( $this->is_enabled() && $this->get_recipient() && order_has_class( $order ) && $order->get_meta( 'financial_aid_amount' ) === '' ) {
            $this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
        }

        $this->restore_locale();
    }

    /**
     * Get content html.
     *
     * @return string
     */
    public function get_content_html() {
        return wc_get_template_html(
            $this->template_html,
            array(
                'order'              => $this->object,
                'email_heading'      => $this->get_heading(),
                'additional_content' => $this->get_additional_content(),
                'sent_to_admin'      => false,
                'plain_text'         => false,
                'email'              => $this,
            )
        );
    }

    /**
     * Get content plain.
     *
     * @return string
     */
    public function get_content_plain() {
        return wc_get_template_html(
            $this->template_plain,
            array(
                'order'              => $this->object,
                'email_heading'      => $this->get_heading(),
                'additional_content' => $this->get_additional_content(),
                'sent_to_admin'      => false,
                'plain_text'         => true,
                'email'              => $this,
            )
        );
    }
}