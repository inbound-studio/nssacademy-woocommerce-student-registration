<?php
/**
 * Financial aid email sent to director for review
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration\Emails;

use function WC_Student_Registration\Functions\get_order_financial_aid_amount;
use function WC_Student_Registration\Functions\get_financial_aid_director_email;
use function WC_Student_Registration\Functions\get_district_reps_edit_order_link;

/**
 * An email sent to the director for review
 *
 * @class       WC_Email_Director_Financial_Aid
 * @version     3.5.0
 * @package     WooCommerce/Classes/Emails
 * @extends     WC_Email
 */
class WC_Email_Director_Financial_Aid extends \WC_Email {

    /**
     * Constructor.
     */
    public function __construct() {
        $this->id                   = 'wc_email_director_financial_aid';
        $this->customer_email       = false;
        $this->title                = __( 'Financial Aid - Director Review', 'wc-student-registration' );
        $this->description          = __( 'Email sent to the director to review financial aid decision.', 'wc-student-registration' );
        $this->template_html        = 'emails/director-financial-aid.php';
		$this->template_plain       = 'emails/plain/director-financial-aid.php';
		$this->placeholders         = [ 
            '{wc-student-registration-edit-financial-aid}' => '',
            '{district_rep_name}' => ''
         ];

        // Call parent constructor.
        parent::__construct();
    }

    /**
     * Get email subject.
     *
     * @return string
     */
    public function get_default_subject() {
        return esc_html__( 'A new financial aid offer was created by {district_rep_name}', 'wc-student-registration' );
    }

    /**
     * Get email heading.
     *
     * @return string
     */
    public function get_default_heading() {
        return esc_html__( 'A new financial aid offer was created by {district_rep_name}', 'wc-student-registration' );
    }

    /**
     * Get email content
     *
     * @return string
     */
    public function get_default_additional_content() {
        $content  = __( 'Please review and approve or reject the financial aid request.', 'wc-student-registration' ) . "\n\n";
        $content .= '{wc-student-registration-edit-financial-aid}';

        return $content;
    }

    /**
     * Trigger the sending of this email.
     *
     * @param int            $order_id The order ID.
     * @param WC_Order|false $order Order object.
     */
    public function trigger( $order_id, $order = false ) {
        $this->setup_locale();

        if ( $order_id && ! is_a( $order, 'WC_Order' ) ) {
            $order = wc_get_order( $order_id );
        }

        if ( is_a( $order, 'WC_Order' ) ) {
            $this->object                                                       = $order;
            $this->recipient                                                    = get_financial_aid_director_email();
            $this->placeholders['{wc-student-registration-edit-financial-aid}'] = get_district_reps_edit_order_link( $order );

            $district_rep = get_userdata( $this->object->get_meta( 'financial_aid_district_rep' ) );

            if ( $district_rep ) {
                $this->placeholders['{district_rep_name}'] = $district_rep->display_name;
            }
        }

        if ( $this->is_enabled() && $this->get_recipient() && $order->get_meta( 'financial_aid' ) === 'yes' ) {
            $this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
        }

        $this->restore_locale();
    }

    /**
     * Get content html.
     *
     * @return string
     */
    public function get_content_html() {
        return wc_get_template_html(
            $this->template_html,
            array(
                'order'              => $this->object,
                'email_heading'      => $this->get_heading(),
                'additional_content' => $this->get_additional_content(),
                'sent_to_admin'      => false,
                'plain_text'         => false,
                'email'              => $this,
            )
        );
    }

    /**
     * Get content plain.
     *
     * @return string
     */
    public function get_content_plain() {
        return wc_get_template_html(
            $this->template_plain,
            array(
                'order'              => $this->object,
                'email_heading'      => $this->get_heading(),
                'additional_content' => $this->get_additional_content(),
                'sent_to_admin'      => false,
                'plain_text'         => true,
                'email'              => $this,
            )
        );
    }
}