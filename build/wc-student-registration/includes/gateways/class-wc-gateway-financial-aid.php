<?php
/**
 * Financial aid payment gateway
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration\Gateways;

if ( class_exists( '\WC_Payment_Gateway' ) ) {

class WC_Gateway_Financial_Aid extends \WC_Payment_Gateway {

	/**
	 * Constructor for the gateway.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		$this->id                 = 'financial_aid_pay_later';
		$this->has_fields         = false;
		$this->method_title       = esc_html__( 'Financial Aid - Pay Later', 'wc-student-registration' );
		$this->method_description = esc_html__( 'Allow customers to make a payment later if seeking approval for financial aid.', 'wc-student-registration' );

		$this->init_form_fields();
		$this->init_settings();

		$this->title              = $this->settings['title'];
		$this->description        = $this->settings['description'];
		$this->enabled            = $this->settings['enabled'];

		add_filter( 'woocommerce_default_order_status', [ $this, 'default_order_status' ] );
		add_action( 'woocommerce_order_status_pending-fa', [ $this, 'send_pending_order_emails' ], 10, 2 );
	}

	/**
	 * Initialise Gateway Settings Form Fields
	 *
	 * @return array
	 */
	public function init_form_fields() {
		$form_fields = array(
			'enabled'     => array(
				'title'       => sprintf( '<b>%s</b>', esc_html__( 'Enable/Disable', 'wc-student-registration' ) ),
				'type'        => 'checkbox',
				'label'       => esc_html__( 'Enable financial aid', 'wc-student-registration' ),
				'default'     => 'no',
			),
			'title'           => array(
				'title'       => __( 'Title', 'wc-student-registration' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'wc-student-registration' ),
				'default'     => __( 'Financial Aid - Pay Later', 'wc-student-registration' ),
				'desc_tip'    => true,
			),
			'description'     => array(
				'title'       => __( 'Description', 'wc-student-registration' ),
				'type'        => 'textarea',
				'description' => __( 'Payment method description that the customer will see on your checkout.', 'wc-student-registration' ),
				'default'     => __( 'Submit your order without payment now. A district rep will review your order and apply financial aid if qualified.', 'wc-student-registration' ),
				'desc_tip'    => true,
			),
			'instructions'    => array(
				'title'       => __( 'Instructions', 'wc-student-registration' ),
				'type'        => 'textarea',
				'description' => __( 'Instructions that will be added to the thank you page and emails.', 'wc-student-registration' ),
				'default'     => '',
				'desc_tip'    => true,
			),
		);


		$this->form_fields = $form_fields;
	}

	/**
	 * Process the payment, set the Order to pending and return the result.
	 *
	 * @param int $order_id Order ID.
	 * @return array
	 */
	public function process_payment( $order_id ) {
		$order = wc_get_order( $order_id );

		if ( $order->get_total() > 0 ) {
			$order->update_status( 'pending-fa' );
		} else {
			$order->payment_complete();
		}

		// Reduce stock levels.
		wc_reduce_stock_levels( $order_id );

		// Remove cart.
		WC()->cart->empty_cart();

		// Return thankyou redirect.
		return [
			'result'   => 'success',
			'redirect' => $this->get_return_url( $order ),
		];
	}

	/**
	 * Change the default order status
	 *
	 * @param string $status
	 * @return string
	 */
	public function default_order_status( $status ) {
		if ( ! is_admin() && isset( WC()->session ) && WC()->session->get( 'chosen_payment_method' ) === $this->id ) {
			$default = 'on-hold';
		}

		return $status;
	}

	/**
	 * Trigger pending order emails
	 *
	 * @param int      $order_id
	 * @param WC_Order $order
	 * @return void
	 */
	public function send_pending_order_emails( $order_id, $order ) {
		if ( $order->get_payment_method() !== $this->id ) {
			return;
		}

		$emails = new \WC_Emails;
		$emails->emails['WC_Email_District_Rep_Financial_Aid']->trigger( $order_id, $order );
		$emails->emails['WC_Email_New_Order']->trigger( $order_id, $order );
		$emails->emails['WC_Email_Customer_Financial_Aid_Review']->trigger( $order_id, $order );
	}
}

}