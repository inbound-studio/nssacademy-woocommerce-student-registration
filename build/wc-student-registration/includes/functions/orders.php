<?php
/**
 * Order functions
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration\Functions;

/**
 * Returns whether a given order has a class registration or not
 *
 * @param mixed $order
 * @return boolean
 */
function order_has_class( $order ) {
    if ( ! is_a( $order, 'WC_Order' ) ) {
        $order = wc_get_order( $order );
    }

    if ( ! $order ) {
        return false;
    }

    foreach ( $order->get_items() as $item ) {
        $product = is_callable( [ $item, 'get_product' ] ) ? $item->get_product() : false;

        if ( is_a( $product, 'WC_Student_Registration\\WC_Product_Class' ) ) {
            return true;
        }
    }

    return false;
}

/**
 * Return orders with a given school district association
 *
 * @param integer $school_district
 * @return array
 */
function get_school_district_orders( $school_district = '' ) {
    $args = [
        'post_status'       => [
            'wc-processing', 'wc-completed', 'wc-pending', 'pending', 'wc-partial-payment', 'wc-pending-fa', 'pending-fa'
        ],
        'meta_query'        => [
            'relation'          => 'AND',
            [
                'key'           => 'financial_aid',
                'value'         => 'yes',
                'compare'       => '='
            ]
        ]
    ];

    if ( $school_district ) {
        $args['meta_query'][] = [
            'key'           => 'school_district',
            'value'         => $school_district,
            'compare'       => '='
        ];
    }
    
    $orders = wc_get_orders( $args );

    return $orders;
}

/**
 * Returns a given orders school district
 *
 * @param mixed $order
 * @return integer|boolean
 */
function get_order_school_district( $order ) {
	if ( ! $order instanceof \WC_Order ) {
		$order = wc_get_order( $order );
	}

	if ( $order ) {
        foreach ( $order->get_items() as $item ) {
			$school_district = $item->get_meta( 'school_district' );

			if ( $school_district ) {
				return absint( $school_district );
			}
		}
	}

	return false;
}

/**
 * Returns a given orders students
 *
 * @param mixed $order
 * @return array
 */
function get_order_students( $order ) {
	if ( ! $order instanceof \WC_Order ) {
		$order = wc_get_order( $order );
    }
    
    $students = [];

	if ( $order ) {
        foreach ( $order->get_items() as $item ) {
			$students[] = $item->get_meta( 'Student' );
		}
	}

	return array_filter( $students );
}

/**
 * Return the given financial aid amount for an order
 *
 * @param mixed $order
 * @return mixed
 */
function get_order_financial_aid_amount( $order ) {
    if ( ! $order instanceof \WC_Order ) {
		$order = wc_get_order( $order );
	}

	if ( $order ) {
        return (float) $order->get_meta( 'financial_aid_amount' ) != '' ? $order->get_meta( 'financial_aid_amount' ) : 0;
    }

    return false;
}

/**
 * Return the pending financial aid amount for an order
 *
 * @param mixed $order
 * @return mixed
 */
function get_order_pending_financial_aid_amount( $order ) {
    if ( ! $order instanceof \WC_Order ) {
		$order = wc_get_order( $order );
	}

	if ( $order ) {
        return (float) $order->get_meta( 'pending_financial_aid_amount' ) != '' ? $order->get_meta( 'pending_financial_aid_amount' ) : 0;
    }

    return false;
}

/**
 * Returns whether the given order needs review by a director
 *
 * @param mixed $order
 * @return boolean
 */
function get_order_financial_aid_needs_review( $order ) {
    if ( ! $order instanceof \WC_Order ) {
		$order = wc_get_order( $order );
	}

	if ( $order ) {
        return (bool) $order->get_meta( 'pending_financial_aid_needs_review' ) != '' ? $order->get_meta( 'pending_financial_aid_needs_review' ) : false;
    }

    return false;
}

/**
 * Returns financial aid notes from director
 *
 * @param mixed $order
 * @return string
 */
function get_order_financial_aid_notes( $order ) {
    if ( ! $order instanceof \WC_Order ) {
		$order = wc_get_order( $order );
	}

	if ( $order ) {
        return $order->get_meta( 'financial_aid_notes' );
    }

    return '';
}

/**
 * Applies a coupon for financial aid to an order
 *
 * @param mixed $order
 * @param mixed $amount
 * @return boolean
 */
function update_order_financial_aid_discount( $order, $amount ) {
    if ( ! $order instanceof \WC_Order ) {
		$order = wc_get_order( $order );
	}

	if ( $order ) {
        $coupons = $order->get_coupons();

        foreach ( $coupons as $coupon ) {
            $code = $coupon->get_code();

            if ( substr( $code, 0, 6 ) === 'wcsrfa' ) {
                $order->remove_coupon( $code );
            }
        }

        if ( $amount > 0 ) {
            $coupon_code = get_financial_aid_discount_coupon( $amount );
            $order->apply_coupon( $coupon_code );
            $order->save();

            return true;
        }
    }

    return false;
}