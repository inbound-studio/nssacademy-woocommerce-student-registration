<?php
/**
 * Functions
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration\Functions;

use WC_Student_Registration\WC_Student;
use WC_Student_Registration\WC_Parent_Guardian;

/**
 * Returns an array of WP_Post
 *
 * @param integer $user_id
 * @return array
 */
function get_user_students( $user_id ) {
	global $wpdb;

	$students = $wpdb->get_col( $wpdb->prepare( "
		SELECT 			ID
		FROM   			$wpdb->posts
		WHERE			post_status = 'publish'
		AND				post_type = 'student'
		AND 			post_author = '%d'
	", absint( $user_id ) ) );

	return array_map( __NAMESPACE__ . '\get_student', $students );
}

/**
 * Returns a student object
 *
 * @param integer $id
 * @return WC_Student
 */
function get_student( $id ) {
	return new WC_Student( $id );
}

/**
 * Returns array of students to populate a select dropdown
 *
 * @param integer $user_id
 * @return array
 */
function get_students_options_array( $user_id ) {
	$students = get_user_students( $user_id );

	$options = [];

	foreach ( $students as $student ) {
		$options[ $student->ID ] = $student->get_full_name();
	}

	return $options;
}

/**
 * Returns an array of school districts to populate a select dropdown
 *
 * @return array
 */
function get_school_districts_options_array() {
	return [ '' => '' ] + get_terms( [ 
		'taxonomy' 		=> 'school_district', 
		'hide_empty' 	=> false, 
		'fields' 		=> 'id=>name' 
	] );
}

/**
 * Get a given users parents/guardians
 *
 * @param integer $user_id
 * @return array
 */
function get_user_parents_guardians( $user_id ) {
	global $wpdb;

	$parents_guardians = $wpdb->get_col( $wpdb->prepare( "
		SELECT 			ID
		FROM   			$wpdb->posts
		WHERE			post_status = 'publish'
		AND				post_type = 'parent_guardian'
		AND 			post_author = '%d'
	", absint( $user_id ) ) );

	return array_map( __NAMESPACE__ . '\get_parent_guardian', $parents_guardians );
}


/**
 * Returns a parent/guardian object
 *
 * @param integer $id
 * @return WC_Parent_Guardian
 */
function get_parent_guardian( $id ) {
	return new WC_Parent_Guardian( $id );
}

/**
 * Returns array of parents/guardians to populate a select dropdown
 *
 * @param integer $user_id
 * @return array
 */
function get_parents_guardians_options_array( $user_id ) {
	$parents_guardians = get_user_parents_guardians( $user_id );

	$options = [];

	foreach ( $parents_guardians as $parent_guardian ) {
		$options[ $parent_guardian->ID ] = $parent_guardian->get_full_name();
	}

	return $options;
}

/**
 * Returns all students
 *
 * @return array
 */
function get_all_students() {
	$students = get_posts( [
		'numberposts'		=> -1,
		'post_type'			=> 'student',
		'post_status'		=> 'publish',
		'fields'			=> 'ids'
	] );

	return array_map( __NAMESPACE__ . '\get_student', $students );
}

/**
 * Returns array of students in a given school district
 *
 * @param integer|array $district
 * @return array
 */
function get_students_in_school_district( $district ) {
	$district = (array) $district;

	$students = get_posts( [
		'numberposts'		=> -1,
		'post_type'			=> 'student',
		'post_status'		=> 'publish',
		'fields'			=> 'ids',
		'tax_query'			=> [
			[
				'taxonomy'	=> 'school_district',
				'field'		=> 'term_id',
				'terms'		=> $district
			]
		]
	] );

	return array_map( __NAMESPACE__ . '\get_student', $students );
}

/**
 * Get the order attached to the student
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_order( $student ) {
	global $wpdb;

	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$order_id = $wpdb->get_var( $wpdb->prepare( "
		SELECT order_id
		FROM {$wpdb->prefix}woocommerce_order_itemmeta 
		JOIN {$wpdb->prefix}woocommerce_order_items 
			ON {$wpdb->prefix}woocommerce_order_items.order_item_id = {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id
			AND {$wpdb->prefix}woocommerce_order_itemmeta.meta_key = 'Student'
		JOIN {$wpdb->posts} 
			ON {$wpdb->prefix}woocommerce_order_items.order_id = {$wpdb->posts}.ID
			AND {$wpdb->posts}.post_type = 'shop_order'
			AND {$wpdb->posts}.post_status NOT IN ( 'wc-cancelled', 'wc-refunded', 'wc-failed' )
		WHERE meta_key = 'Student'
		AND   meta_value = '%s'
		ORDER BY {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id DESC
		LIMIT 1;
	", $student ) );

	if ( $order_id ) {
		return wc_get_order( $order_id );
	}

	return false;
}

/**
 * Returns student registration line item
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_registration_line_item( $student ) {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$order = get_student_order( $student );

	if ( $order ) {
		foreach ( $order->get_items() as $item ) {
			$product = $item->get_product();

			if ( $student == $item->get_meta( 'Student' ) ) {
				return $item;
			}
		}
	}

	return false;
}

/**
 * Returns student registered class
 *
 * @param mixed $student
 * @return mixed
 */
function get_student_registered_class( $student ) {
	if ( is_a( $student, 'WC_Student_Registration\\WC_Student' ) ) {
		$student = $student->ID;
	}

	$order_line_item = get_student_registration_line_item( $student );

	if ( $order_line_item ) {
		$product = $order_line_item->get_product();

		return $product;
	}

	return false;
}