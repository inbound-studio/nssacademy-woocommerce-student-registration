<?php
/**
 * District rep functions
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration\Functions;

/**
 * Returns the ID of the district reps school district
 *
 * @param integer $user_id
 * @return integer
 */
function get_district_rep_district( $user_id ) {
	if ( ! user_can( $user_id, 'edit_workflow' ) ) {
		return 0;
	}

	return (int) get_user_meta( $user_id, 'school_district', true );
}

/**
 * Returns an array of district reps by school district
 *
 * @param integer $school_district
 * @return array
 */
function get_district_reps_by_school_district( $school_district ) {
	return get_users( [
		'role'				=> 'district_rep',
		'number'			=> -1,
		'meta_query'		=> [
			[
				'key'		=> 'school_district',
				'value'		=> absint( $school_district ),
				'compare'	=> '='
			]
		]
	] );
}

/**
 * Returns the link to edit an order for financial aid review
 *
 * @param mixed $order
 * @return string
 */
function get_district_reps_edit_order_link( $order ) {
	if ( ! $order instanceof \WC_Order ) {
		$order = wc_get_order( $order );
	}

	if ( $order ) {
		return add_query_arg( [ 'order_id' => $order->get_id() ], get_permalink( get_financial_aid_orders_page_id() ) );
	}

	return home_url();
}

/**
 * Filter author display name
 *
 * @param string $value
 * @param WP_User $user_id
 * @return string
 */
function author_display_name( $value, $user_id ) {
	$user = get_userdata( $user_id );

	$value = trim( $user->first_name . ' ' . $user->last_name );

	return $value;
}
add_filter( 'get_the_author_display_name', __NAMESPACE__ . '\author_display_name', 10, 2 );