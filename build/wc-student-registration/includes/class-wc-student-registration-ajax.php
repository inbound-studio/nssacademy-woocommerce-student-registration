<?php
/**
 * WC_Student_Registration_AJAX class file
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration;

class WC_Student_Registration_AJAX {

    public function __construct() {
        add_action( 'wp_ajax_wcsr_get_person', [ $this, 'get_person'] );
        add_action( 'wp_ajax_wcsr_get_students', [ $this, 'get_students'] );
        add_action( 'wp_ajax_wcsr_save_student', [ $this, 'save_student'] );
    }

    /**
     * Returns a persons info
     *
     * @return void
     */
    public function get_person() {
        if ( ! wp_verify_nonce( $_REQUEST['nonce'] ) ) {
            wp_send_json_error( new \WP_Error( '001', __( 'Invalid nonce', 'wc-student-registration' ) ) );
        }

        $person = $_REQUEST['person'] ?? 0;
        $type   = $_REQUEST['type'] ?? 'student';

        switch ( $type ) {
            case 'student' :
            default : 
                $keys = [
                    'student_first_name',
                    'student_last_name',
                    'registered_class',
                    'student_gender',
                    'student_dob',
                    'student_ethnicity',
                    'student_shirt_size',
                    'student_school_name',
                    'student_school_district',
                    'student_grade',
                    'student_health_concerns_conditional',
                    'student_health_concerns',
                    'student_medications_conditional',
                    'student_reason_medication',
                    'student_medications',
                    'student_diabetes_conditional',
                    'student_method_transport',
                    'student_strengths',
                    'student_skills_to_improve',
                    'student_additional_comments',
                    'student_emergency_contacts',
                    'authorize_medicate_student',
                    'consent_media_release_student'
                ];

                $_meta = get_post_meta( $person );
                break;

            case 'parent-guardian' :
                $keys = [
                    'parent_guardian_first_name',
                    'parent_guardian_last_name',
                    'parent_guardian_relationship',
                    'parent_guardian_phone',
                    'parent_guardian_email',
                ];

                $_meta = get_post_meta( $person );
                break;

            case 'account' :
                $keys = [
                    'first_name',
                    'last_name',
                    'billing_phone',
                    'billing_relationship',
                    'carpool'
                ];

                $_meta = get_user_meta( $person );
                break;
        }

        $meta  = [];
        ?>

        <table cellspacing="10" cellpadding="10" border="1" class="person-info-table">
            <tbody>
                <?php
                foreach ( $keys as $key ) {
                    foreach ( $_meta as $_key => $_value ) {
                        if ( $key === $_key ) {
                            switch ( $key ) {
                                case 'registered_class' :
                                    $value = get_the_title( current( $_value ) );
                                    break;

                                case 'student_school_district' :
                                    $value = get_term( current( $_value ), 'school_district' )->name;
                                    break;

                                default :
                                    $value = current( $_value );
                            }
                            ?>

                            <tr>
                                <td><?php print esc_html( ucfirst( preg_replace( '/[-_]/', ' ', $key ) ) ); ?></td>
                                <td><?php print esc_html( $value ); ?></td>
                            </tr>

                            <?php
                        }
                    }
                }
                ?>
            </tbody>
        </table>

        <?php
    }

    /**
     * Return array of students searched by a term
     *
     * @return array
     */
    public function get_students() {
        ob_start();

		check_ajax_referer();

		if ( ! current_user_can( 'edit_shop_orders' ) ) {
			wp_die( -1 );
		}

		$term  = isset( $_GET['term'] ) ? (string) wc_clean( wp_unslash( $_GET['term'] ) ) : '';
		$limit = -1;

		if ( empty( $term ) ) {
			wp_die();
		}

		$students = [];

        if ( 3 > strlen( $term ) ) {
            $limit = 20;
        }
        
        $students = get_posts( [
            'post_type'         => 'student',
            'post_status'       => 'publish',
            'posts_per_page'    => $limit,
            's'                 => $term
        ] );

		$found_students = [];

		foreach ( $students as $student ) {
            $student = new WC_Student( $student->ID );
            
            if ( $student ) {
                $found_students[ $student->ID ] = $student->get_full_name();
            }
        }
        
		wp_send_json( $found_students );
    }

    /**
     * Save a student on an order line item
     *
     * @return array
     */
    public function save_student() {
        ob_start();

		check_ajax_referer();

		if ( ! current_user_can( 'edit_shop_orders' ) ) {
			wp_die( -1 );
		}

		$student = isset( $_REQUEST['student'] ) ? (int) $_REQUEST['student'] : '';
        $item_id = isset( $_REQUEST['item_id'] ) ? (int) $_REQUEST['item_id'] : '';
        
        $update = wc_update_order_item_meta( $item_id, 'Student', $student );
        
		wp_send_json( $update );
    }
}

return new WC_Student_Registration_AJAX;