<?php
/**
 * Student registration list shortcode
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration\Shortcodes;

use function WC_Student_Registration\Functions\get_all_students;
use function WC_Student_Registration\Functions\get_students_in_school_district;
use function WC_Student_Registration\Functions\get_district_rep_district;
use function WC_Student_Registration\Functions\get_student_order;

/**
 * Displays a list of student registrations for district reps to view
 *
 * @param array $atts
 * @param string $content
 * @return string
 */
function student_registration_list( $atts, $content = '' ) {
    // Permissions check
    if ( ! current_user_can( 'edit_workflow' ) && ! current_user_can( 'review_financial_aid' ) ) {
        return sprintf( '<p>%s</p>', __( 'You do not have permission to view this content.', 'wc-student-registration' ) );
    }

    if ( current_user_can( 'manage_options' ) ) {
        $students = get_all_students();
    } else {
        $students = get_students_in_school_district( get_district_rep_district( get_current_user_id() ) );
    }

    ob_start();
    ?>

    <p class="student-registration-export">
        <a href="<?php print add_query_arg( [ '_wpnonce' => wp_create_nonce(), 'export-wcsr-reg-list' => 1 ] ); ?>" class="button student-registration-export-trigger fas fa-file-export">
            <?php esc_html_e( 'Export', 'wc-student-registration' ); ?>
        </a>
    </p>

    <table class="student-registration-list">
        <thead>
            <tr>
                <th><?php _e( 'Account Name', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Student Name', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Student Class', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'T-Shirt Size', 'wc-student-registration' ); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if ( $students ) : ?>

                <?php foreach ( $students as $student ) : if ( ! get_student_order( $student ) ) continue; ?>

                <tr>
                    <td><a href="#" data-person-type="account" data-person="<?php print get_the_author_meta( 'ID', $student->get_account() ); ?>"><?php esc_html_e( get_the_author_meta( 'display_name', $student->get_account() ), 'wc-student-registration' ); ?></a></td>
                    <td><a href="#" class="student-info" data-person="<?php print esc_attr( $student->ID ); ?>"><?php esc_html_e( $student->get_full_name(), 'wc-student-registration' ); ?></a></td>
                    <td><?php esc_html_e( $student->get_registered_class() ? get_the_title( $student->get_registered_class() ) : '', 'wc-student-registration' ); ?></td>
                    <td><?php esc_html_e( get_post_meta( $student->ID, 'student_shirt_size', true ), 'wc-student-registration' ); ?></td>
                </tr>

                <?php endforeach; ?>

            <?php else : ?>

            <tr>
                <td colspan="6"><?php _e( 'No students found.', 'wc-student-registration' ); ?></td>
            </tr>

            <?php endif; ?>
        </tbody>
    </table>

    <?php 
    return ob_get_clean();
}
add_shortcode( 'wc-student-registration-list', __NAMESPACE__ . '\student_registration_list' );