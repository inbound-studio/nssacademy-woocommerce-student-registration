<?php
/**
 * Student registration orders list shortcode
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration\Shortcodes;

use WC_Student_Registration\WC_Student;
use WC_Student_Registration\WC_Parent_Guardian;

use function WC_Student_Registration\Functions\get_school_district_orders;
use function WC_Student_Registration\Functions\get_school_district_name;
use function WC_Student_Registration\Functions\get_financial_aid_orders_page_id;
use function WC_Student_Registration\Functions\get_order_financial_aid_amount;
use function WC_Student_Registration\Functions\get_district_rep_district;
use function WC_Student_Registration\Functions\get_order_pending_financial_aid_amount;
use function WC_Student_Registration\Functions\get_order_financial_aid_needs_review;
use function WC_Student_Registration\Functions\get_order_financial_aid_notes;

/**
 * Displays a list of student registrations orders for district reps to view
 *
 * @param array $atts
 * @param string $content
 * @return string
 */
function student_registration_orders( $atts, $content = '' ) {
    // Permissions check
    if ( ! current_user_can( 'edit_workflow' ) && ! current_user_can( 'review_financial_aid' ) ) {
        return sprintf( '<p>%s</p>', __( 'You do not have permission to view this content.', 'wc-student-registration' ) );
    }

    if ( current_user_can( 'manage_options' ) ) {
        $orders = get_school_district_orders();
    } else {
        $orders = get_school_district_orders( get_district_rep_district( get_current_user_id() ) );
    }

    ob_start();

    if ( ! isset( $_REQUEST['order_id'] ) ) :
    ?>

    <table class="student-registration-list student-registration-orders-list">
        <thead>
            <tr>
                <th><?php _e( 'Order ID', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Customer Name', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'School District', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Financial Aid', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Financial Aid Amount', 'wc-student-registration' ); ?></th>
                <th><?php _e( 'Status', 'wc-student-registration' ); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if ( $orders ) : ?>

                <?php foreach ( $orders as $order ) : $user = $order->get_user(); ?>

                <tr>
                    <td><?php echo sprintf( '<a href="%s">%s</a>', esc_url( add_query_arg( [ 'order_id' => $order->get_id() ], get_permalink( get_financial_aid_orders_page_id() ) ) ), esc_html( $order->get_id() ) ); ?></td>
                    <td><?php echo esc_html( $user->display_name ); ?></td>
                    <td><?php echo esc_html( get_school_district_name( $order->get_meta( 'school_district' ) ) ); ?></td>
                    <td><?php echo esc_html( ucfirst( $order->get_meta( 'financial_aid' ) ? $order->get_meta( 'financial_aid' ) : 'no' ) ) ; ?></td>
                    <td><?php echo $order->get_status() === 'pending-fa' ? '-' : wc_price( get_order_financial_aid_amount( $order ) ) ; ?></td>
                    <td><?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ) ; ?></td>
                </tr>

                <?php endforeach; ?>

            <?php else : ?>

            <tr>
                <td colspan="6"><?php _e( 'No orders found.', 'wc-student-registration' ); ?></td>
            </tr>

            <?php endif; ?>
        </tbody>
    </table>

    <?php 
    else :

        $order = wc_get_order( $_REQUEST['order_id'] );

        if ( $order ) :
        ?>

            <h3><?php esc_html_e( 'Class Registration', 'wc-student-registation' ); ?></h3>

            <table class="student-registration-list student-registration-view-order">
                <thead>
                    <tr>
                        <th><?php _e( 'Student', 'wc-student-registration' ); ?></th>
                        <th><?php _e( 'Class', 'wc-student-registration' ); ?></th>
                        <th><?php _e( 'Parent/Guardian 1', 'wc-student-registration' ); ?></th>
                        <th><?php _e( 'Parent/Guardian 2', 'wc-student-registration' ); ?></th>
                        <th><?php _e( 'School District', 'wc-student-registration' ); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        foreach ( $order->get_items() as $item ) :
                            $student           = new WC_Student( $item->get_meta( 'Student' ) );
                            $parent_guardian_1 = new WC_Parent_Guardian( $item->get_meta( 'Parent/Guardian 1' ) );
                            $parent_guardian_2 = new WC_Parent_Guardian( $item->get_meta( 'Parent/Guardian 2' ) );
                    ?>

                        <tr>
                            <td><a href="#" class="student-info" data-person="<?php print esc_attr( $student->ID ); ?>"><?php echo esc_html( $student->get_full_name() ); ?></a></td>
                            <td><?php echo esc_html( $item->get_name() ); ?></td>
                            <td><a href="#" class="parent-guardian-info" data-person-type="parent-guardian" data-person="<?php print esc_attr( $parent_guardian_1->ID ); ?>"><?php echo esc_html( $parent_guardian_1->get_full_name() ); ?></a></td>
                            <td><a href="#" class="parent-guardian-info" data-person-type="parent-guardian" data-person="<?php print esc_attr( $parent_guardian_2->ID ); ?>"><?php echo esc_html( $parent_guardian_2->get_full_name() ); ?></a></td>
                            <td><?php echo esc_html( get_school_district_name( $order->get_meta( 'school_district' ) ) ); ?></td>
                        </tr>

                    <?php endforeach; ?>
                </tbody>
            </table>

            <h3><?php esc_html_e( 'Financial Aid', 'wc-student-registation' ); ?></h3>

            <?php 
            /**
             * Director financial aid review
             */
            if ( current_user_can( 'review_financial_aid' ) && get_order_financial_aid_needs_review( $order ) ) : ?>

                <?php if ( $order->get_status() === 'pending-fa' && get_order_pending_financial_aid_amount( $order ) !== false ) : ?>

                    <p><strong><?php printf( __( 'Financial aid amount to review: %s', 'wc-student-registration' ), wc_price( get_order_pending_financial_aid_amount( $order ) ) ); ?></strong></p>

                    <form method="post">
                        <p>
                            <label><input type="radio" value="approved" name="financial_aid_review" required /> <?php esc_html_e( 'Approve', 'wc-student-registration' ); ?></label><br>
                            <label><input type="radio" value="denied" name="financial_aid_review" required /> <?php esc_html_e( 'Reject', 'wc-student-registration' ); ?></label>
                        </p>
                        <p>
                            <textarea name="financial_aid_notes" cols="40" placeholder="<?php esc_attr_e( 'Notes', 'wc-student-registration' ); ?>" required></textarea>
                        </p>
                        <?php wp_nonce_field(); ?>
                        <input type="hidden" name="wcsr_action" value="submit_financial_aid_review" />
                        <input type="hidden" name="order_id" value="<?php echo esc_attr( $order->get_id() ); ?>" />
                        <input type="submit" placeholder="0" value="<?php esc_attr_e( 'Submit', 'wc-student-registration' ); ?>" /><br>
                    </form>

                <?php endif; ?>

            <?php 
            endif;

            /**
             * District rep approval / denial
             */
            if ( ! current_user_can( 'review_financial_aid' ) || current_user_can( 'manage_options' ) ) : ?>

                <?php if ( $order->get_status() !== 'pending-fa' ) : ?>

                    <p><strong><?php printf( __( 'Financial aid amount provided: %s', 'wc-student-registration' ), wc_price( get_order_financial_aid_amount( $order ) ) ); ?></strong></p>

                <?php endif; ?>

                <?php if ( ! get_order_financial_aid_needs_review( $order ) ) : ?>

                    <?php if ( ! get_order_financial_aid_notes( $order ) ) : ?>

                        <p><?php esc_html_e( 'Enter the amount of financial aid assistance that will be applied to the class registrations above. If multiple class registrations are listed above, the amount of financial aid provided below will be applied to the entire order, and is not applied to each registration individually.', 'wc-student-registration' ); ?></p>

                    <?php else : ?>

                        <p>
                            <strong><?php esc_html_e( 'Notes from director', 'wc-student-registration' ); ?>:</strong><br>
                            <?php esc_html_e( get_order_financial_aid_notes( $order ), 'wc-student-registration' ); ?>
                        </p>

                    <?php endif; ?>

                    <?php if ( $order->get_status() !== 'pending' ) : ?>

                    <form method="post">
                        <div>
                            <span class="description"><?php printf( esc_html__( 'Amounts are in %s (%s)', 'wc-student-registration' ), get_woocommerce_currency(), get_woocommerce_currency_symbol() ); ?></span><br>
                            <input type="number" min="0" step="1" name="financial_aid_amount" placeholder="0" />
                        </div>
                        <?php wp_nonce_field(); ?>
                        <input type="hidden" name="wcsr_action" value="submit_financial_aid" />
                        <input type="hidden" name="order_id" value="<?php echo esc_attr( $order->get_id() ); ?>" />
                        <input type="submit" placeholder="0" value="<?php esc_attr_e( 'Apply', 'wc-student-registration' ); ?>" /><br>
                    </form>

                    <?php endif; ?>

                <?php endif; ?>

            <?php endif; ?>

        <?php    
        endif;

    endif;

    return ob_get_clean();
}
add_shortcode( 'wc-student-registration-orders', __NAMESPACE__ . '\student_registration_orders' );