<?php
/**
 * Student object
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration;

class WC_Student {

	/**
	 * Student post ID
	 *
	 * @var integer
	 */
	public $ID;

	/**
	 * Student post ID
	 *
	 * @var integer
	 */
	public $id;

	/**
	 * Instance of student WP_Post object
	 *
	 * @var WP_Post
	 */
	protected $post;

	public function __construct( $id ) {
		$this->ID    = $id;
		$this->id    = $id;
		$this->post  = get_post( $id );
	}

	/**
	 * Returns WP_Post object of this student
	 *
	 * @return WP_Post
	 */
	public function get_post() {
		return $this->post;
	}

	/**
	 * Return associated WP user account ID
	 *
	 * @return integer
	 */
	public function get_account() {
		return absint( $this->get_post()->post_author );
	}

	/**
	 * Return student first name
	 *
	 * @return string
	 */
	public function get_first_name() {
		return get_post_meta( $this->ID, 'student_first_name', true );
	}

	/**
	 * Return student last name
	 *
	 * @return string
	 */
	public function get_last_name() {
		return get_post_meta( $this->ID, 'student_last_name', true );
	}

	/**
	 * Return student full name
	 *
	 * @return string
	 */
	public function get_full_name() {
		$name = trim( sprintf( '%s %s', $this->get_first_name(), $this->get_last_name() ) );

		if ( empty( $name ) ) {
			$name = __( 'No name set', 'wc-student-registration' );
		}

		return $name;
	}

	/**
	 * Return student currently registered class
	 *
	 * @return string
	 */
	public function get_registered_class() {
		return get_post_meta( $this->ID, 'registered_class', true );
	}

}
