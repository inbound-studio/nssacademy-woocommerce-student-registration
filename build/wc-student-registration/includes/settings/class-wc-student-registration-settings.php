<?php
/**
 * WC_Student_Registration_Settings class file
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration;

/**
 * WC_Admin_Settings_General.
 */
class WC_Student_Registration_Settings extends \WC_Integration {

	/**
	 * Constructor.
	 */
	public function __construct() {
        global $woocommerce;
        
		$this->id                 = 'wc-student-registration';
		$this->method_title       = __( 'Student Registration', 'wc-student-registration' );
		$this->method_description = __( 'Add student registration capability', 'wc-student-registration' );

		// Load the settings.
		$this->init_form_fields();
		$this->init_settings();

        add_action( 'woocommerce_update_options_integration_' .  $this->id, [ $this, 'process_admin_options' ] );
        add_action( 'admin_menu', [ $this, 'student_settings'] );
    }
    

    public function init_form_fields() {
		$this->form_fields = [
            'financial_aid_page_id'         => [
                'title'    => __( 'Financial Aid Order Page', 'wc-student-registration' ),
                'id'       => 'wc_student_registration_financial_aid_page',
                'type'     => 'single_select_page',
                'default'  => '',
                'class'    => 'wc-enhanced-select-nostd',
                'css'      => 'min-width:300px;',
                /* Translators: %s Page contents. */
                'desc_tip' => sprintf( __( 'Page contents: [%s]', 'wc-student-registration' ), 'wc-student-registration-orders' ),
                'autoload' => true,
            ],
            'financial_aid_director_email'  => [
                'title'    => __( 'Financial Aid Director Email', 'wc-student-registration' ),
                'id'       => 'wc_student_registration_financial_aid_director_email',
                'type'     => 'text',
                'default'  => '',
                'desc_tip' => __( 'Emails for financial aid review will be sent here', 'wc-student-registration' ),
            ],
            'out_of_district_fee'           => [
                'title'    => __( 'Out of District Fee', 'wc-student-registration' ),
                'id'       => 'wc_student_registration_out_of_district_fee',
                'type'     => 'number',
                'default'  => '25',
                'desc_tip' => __( 'Fee to be added to each class registration if not in district', 'wc-student-registration' ),
            ]
        ];
    }
    

    public function generate_single_select_page_html( $key, $data ) {
        $field_key = $this->get_field_key( $key );

        $args = [
            'name'             => esc_attr( $field_key ),
            'id'               => esc_attr( $field_key ),
            'sort_column'      => 'menu_order',
            'sort_order'       => 'ASC',
            'show_option_none' => ' ',
            'class'            => $data['class'],
            'echo'             => false,
            'selected'         => absint( $this->get_option( $key ) ),
            'post_status'      => 'publish,private,draft',
        ];

        ob_start();
        ?>
        <tr valign="top" class="single_select_page">
            <th scope="row" class="titledesc">
                <label for="<?php echo esc_attr( $field_key ); ?>"><?php echo wp_kses_post( $data['title'] ); ?> <?php echo $this->get_tooltip_html( $data ); // WPCS: XSS ok. ?></label>
            </th>
            <td class="forminp">
            <legend class="screen-reader-text"><span><?php echo wp_kses_post( $data['title'] ); ?></span></legend>
                <?php echo str_replace( ' id=', " data-placeholder='" . esc_attr__( 'Select a page&hellip;', 'wc-student-registration' ) . "' style='" . esc_attr( $data['css'] ) . "' class='" . esc_attr( $data['class'] ) . "' id=", wp_dropdown_pages( $args ) ); // WPCS: XSS ok. ?> <?php echo $description; // WPCS: XSS ok. ?>
            </td>
        </tr>
        <?php
		return ob_get_clean();
    }
    

    public function student_settings() {
        add_submenu_page( 'edit.php?post_type=student', __( 'Export Students', 'wc-student-registration' ), __( 'Export Students', 'wc-student-registration' ), 'manage_options', 'wcsr_students_export', function() {
            ?>

            <div class="wrap">
                <h1><?php _e( 'Export Students', 'wc-student-registration' ); ?></h1>

                <p>
                    <a href="<?php print add_query_arg( [ '_wpnonce' => wp_create_nonce(), 'export-wcsr-students-registratios' => 1 ] ); ?>" class="button button-primary student-registration-export-trigger fas fa-file-export">
                        <?php esc_html_e( 'Export Student Registrations', 'wc-student-registration' ); ?>
                    </a>
                </p>
            </div>

            <?php
        } );
    }

}