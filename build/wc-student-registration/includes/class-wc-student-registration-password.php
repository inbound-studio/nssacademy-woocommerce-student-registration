<?php
/**
 * WC_Student_Registration_Password class file
 *
 * @package WooCommerce Student Registration
 */

namespace WC_Student_Registration;

class WC_Student_Registration_Password {

    public function __construct() {
        add_action( 'wp_head', [ $this, 'js_head_password'] );

        if ( ! isset( $_GET['wcsr_password_check'] ) || is_admin() ) {
            return;
        }

        add_filter( 'template_include', [ $this, 'endpoint' ] );
        add_filter( 'the_content', [ $this, 'endpoint_content'], 100 );
        add_action( 'wp', [ $this, 'form_handler' ] );
    }

    /**
     * Custom page endpoint for our password
     *
     * @param string $template
     * @return string
     */
    public function endpoint( $template ) {
        $password = locate_template( [ 'course-registration-password.php' ] );

        if ( $password ) {
            $template = $password;
        }

        return $template;
    }

    /**
     * Output password field content
     *
     * @param string $content
     * @return string
     */
    public function endpoint_content( $content ) {
        ob_start();

        wc_print_notices();
        ?>

        <form method="post">
            <label for="course_registration_password"><?php _e( 'Password', 'wc-student-registration' ); ?></label>
            <input type="password" id="course_registration_password" name="course_registration_password" value="" />
            <input type="submit" />
        </form>

        <?php
        return ob_get_clean();
    }

    /**
     * Add a cookie to users browser if password is correct
     *
     * @return void
     */
    public function form_handler() {
        if ( ! empty( $_POST ) && ! empty( $password = $_POST['course_registration_password'] ) ) {
            if ( defined( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PASSWORD' ) ) {
                if ( $password == constant( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PASSWORD' ) ) {
                    $cart_item = isset( $_COOKIE['wcsr_course_cart_item'] ) ? json_decode( base64_decode( $_COOKIE['wcsr_course_cart_item'] ), true ) : [];

                    setcookie( 'wcsr_course_registration_password', 'yes', time() + 60 * 60 * 24 * 30, '/' );

                    if ( ! empty( $cart_item ) ) {
                        WC()->cart->add_to_cart( $cart_item['product_id'], $cart_item['quantity'], $cart_item['variation_id'], $cart_item['variation'], array_merge( [ 'wcsr_no_password' => true ], $cart_item ) );
                    
                        unset( $_COOKIE['wcsr_course_cart_item'] );
                    }

                    wp_redirect( wc_get_cart_url() );
                    exit;
                } else {
                    wc_add_notice( __( 'Incorrect password, please try again.', 'wc-student-registration' ), 'error' );
                }
            }
        }
    }

    public function js_head_password() {
        if ( defined( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PASSWORD' ) ) {
        ?>

        <script>
            var wcsrRegPassword = '<?php print esc_js( constant( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PASSWORD' ) ); ?>';
            var wcsrPreRegPassword = '<?php print esc_js( constant( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PRE_REG_PASSWORD' ) ); ?>';

            var wcsrRegPasswordTime = '<?php print esc_js( constant( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_REG_TIMESTAMP' ) ); ?>';
            var wcsrPreRegPasswordTime = '<?php print esc_js( constant( 'SUMMER_ACADEMY_' . date( 'Y' ) . '_PRE_REG_TIMESTAMP' ) ); ?>';
        </script>

        <?php
        }
    }

}

return new WC_Student_Registration_Password;