# WooCommerce Student Registration

**Setup**

`composer install`  
`npm install`

**Search replace (case-sensitive):**

WooCommerce Student Registration  
WC_Student_Registration  
WC_STUDENT_REGISTRATION  
wc-student-registration  
wc_student_registration

**Rename:**

wc-student-registration.php  
includes/class-wc-student-registration.php

